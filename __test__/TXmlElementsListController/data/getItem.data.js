// [v0.1.004-20230921]

/* class:    TXmlElementsListController
 * method:   getItem
 * property: ---
 */

const preloads = {
  get obj(){
    return [
      { __text: 'item 1' },
      { __text: 'item 2' },
      { __text: 'item 3' },
    ];
  },
  get options(){ return undefined; },
  status: {
    obj: [
      { __text: 'item 1' },
      { __text: 'item 2' },
      { __text: 'item 3' },
    ],
    options: undefined,
    __unwrap: undefined,
  },
};

const valueTestDescr = {
  msg: 'run tests against "index" param',
  rem: '',
  param: [
    {
      msg: 'no args are given',
      param: [
        ({
          msg: 'undefined value is passed',
          rem: '(function failed: "null" is returned)',
          preloads: preloads,
          values: {
            index: undefined,
          },
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: null,
            },
            assertQty: 2,
            before: undefined,
            after: undefined,
          },
          initTest(){
            this.status.before = this.preloads.status;
            return this;
          },
        }).initTest(),
      ],
    }, {
      msg: 'non-valid args are given',
      param: [
        ({
          msg: 'value is a "null"',
          rem: '(function failed: "null" is returned)',
          preloads: preloads,
          values: {
            index: null,
          },
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: null,
            },
            assertQty: 2,
            before: undefined,
            after: undefined,
          },
          initTest(){
            this.status.before = this.preloads.status;
            return this;
          },
        }).initTest(),
        ({
          msg: 'value is a negative number',
          rem: '(function failed: "null" is returned)',
          preloads: preloads,
          values: {
            index: -15,
          },
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: null,
            },
            assertQty: 2,
            before: undefined,
            after: undefined,
          },
          initTest(){
            this.status.before = this.preloads.status;
            return this;
          },
        }).initTest(),
        ({
          msg: 'value is a string',
          rem: '(function failed: "null" is returned)',
          preloads: preloads,
          values: {
            index: 'some string',
          },
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: null,
            },
            assertQty: 2,
            before: undefined,
            after: undefined,
          },
          initTest(){
            this.status.before = this.preloads.status;
            return this;
          },
        }).initTest(),
      ],
    }, {
      msg: 'a valid args are given',
      param: [
        ({
          msg: 'value is a positive number',
          rem: '',
          preloads: preloads,
          values: {
            index: 2,
          },
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: 'TXmlElementController',
              value: 'item 3',
            },
            assertQty: 3,
            before: undefined,
            after: undefined,
          },
          initTest(){
            this.status.before = this.preloads.status;
            return this;
          },
        }).initTest(),
        ({
          msg: 'value is a string that represents a positive number',
          rem: '',
          preloads: preloads,
          values: {
            index: '1',
          },
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: 'TXmlElementController',
              value: 'item 2',
            },
            assertQty: 3,
            before: undefined,
            after: undefined,
          },
          initTest(){
            this.status.before = this.preloads.status;
            return this;
          },
        }).initTest(),
      ],
    },
  ],
};

const rangeTestDescr = {
  msg: 'run tests against index range',
  param: [
    {
      msg: 'index is out of range',
      param: [
        ({
          msg: 'index is less than "0"',
          rem: '(function failed: "null" is returned)',
          preloads: preloads,
          values: {
            index: -15,
          },
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: null,
            },
            assertQty: 2,
            before: undefined,
            after: undefined,
          },
          initTest(){
            this.status.before = this.preloads.status;
            return this;
          },
        }).initTest(),
        ({
          msg: 'index is greater than "maxIndex"',
          rem: '(function failed: "null" is returned)',
          preloads: preloads,
          values: {
            index: 15,
          },
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: null,
            },
            assertQty: 2,
            before: undefined,
            after: undefined,
          },
          initTest(){
            this.status.before = this.preloads.status;
            return this;
          },
        }).initTest(),
      ],
    }, {
      msg: 'index is in range',
      param: [
        ({
          msg: 'run test',
          rem: '',
          preloads: preloads,
          values: {
            index: 2,
          },
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: 'TXmlElementController',
              value: 'item 3',
            },
            assertQty: 3,
            before: undefined,
            after: undefined,
          },
          initTest(){
            this.status.before = this.preloads.status;
            return this;
          },
        }).initTest(),
      ],
    },
  ],
};

module.exports = {
  preloads: preloads,
  descr: [
    valueTestDescr,
    rangeTestDescr,
  ],
};
