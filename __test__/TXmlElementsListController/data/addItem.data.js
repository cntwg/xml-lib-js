// [v0.1.003-20230921]

/* class:    TXmlElementsListController
 * method:   addItem
 * property: ---
 */

const EMPTY_STRING = '';
const EMPTY_OBJECT = {};
const EMPTY_ARRAY = [];
const EMPTY_ELEM = { __attr: {}, __text: '' };

const initElem = () => { return { __attr: {}, __text: '' } };

const preloads = {
  get obj(){ return []; },
  get options(){ return undefined; },
};

const valueTestDescr = {
  msg: 'run tests against "item" param',
  rem: '',
  param: [
    {
      msg: 'no args are given',
      param: [
        {
          msg: 'undefined value is passed',
          rem: '',
          preloads: preloads,
          values: {
            value: undefined,
          },
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: -1,
            },
            assertQty: 2,
            before: {
              size: 0,
              maxIndex: -1,
              isEmpty: true,
              obj: [],
              options: undefined,
            },
            after: {
              size: 0,
              maxIndex: -1,
              isEmpty: true,
              obj: [],
              options: undefined,
            },
          },
        },
      ],
    }, {
      msg: 'non-valid args are given',
      param: [
        {
          msg: 'a "number" is passed',
          rem: '',
          preloads: preloads,
          values: {
            value: 123,
          },
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: -1,
            },
            assertQty: 2,
            before: {
              size: 0,
              maxIndex: -1,
              isEmpty: true,
              obj: [],
              options: undefined,
            },
            after: {
              size: 0,
              maxIndex: -1,
              isEmpty: true,
              obj: [],
              options: undefined,
            },
          },
        }, {
          msg: 'a "string" is passed',
          rem: '',
          preloads: preloads,
          values: {
            value: 'some string',
          },
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: -1,
            },
            assertQty: 2,
            before: {
              size: 0,
              maxIndex: -1,
              isEmpty: true,
              obj: [],
              options: undefined,
            },
            after: {
              size: 0,
              maxIndex: -1,
              isEmpty: true,
              obj: [],
              options: undefined,
            },
          },
        },
      ],
    }, {
      msg: 'a valid args are given',
      param: [
        {
          msg: 'an empty object is passed',
          rem: '',
          preloads: preloads,
          values: {
            value: {},
          },
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: 0,
            },
            assertQty: 2,
            before: {
              size: 0,
              maxIndex: -1,
              isEmpty: true,
              obj: [],
              options: undefined,
            },
            after: {
              size: 1,
              maxIndex: 0,
              isEmpty: false,
              obj: [ {} ],
              options: undefined,
            },
          },
        }, {
          msg: 'an empty element template is passed',
          rem: '',
          preloads: preloads,
          values: {
            value: initElem(),
          },
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: 0,
            },
            assertQty: 2,
            before: {
              size: 0,
              maxIndex: -1,
              isEmpty: true,
              obj: [],
              options: undefined,
            },
            after: {
              size: 1,
              maxIndex: 0,
              isEmpty: false,
              obj: [ initElem() ],
              options: undefined,
            },
          },
        },
      ],
    },
  ],
};

module.exports = {
  preloads: preloads,
  descr: [
    valueTestDescr,
  ],
};
