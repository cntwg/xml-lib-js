// [v0.1.001-20230214]

/* class:    TXmlElementsListController
 * method:   constructor
 * property: ---
 */

const EMPTY_STRING = '';
const EMPTY_OBJECT = {};
const EMPTY_ARRAY = [];
const EMPTY_ELEM = { __attr: {}, __text: '' };

const initElem = () => { return { __attr: {}, __text: '' } };

const objTestDescr = {
  msg: 'run tests aganst "obj" param (use default options)',
  param: [
    {
      msg: 'no args are given',
      preloads: {
        get obj(){ return undefined; },
        get options(){ return undefined; },
      },
      param: [
        {
          msg: 'undefined value is passed',
          get obj(){ return undefined; },
          get options(){ return undefined; },
          status: {
            class: 'TXmlElementsListController',
            size: 0,
            maxIndex: -1,
            isEmpty: true,
            obj: undefined,
            options: undefined,
          },
        },
      ],
    }, {
      msg: 'non-valid args are given',
      preloads: {
        get obj(){ return undefined; },
        get options(){ return undefined; },
      },
      param: [
        {
          msg: 'null value is passed',
          get obj(){ return null; },
          get options(){ return undefined; },
          status: {
            class: 'TXmlElementsListController',
            size: 0,
            maxIndex: -1,
            isEmpty: true,
            obj: null,
            options: undefined,
          },
        }, {
          msg: 'an object value is passed',
          get obj(){ return {}; },
          get options(){ return undefined; },
          status: {
            class: 'TXmlElementsListController',
            size: 0,
            maxIndex: -1,
            isEmpty: true,
            obj: EMPTY_OBJECT,
            options: undefined,
          },
        },
      ],
    }, {
      msg: 'a valid args are given (an array is passed)',
      preloads: {
        get obj(){ return undefined; },
        get options(){ return undefined; },
      },
      param: [
        {
          msg: 'an empty array is passed',
          get obj(){ return []; },
          get options(){ return undefined; },
          status: {
            class: 'TXmlElementsListController',
            size: 0,
            maxIndex: -1,
            isEmpty: true,
            obj: [],
            options: undefined,
          },
        }, {
          msg: 'a non-empty array is passed',
          get obj(){
            return [
              initElem(),
              initElem(),
            ];
          },
          get options(){ return undefined; },
          status: {
            class: 'TXmlElementsListController',
            size: 2,
            maxIndex: 1,
            isEmpty: false,
            obj: [
              initElem(),
              initElem(),
            ],
            options: undefined,
          },
        },
      ],
    },
  ],
};

module.exports = {
  descr: [
    objTestDescr,
  ],
};
