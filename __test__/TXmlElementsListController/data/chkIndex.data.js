// [v0.1.003-20230921]

/* class:    TXmlElementsListController
 * method:   chkIndex
 * property: ---
 */

const EMPTY_STRING = '';
const EMPTY_OBJECT = {};
const EMPTY_ARRAY = [];
const EMPTY_ELEM = { __attr: {}, __text: '' };

const initElem = () => { return { __attr: {}, __text: '' } };

const preloads = {
  get obj(){
    return [
      { __text: 'item 1' },
      { __text: 'item 2' },
      { __text: 'item 3' },
      { __text: 'item 4' },
    ];
  },
  get options(){ return undefined; },
};

const valueTestDescr = {
  msg: 'run tests against "index" param',
  rem: '',
  param: [
    {
      msg: 'no args are given',
      param: [
        {
          msg: 'value is undefined',
          rem: '',
          preloads: preloads,
          values: {
            value: undefined,
          },
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: false,
            },
            assertQty: 2,
            maxIndex: 3,
            size: 4,
            isEmpty: false,
            obj: preloads.obj,
            options: undefined,
          },
        },
      ],
    }, {
      msg: 'non-valid args are given',
      param: [
        {
          msg: 'value is a "null"',
          rem: '',
          preloads: preloads,
          values: {
            value: null,
          },
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: false,
            },
            assertQty: 2,
            maxIndex: 3,
            size: 4,
            isEmpty: false,
            obj: preloads.obj,
            options: undefined,
          },
        }, {
          msg: 'value is a negative number',
          rem: '',
          preloads: preloads,
          values: {
            value: -15,
          },
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: false,
            },
            assertQty: 2,
            maxIndex: 3,
            size: 4,
            isEmpty: false,
            obj: preloads.obj,
            options: undefined,
          },
        }, {
          msg: 'value is a string',
          rem: '',
          preloads: preloads,
          values: {
            value: 'some string',
          },
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: false,
            },
            assertQty: 2,
            maxIndex: 3,
            size: 4,
            isEmpty: false,
            obj: preloads.obj,
            options: undefined,
          },
        },
      ],
    }, {
      msg: 'a valid args are given',
      param: [
        {
          msg: 'value is a positive number',
          rem: '',
          preloads: preloads,
          values: {
            value: 2,
          },
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: true,
            },
            assertQty: 2,
            maxIndex: 3,
            size: 4,
            isEmpty: false,
            obj: preloads.obj,
            options: undefined,
          },
        }, {
          msg: 'value is a string',
          rem: 'convertable to a positive number',
          preloads: preloads,
          values: {
            value: '3',
          },
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: true,
            },
            assertQty: 2,
            maxIndex: 3,
            size: 4,
            isEmpty: false,
            obj: preloads.obj,
            options: undefined,
          },
        },
      ],
    },
  ],
};

const rangeTestDescr = {
  msg: 'run tests against index range',
  rem: '',
  param: [
    {
      msg: 'index is out of range',
      param: [
        {
          msg: 'index is less than "0"',
          rem: '',
          preloads: preloads,
          values: {
            value: -15,
          },
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: false,
            },
            assertQty: 2,
            maxIndex: 3,
            size: 4,
            isEmpty: false,
            obj: preloads.obj,
            options: undefined,
          },
        }, {
          msg: 'index is greater than "maxIndex"',
          rem: '',
          preloads: preloads,
          values: {
            value: 15,
          },
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: false,
            },
            assertQty: 2,
            maxIndex: 3,
            size: 4,
            isEmpty: false,
            obj: preloads.obj,
            options: undefined,
          },
        },
      ],
    }, {
      msg: 'index is in range',
      param: [
        {
          msg: 'perform test',
          rem: '',
          preloads: preloads,
          values: {
            value: 2,
          },
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: true,
            },
            assertQty: 2,
            maxIndex: 3,
            size: 4,
            isEmpty: false,
            obj: preloads.obj,
            options: undefined,
          },
        },
      ],
    },
  ],
};

module.exports = {
  preloads: preloads,
  descr: [
    valueTestDescr,
    rangeTestDescr,
  ],
};
