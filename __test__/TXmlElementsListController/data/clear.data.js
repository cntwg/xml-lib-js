// [v0.1.002-20230921]

/* class:    TXmlElementsListController
 * method:   clear
 * property: ---
 */

const EMPTY_STRING = '';
const EMPTY_OBJECT = {};
const EMPTY_ARRAY = [];
const EMPTY_ELEM = { __attr: {}, __text: '' };

const initElem = () => { return { __attr: {}, __text: '' } };

const preloads = {
  get obj(){
    return [
      { __text: 'item 1' },
      { __text: 'item 2' },
      { __text: 'item 3' },
      { __text: 'item 4' },
    ];
  },
  get options(){ return undefined; },
};

const resultTestDescr = {
  msg: 'run tests against result values',
  rem: '',
  param: [
    {
      msg: 'some elements was loaded',
      param: [
        {
          msg: 'perform test',
          rem: '',
          preloads: preloads,
          values: {
            value: undefined,
          },
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: undefined,
            },
            assertQty: 2,
            before: {
              maxIndex: 3,
              size: 4,
              isEmpty: false,
              obj: preloads.obj,
              options: undefined,
            },
            after: {
              maxIndex: -1,
              size: 0,
              isEmpty: true,
              obj: [],
              options: undefined,
            },
          },
        },
      ],
    },
  ],
};

module.exports = {
  preloads: preloads,
  descr: [
    resultTestDescr,
  ],
};
