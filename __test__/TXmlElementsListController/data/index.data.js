// [v0.1.007-20230214]

/* class:    TXmlElementsListController
 * method:   ---
 * property: ---
 */

module.exports = {
  d1t: require('./constructor.data.js').descr,
  main: {
    addItem: require('./addItem.data.js'),
    loadItems: require('./loadItems.data.js'),
    chkIndex: require('./chkIndex.data.js'),
    getItem: require('./getItem.data.js'),
    setItem: require('./setItem.data.js'),
    insItem: require('./insItem.data.js'),
    delItem: require('./delItem.data.js'),
    clear: require('./clear.data.js'),
  },
  getProp: {},
  setProp: {},
};
