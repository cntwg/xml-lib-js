// [v0.1.003-20230921]

/* class:    TXmlElementsListController
 * method:   insItem
 * property: ---
 */

const EMPTY_STRING = '';
const EMPTY_OBJECT = {};
const EMPTY_ARRAY = [];
const EMPTY_ELEM = { __attr: {}, __text: '' };

const initElem = () => { return { __attr: {}, __text: '' } };

const preloads = {
  get obj(){
    return [
      { __text: 'item 1' },
      { __text: 'item 2' },
      { __text: 'item 3' },
      { __text: 'item 4' },
    ];
  },
  get options(){ return undefined; },
};

const valueTestDescr = {
  msg: 'run tests against "index" param',
  rem: '',
  param: [
    {
      msg: 'no args are given',
      param: [
        {
          msg: 'value is undefined',
          rem: '',
          preloads: preloads,
          values: {
            index: undefined,
            item: { __text: 'item 5' },
          },
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: false,
            },
            assertQty: 2,
            before: {
              maxIndex: 3,
              size: 4,
              isEmpty: false,
              obj: preloads.obj,
              options: undefined,
            },
            after: {
              maxIndex: 3,
              size: 4,
              isEmpty: false,
              obj: preloads.obj,
              options: undefined,
            },
          },
        },
      ],
    }, {
      msg: 'non-valid args are given',
      param: [
        {
          msg: 'value is "null"',
          rem: '',
          preloads: preloads,
          values: {
            index: null,
            item: { __text: 'item 5' },
          },
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: false,
            },
            assertQty: 2,
            before: {
              maxIndex: 3,
              size: 4,
              isEmpty: false,
              obj: preloads.obj,
              options: undefined,
            },
            after: {
              maxIndex: 3,
              size: 4,
              isEmpty: false,
              obj: preloads.obj,
              options: undefined,
            },
          },
        },
      ],
    }, {
      msg: 'a valid args are given',
      param: [
        {
          msg: 'value is a positive number',
          rem: '',
          preloads: preloads,
          values: {
            index: 2,
            item: { __text: 'item 5' },
          },
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: true,
            },
            assertQty: 2,
            before: {
              maxIndex: 3,
              size: 4,
              isEmpty: false,
              obj: preloads.obj,
              options: undefined,
            },
            after: {
              maxIndex: 4,
              size: 5,
              isEmpty: false,
              obj: [
                { __text: 'item 1' },
                { __text: 'item 2' },
                { __text: 'item 5' },
                { __text: 'item 3' },
                { __text: 'item 4' },
              ],
              options: undefined,
            },
          },
        }, {
          msg: 'value is a string',
          rem: 'convertable to a positive number',
          preloads: preloads,
          values: {
            index: '1',
            item: { __text: 'item 5' },
          },
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: true,
            },
            assertQty: 2,
            before: {
              maxIndex: 3,
              size: 4,
              isEmpty: false,
              obj: preloads.obj,
              options: undefined,
            },
            after: {
              maxIndex: 4,
              size: 5,
              isEmpty: false,
              obj: [
                { __text: 'item 1' },
                { __text: 'item 5' },
                { __text: 'item 2' },
                { __text: 'item 3' },
                { __text: 'item 4' },
              ],
              options: undefined,
            },
          },
        },
      ],
    },
  ],
};

const rangeTestDescr = {
  msg: 'run tests aganst index range',
  rem: '',
  param: [
    {
      msg: 'index is out of range',
      param: [
        {
          msg: 'index is less than "0"',
          rem: '',
          preloads: preloads,
          values: {
            index: -15,
            item: { __text: 'item 5' },
          },
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: false,
            },
            assertQty: 2,
            before: {
              maxIndex: 3,
              size: 4,
              isEmpty: false,
              obj: preloads.obj,
              options: undefined,
            },
            after: {
              maxIndex: 3,
              size: 4,
              isEmpty: false,
              obj: preloads.obj,
              options: undefined,
            },
          },
        }, {
          msg: 'index is greater than "maxIndex"',
          rem: '',
          preloads: preloads,
          values: {
            index: 15,
            item: { __text: 'item 5' },
          },
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: false,
            },
            assertQty: 2,
            before: {
              maxIndex: 3,
              size: 4,
              isEmpty: false,
              obj: preloads.obj,
              options: undefined,
            },
            after: {
              maxIndex: 3,
              size: 4,
              isEmpty: false,
              obj: preloads.obj,
              options: undefined,
            },
          },
        },
      ],
    }, {
      msg: 'index is in range',
      param: [
        {
          msg: 'perform test',
          rem: '',
          preloads: preloads,
          values: {
            index: 1,
            item: { __text: 'item 5' },
          },
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: true,
            },
            assertQty: 2,
            before: {
              maxIndex: 3,
              size: 4,
              isEmpty: false,
              obj: preloads.obj,
              options: undefined,
            },
            after: {
              maxIndex: 4,
              size: 5,
              isEmpty: false,
              obj: [
                { __text: 'item 1' },
                { __text: 'item 5' },
                { __text: 'item 2' },
                { __text: 'item 3' },
                { __text: 'item 4' },
              ],
              options: undefined,
            },
          },
        },
      ],
    },
  ],
};

const objTestDescr = {
  msg: 'run tests against "item" param',
  rem: '',
  param: [
    {
      msg: 'no args are given',
      param: [
        {
          msg: 'value is undefined',
          rem: '',
          preloads: preloads,
          values: {
            index: 1,
            item: undefined,
          },
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: false,
            },
            assertQty: 2,
            before: {
              maxIndex: 3,
              size: 4,
              isEmpty: false,
              obj: preloads.obj,
              options: undefined,
            },
            after: {
              maxIndex: 3,
              size: 4,
              isEmpty: false,
              obj: preloads.obj,
              options: undefined,
            },
          },
        },
      ],
    }, {
      msg: 'non-valid args are given',
      param: [
        {
          msg: 'value is a "boolean"',
          rem: '',
          preloads: preloads,
          values: {
            index: 1,
            item: true,
          },
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: false,
            },
            assertQty: 2,
            before: {
              maxIndex: 3,
              size: 4,
              isEmpty: false,
              obj: preloads.obj,
              options: undefined,
            },
            after: {
              maxIndex: 3,
              size: 4,
              isEmpty: false,
              obj: preloads.obj,
              options: undefined,
            },
          },
        },
      ],
    }, {
      msg: 'a valid args are given',
      param: [
        {
          msg: 'value is an object',
          rem: '',
          preloads: preloads,
          values: {
            index: 1,
            item: { __text: 'item 5' },
          },
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: true,
            },
            assertQty: 2,
            before: {
              maxIndex: 3,
              size: 4,
              isEmpty: false,
              obj: preloads.obj,
              options: undefined,
            },
            after: {
              maxIndex: 4,
              size: 5,
              isEmpty: false,
              obj: preloads.obj,
              obj: [
                { __text: 'item 1' },
                { __text: 'item 5' },
                { __text: 'item 2' },
                { __text: 'item 3' },
                { __text: 'item 4' },
              ],
            },
          },
        },
      ],
    },
  ],
};

module.exports = {
  preloads: preloads,
  descr: [
    valueTestDescr,
    rangeTestDescr,
    objTestDescr,
  ],
};
