// [v0.1.004-20230926]

/* class:    TXmlElementsListController
 * method:   loadItems
 * property: ---
 */

const EMPTY_STRING = '';
const EMPTY_OBJECT = {};
const EMPTY_ARRAY = [];
const EMPTY_ELEM = { __attr: {}, __text: '' };

const initElem = () => { return { __attr: {}, __text: '' } };

const preloads = {
  get obj(){ return []; },
  get options(){ return undefined; },
};

const valueTestDescr = {
  msg: 'run tests against "items" param',
  rem: '',
  param: [
    {
      msg: 'value is an object',
      param: [
        {
          msg: 'an empty element template is passed',
          rem: '',
          preloads: preloads,
          values: {
            items: initElem(),
            options: undefined,
          },
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: 1,
            },
            assertQty: 2,
            before: {
              size: 0,
              maxIndex: -1,
              isEmpty: true,
              obj: [],
              options: undefined,
            },
            after: {
              size: 1,
              maxIndex: 0,
              isEmpty: false,
              obj: [
                initElem(),
              ],
              options: undefined,
            },
          },
        },
      ],
    }, {
      msg: 'value is an array',
      param: [
        {
          msg: 'an empty array is passed',
          rem: '',
          preloads: preloads,
          values: {
            items: [],
            options: undefined,
          },
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: 0,
            },
            assertQty: 2,
            before: {
              size: 0,
              maxIndex: -1,
              isEmpty: true,
              obj: [],
              options: undefined,
            },
            after: {
              size: 0,
              maxIndex: -1,
              isEmpty: true,
              obj: [],
              options: undefined,
            },
          },
        }, {
          msg: 'a non-empty array is passed',
          rem: '',
          preloads: preloads,
          values: {
            items: [
              initElem(),
              initElem(),
            ],
            options: undefined,
          },
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: 2,
            },
            assertQty: 2,
            before: {
              size: 0,
              maxIndex: -1,
              isEmpty: true,
              obj: [],
              options: undefined,
            },
            after: {
              size: 2,
              maxIndex: 1,
              isEmpty: false,
              obj: [
                initElem(),
                initElem(),
              ],
              options: undefined,
            },
          },
        },
      ],
    },
  ],
};

const optTestDescr = {
  msg: 'run tests against "options" param',
  rem: '',
  param: [
    {
      msg: 'options: default',
      param: [
        {
          msg: 'perform test',
          rem: '',
          preloads: {
            get obj(){
              return [
                { __text: 'item 1' },
                { __text: 'item 2' },
              ];
            },
            get options(){ return undefined; },
          },
          values: {
            items: [
              { __text: 'item 3' },
              { __text: 'item 4' },
            ],
            options: undefined,
          },
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: 2,
            },
            assertQty: 2,
            before: {
              size: 2,
              maxIndex: 1,
              isEmpty: false,
              obj: [
                { __text: 'item 1' },
                { __text: 'item 2' },
              ],
              options: undefined,
            },
            after: {
              size: 2,
              maxIndex: 1,
              isEmpty: false,
              obj: [
                { __text: 'item 3' },
                { __text: 'item 4' },
              ],
              options: undefined,
            },
          },
        },
      ],
    }, {
      msg: 'options: object',
      param: [
        {
          msg: 'options: useClear = false',
          rem: '',
          preloads: {
            get obj(){
              return [
                { __text: 'item 1' },
                { __text: 'item 2' },
              ];
            },
            get options(){ return undefined; },
          },
          values: {
            items: [
              { __text: 'item 3' },
              { __text: 'item 4' },
            ],
            options: {
              useClear: false,
            },
          },
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: 2,
            },
            assertQty: 2,
            before: {
              size: 2,
              maxIndex: 1,
              isEmpty: false,
              obj: [
                { __text: 'item 1' },
                { __text: 'item 2' },
              ],
              options: undefined,
            },
            after: {
              size: 4,
              maxIndex: 3,
              isEmpty: false,
              obj: [
                { __text: 'item 1' },
                { __text: 'item 2' },
                { __text: 'item 3' },
                { __text: 'item 4' },
              ],
              options: undefined,
            },
          },
        }, {
          msg: 'options: useClear = true',
          rem: '',
          preloads: {
            get obj(){
              return [
                { __text: 'item 1' },
                { __text: 'item 2' },
              ];
            },
            get options(){ return undefined; },
          },
          values: {
            items: [
              { __text: 'item 3' },
              { __text: 'item 4' },
            ],
            options: {
              useClear: true,
            },
          },
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: 2,
            },
            assertQty: 2,
            before: {
              size: 2,
              maxIndex: 1,
              isEmpty: false,
              obj: [
                { __text: 'item 1' },
                { __text: 'item 2' },
              ],
              options: undefined,
            },
            after: {
              size: 2,
              maxIndex: 1,
              isEmpty: false,
              obj: [
                { __text: 'item 3' },
                { __text: 'item 4' },
              ],
              options: undefined,
            },
          },
        },
      ],
    },
  ],
};

module.exports = {
  preloads: preloads,
  descr: [
    valueTestDescr,
    optTestDescr,
  ],
};
