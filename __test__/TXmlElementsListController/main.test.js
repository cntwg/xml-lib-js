// [v0.1.015-20240624]

const { runTestFn } = require('#test-dir/test-hfunc.js');

const xmlDoc = require('#lib/xmldoc-lib.js');
const TXmlElementController = xmlDoc.TXmlElementController;
const TXmlElementsListController = xmlDoc.TXmlElementsListController;
//const TXmlAttributesMapper = xmlDoc.TXmlAttributesMapper;

const t_Dat = require('./data/index.data.js');

const __unwrap_ec = TXmlElementController.__unwrap;

// class: 'TXmlElementsListController'
describe('class: TXmlElementsListController', () => {
  describe('1. create a new instance (auto mode)', () => {
    const testFn = (...args) => { return new TXmlElementsListController(...args); };
    const testData = t_Dat.d1t;
    let testInst = null;
    describe.each(testData)('$msg', ({ param }) => {
      describe.each(param)('$msg', ({ preloads, param }) => {
        describe.each(param)('$msg', ({ obj, options, status }) => {
          beforeAll(() => {
            testInst = null;
            if (obj === undefined) obj = preloads.obj;
            if (options === undefined) options = preloads.options;
          });
          it('done', () => {
            expect(() => { testInst = testFn(obj, options); }).not.toThrow(TypeError);
          });
          it('check instance status', () => {
            expect(testInst instanceof xmlDoc[status.class]).toBe(true);
            expect(testInst.size).toBe(status.size);
            expect(testInst.maxIndex).toBe(status.maxIndex);
            expect(testInst.isEmpty()).toBe(status.isEmpty);
            expect(testInst.isNotEmpty()).toBe(!status.isEmpty);
            expect(obj).toStrictEqual(status.obj);
            expect(options).toStrictEqual(status.options);
          });
        });
      });
    });
  });

  describe('2. run tests aganst instance properties/methods', () => {
    describe.each([
      {
        msg: '2.1 - method: addItem()',
        method: 'addItem',
      }, {
        msg: '2.2 - method: loadItems()',
        method: 'loadItems',
      },
    ])('$msg', ({ method }) => {
      const testData = t_Dat.main[method];
      describe.each(testData.descr)('$msg $rem', ({ param }) => {
        describe.each(param)('$msg', ({ param }) => {
          describe.each(param)('$msg', ({ preloads, values, status }) => {
            const { before: stat_b, after: stat_a } = status;
            let testInst = null; let obj = null; let options = undefined;
            beforeAll(() => {
              ({ obj, options } = preloads);
              testInst = new TXmlElementsListController(obj, options);
            });
            it('check instance status (before)', () => {
              expect(testInst instanceof TXmlElementsListController).toBe(true);
              expect(testInst.size).toBe(stat_b.size);
              expect(testInst.maxIndex).toBe(stat_b.maxIndex);
              expect(testInst.isEmpty()).toBe(stat_b.isEmpty);
              expect(testInst.isNotEmpty()).toBe(!stat_b.isEmpty);
              expect(obj).toStrictEqual(stat_b.obj);
              expect(options).toStrictEqual(stat_b.options);
            });
            it('perform ops', () => {
              const { ops, assertQty } = status;
              let result = null; let isERR = false;
              try {
                result = runTestFn({ testInst, method }, values);
                //console.log('CHECK: => NO_ERR');
              } catch (err) {
                //console.log('CHECK: IS_ERR => '+err);
                isERR = true;
                result = err;
              } finally {
                expect.assertions(assertQty);
                expect(isERR).toBe(ops.isERR);
                if (isERR) {
                  const { errType, errCode } = ops;
                  expect(result instanceof errType).toBe(true);
                  expect(result.code).toStrictEqual(errCode);
                } else {
                  const { className, value } = ops;
                  if (typeof className === 'string') {
                    expect(result instanceof xmlDoc[className]).toBe(true);
                  } else {
                    expect(result).toStrictEqual(value);
                  };
                };
              };
            });
            it('check instance status (after)', () => {
              expect(testInst instanceof TXmlElementsListController).toBe(true);
              expect(testInst.size).toBe(stat_a.size);
              expect(testInst.maxIndex).toBe(stat_a.maxIndex);
              expect(testInst.isEmpty()).toBe(stat_a.isEmpty);
              expect(testInst.isNotEmpty()).toBe(!stat_a.isEmpty);
              expect(obj).toStrictEqual(stat_a.obj);
              expect(options).toStrictEqual(stat_a.options);
            });
          });
        });
      });
    });

    describe.each([
      {
        msg: '2.3 - method: chkIndex()',
        method: 'chkIndex',
      },
    ])('$msg', ({ method }) => {
      const testData = t_Dat.main[method];
      describe.each(testData.descr)('$msg $rem', ({ param }) => {
        describe.each(param)('$msg', ({ param }) => {
          describe.each(param)('$msg $rem', ({ preloads, values, status }) => {
            const { before: stat_b, after: stat_a } = status;
            let testInst = null; let obj = null; let options = undefined;
            beforeAll(() => {
              ({ obj, options } = preloads);
              testInst = new TXmlElementsListController(obj, options);
            });
            it('check instance status', () => {
              expect(testInst instanceof TXmlElementsListController).toBe(true);
              expect(testInst.size).toBe(status.size);
              expect(testInst.maxIndex).toBe(status.maxIndex);
              expect(testInst.isEmpty()).toBe(status.isEmpty);
              expect(testInst.isNotEmpty()).toBe(!status.isEmpty);
              expect(obj).toStrictEqual(status.obj);
              expect(options).toStrictEqual(status.options);
            });
            it('perform ops', () => {
              const { ops, assertQty } = status;
              let result = null; let isERR = false;
              try {
                result = runTestFn({ testInst, method }, values);
                //console.log('CHECK: => NO_ERR');
              } catch (err) {
                //console.log('CHECK: IS_ERR => '+err);
                isERR = true;
                result = err;
              } finally {
                expect.assertions(assertQty);
                expect(isERR).toBe(ops.isERR);
                if (isERR) {
                  const { errType, errCode } = ops;
                  expect(result instanceof errType).toBe(true);
                  expect(result.code).toStrictEqual(errCode);
                } else {
                  const { className, value } = ops;
                  if (typeof className === 'string') {
                    expect(result instanceof xmlDoc[className]).toBe(true);
                  } else {
                    expect(result).toStrictEqual(value);
                  };
                };
              };
            });
          });
        });
      });
    });

    describe.each([
      {
        msg: '2.4 - method: getItem()',
        method: 'getItem',
      },
    ])('$msg', ({ method }) => {
      const testData = t_Dat.main[method];
      describe.each(testData.descr)('$msg $rem', ({ param }) => {
        describe.each(param)('$msg', ({ param }) => {
          describe.each(param)('$msg $rem', ({ preloads, values, status }) => {
            const { before: stat_b, after: stat_a } = status;
            let testInst = null; let obj = null; let options = undefined;
            beforeAll(() => {
              ({ obj, options } = preloads);
              testInst = new TXmlElementsListController(obj, options);
            });
            it('check instance status (before)', () => {
              expect(testInst instanceof TXmlElementsListController).toBe(true);
              expect(obj).toStrictEqual(stat_b.obj);
              expect(options).toStrictEqual(stat_b.options);
            });
            it('perform ops', () => {
              const { ops, assertQty } = status;
              let result = null; let isERR = false;
              try {
                result = runTestFn({ testInst, method }, values);
                //console.log('CHECK: => NO_ERR');
              } catch (err) {
                //console.log('CHECK: IS_ERR => '+err);
                isERR = true;
                result = err;
              } finally {
                expect.assertions(assertQty);
                expect(isERR).toBe(ops.isERR);
                if (isERR) {
                  const { errType, errCode } = ops;
                  expect(result instanceof errType).toBe(true);
                  expect(result.code).toStrictEqual(errCode);
                } else {
                  const { className, value } = ops;
                  if (typeof className === 'string') {
                    expect(result instanceof xmlDoc[className]).toBe(true);
                    if (className === 'TXmlElementController') {
                      expect(result.textValue).toStrictEqual(value);
                    };
                  } else {
                    expect(result).toStrictEqual(value);
                  };
                };
              };
            });
          });
        });
      });
    });

    describe.each([
      {
        msg: '2.5 - method: setItem()',
        method: 'setItem',
      }, {
        msg: '2.6 - method: insItem()',
        method: 'insItem',
      }, {
        msg: '2.7 - method: delItem()',
        method: 'delItem',
      }, {
        msg: '2.8 - method clear()',
        method: 'clear',
      },
    ])('$msg', ({ method }) => {
      const testData = t_Dat.main[method];
      describe.each(testData.descr)('$msg $rem', ({ param }) => {
        describe.each(param)('$msg', ({ param }) => {
          describe.each(param)('$msg $rem', ({ preloads, values, status }) => {
            const { before: stat_b, after: stat_a } = status;
            let testInst = null; let obj = null; let options = undefined;
            beforeAll(() => {
              ({ obj, options } = preloads);
              testInst = new TXmlElementsListController(obj, options);
            });
            it('check instance status (before)', () => {
              expect(testInst instanceof TXmlElementsListController).toBe(true);
              expect(testInst.size).toBe(stat_b.size);
              expect(testInst.maxIndex).toBe(stat_b.maxIndex);
              expect(testInst.isEmpty()).toBe(stat_b.isEmpty);
              expect(testInst.isNotEmpty()).toBe(!stat_b.isEmpty);
              expect(obj).toStrictEqual(stat_b.obj);
              expect(options).toStrictEqual(stat_b.options);
            });
            it('perform ops', () => {
              const { ops, assertQty } = status;
              let result = null; let isERR = false;
              try {
                result = runTestFn({ testInst, method }, values);
                //console.log('CHECK: => NO_ERR');
              } catch (err) {
                //console.log('CHECK: IS_ERR => '+err);
                isERR = true;
                result = err;
              } finally {
                expect.assertions(assertQty);
                expect(isERR).toBe(ops.isERR);
                if (isERR) {
                  const { errType, errCode } = ops;
                  expect(result instanceof errType).toBe(true);
                  expect(result.code).toStrictEqual(errCode);
                } else {
                  const { className, value } = ops;
                  if (typeof className === 'string') {
                    expect(result instanceof xmlDoc[className]).toBe(true);
                  } else {
                    expect(result).toStrictEqual(value);
                  };
                };
              };
            });
            it('check instance status (after)', () => {
              expect(testInst instanceof TXmlElementsListController).toBe(true);
              expect(testInst.size).toBe(stat_a.size);
              expect(testInst.maxIndex).toBe(stat_a.maxIndex);
              expect(testInst.isEmpty()).toBe(stat_a.isEmpty);
              expect(testInst.isNotEmpty()).toBe(!stat_a.isEmpty);
              expect(obj).toStrictEqual(stat_a.obj);
              expect(options).toStrictEqual(stat_a.options);
            });
          });
        });
      });
    });

    /**/
  });
});
