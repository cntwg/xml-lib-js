// [v0.1.003-20230921]

/* class:    TXmlElementController
 * method:   clear
 * property: ---
 */

const EMPTY_STRING = '';
const EMPTY_OBJECT = {};
const EMPTY_ELEM = { __attr: {}, __text: '' };

const initHost = () => { return { __attr: {}, __text: '' } };

const preloads = {
  get obj(){
    return {
      __attr: { lang: 'en' },
      __text: 'some text value',
    };
  },
  get options(){ return undefined; },
};

const resultTestDescr = {
  msg: 'run tests against result values',
  rem: '',
  param: [
    {
      msg: 'some elements was loaded',
      param: [
        {
          msg: 'perform test',
          preloads: preloads,
          values: {
            value: undefined,
          },
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: undefined,
            },
            assertQty: 2,
            before: {
              unwrapped: {
                __attr: { lang: 'en' },
                __text: 'some text value',
              },
              obj: {
                __attr: { lang: 'en' },
                __text: 'some text value',
              },
              options: undefined,
            },
            after: {
              unwrapped: {},
              obj: {},
              options: undefined,
            },
          },
        },
      ],
    },
  ],
};

module.exports = {
  preloads: preloads,
  descr: [
    resultTestDescr,
  ],
};
