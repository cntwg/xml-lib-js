// [v0.1.006-20240701]

/* class:    TXmlElementController
 * method:   delChild
 * property: ---
 */

const { genTestCase } = require('#test-dir/test-hfunc.js');

const { TXmlElementController } = require('#lib/xmldoc-lib.js');

const __unwrap_ec = TXmlElementController.__unwrap;

const preloads = {
  TEST_ELEMENT_1: {
    get obj(){
      return {
        __attr: {},
        __text: '',
        item: {
          __attr: { id: '1' },
          __text: 'first',
        },
        tools: [],
      };
    },
    get options(){
      return undefined;
    },
    status: {
      obj: {
        __attr: {},
        __text: '',
        item: {
          __attr: { id: '1' },
          __text: 'first',
        },
        tools: [],
      },
      options: undefined,
      __unwrap: undefined,
    },
  },
  TEST_ELEMENT_2: {
    get obj(){
      return {
        __attr: {},
        __text: '',
        item: {
          __attr: { id: '1' },
          __text: 'first',
        },
        tools: [
          {
            __attr: { id: '2' },
            __text: 'second',
          },
        ],
      };
    },
    get options(){
      return undefined;
    },
    status: {
      obj: {
        __attr: {},
        __text: '',
        item: {
          __attr: { id: '1' },
          __text: 'first',
        },
        tools: [
          {
            __attr: { id: '2' },
            __text: 'second',
          },
        ],
      },
      options: undefined,
      __unwrap: undefined,
    },
  },
  TEST_ELEMENT_3: {
    get obj(){
      return {
        __attr: {},
        __text: '',
        item: {
          __attr: { id: '1' },
          __text: 'first',
        },
        tools: [
          {
            __attr: { id: '2' },
            __text: 'second',
          }, {
            __attr: { id: '3' },
            __text: 'third',
          },
        ],
      };
    },
    get options(){
      return undefined;
    },
    status: {
      obj: {
        __attr: {},
        __text: '',
        item: {
          __attr: { id: '1' },
          __text: 'first',
        },
        tools: [
          {
            __attr: { id: '2' },
            __text: 'second',
          }, {
            __attr: { id: '3' },
            __text: 'third',
          },
        ],
      },
      options: undefined,
      __unwrap: undefined,
    },
  },
  BAD_ELEMENT: {
    get obj(){
      return {
        __attr: {},
        __text: '',
        item: 'first',
      };
    },
    get options(){
      return undefined;
    },
    status: {
      obj: {
        __attr: {},
        __text: '',
        item: 'first',
      },
      options: undefined,
      __unwrap: undefined,
    },
  },
};

const valueTestDescr = {
  msg: 'run tests against "value" param',
  rem: '',
  param: [
    ...genTestCase({
      inGroup: 'no args are given',
      preloads: preloads.TEST_ELEMENT_1,
      cases: [
        {
          msg: 'undefined value is passed',
          rem: '',
          values: {
            name: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: false,
          __unwrap: __unwrap_ec,
        },
        assertQty: 2,
        before: undefined,
        after: preloads.TEST_ELEMENT_1.status,
      },
    }, { auto: true }),
    ...genTestCase({
      inGroup: 'a non-valid args are given',
      preloads: preloads.TEST_ELEMENT_1,
      cases: [
        {
          msg: 'value is a "null"',
          rem: '',
          values: {
            name: null,
          },
        }, {
          msg: 'value is a boolean',
          rem: '',
          values: {
            name: true,
          },
        }, {
          msg: 'value is a number',
          rem: '',
          values: {
            name: 2,
          },
        }, {
          msg: 'value is an empty string',
          rem: '',
          values: {
            name: '',
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: false,
          __unwrap: __unwrap_ec,
        },
        assertQty: 2,
        before: undefined,
        after: preloads.TEST_ELEMENT_1.status,
      },
    }, { auto: true }),
    ...genTestCase({
      inGroup: 'a valid args are given',
      preloads: preloads.TEST_ELEMENT_1,
      cases: [
        {
          msg: 'value is a string',
          rem: '(non-empty)',
          values: {
            name: 'item',
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: true,
          __unwrap: __unwrap_ec,
        },
        assertQty: 2,
        before: undefined,
        after: {
          obj: {
            __attr: {},
            __text: '',
            tools: [],
          },
          options: undefined,
          __unwrap: undefined,
        },
      },
    }, { auto: true }),
    ...genTestCase({
      inGroup: 'a special args are given',
      preloads: preloads.TEST_ELEMENT_1,
      cases: [
        {
          msg: 'value is a "NaN"',
          rem: '',
          values: {
            name: NaN,
          },
        }, {
          msg: 'value is a number',
          rem: '',
          values: {
            name: 2,
          },
        }, {
          msg: 'value is an string',
          rem: '(convertable to a positive number)',
          values: {
            name: '3',
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: false,
          __unwrap: __unwrap_ec,
        },
        assertQty: 2,
        before: undefined,
        after: preloads.TEST_ELEMENT_1.status,
      },
    }, { auto: true }),
  ],
};

const resultTestDescr = {
  msg: 'run tests against result values',
  rem: '(constructor: default options is used)',
  param: [
    ...genTestCase({
      inGroup: 'constructor: host is an element',
      preloads: preloads.TEST_ELEMENT_1,
      cases: [
        {
          msg: 'target child is not exists',
          rem: '',
          values: {
            name: 'note',
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: false,
          __unwrap: __unwrap_ec,
        },
        assertQty: 2,
        before: undefined,
        after: preloads.TEST_ELEMENT_1.status,
      },
    }, { auto: true }),
    ...genTestCase({
      inGroup: 'constructor: host is an element',
      preloads: preloads.TEST_ELEMENT_1,
      cases: [
        {
          msg: 'target child is exists',
          rem: '(child is an object)',
          values: {
            name: 'item',
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: true,
          __unwrap: __unwrap_ec,
        },
        assertQty: 2,
        before: undefined,
        after: {
          obj: {
            __attr: {},
            __text: '',
            tools: [],
          },
          options: undefined,
          __unwrap: undefined,
        },
      },
    }, { auto: true }),
    ...genTestCase({
      inGroup: 'constructor: host is an element',
      preloads: preloads.TEST_ELEMENT_1,
      cases: [
        {
          msg: 'target child is exists',
          rem: '(child is an empty array)',
          values: {
            name: 'tools',
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: true,
          __unwrap: undefined,
        },
        assertQty: 2,
        before: undefined,
        after: {
          obj: {
            __attr: {},
            __text: '',
            item: {
              __attr: { id: '1' },
              __text: 'first',
            },
          },
          options: undefined,
          __unwrap: undefined,
        },
      },
    }, { auto: true }),
    ...genTestCase({
      inGroup: 'constructor: host is an element',
      preloads: preloads.TEST_ELEMENT_2,
      cases: [
        {
          msg: 'target child is exists',
          rem: '(child is an array with a single element)',
          values: {
            name: 'tools',
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: true,
          __unwrap: undefined,
        },
        assertQty: 2,
        before: undefined,
        after: {
          obj: {
            __attr: {},
            __text: '',
            item: {
              __attr: { id: '1' },
              __text: 'first',
            },
          },
          options: undefined,
          __unwrap: undefined,
        },
      },
    }, { auto: true }),
    ...genTestCase({
      inGroup: 'constructor: host is an element',
      preloads: preloads.TEST_ELEMENT_3,
      cases: [
        {
          // // TODO: [?] consider method behavior
          msg: 'target child is exists',
          rem: '(child is an array with a few elements) [*]',
          values: {
            name: 'tools',
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: false,
          __unwrap: undefined,
        },
        assertQty: 2,
        before: undefined,
        after: preloads.TEST_ELEMENT_3.status,
      },
    }, { auto: true }),
    ...genTestCase({
      inGroup: 'constructor: host is an element',
      preloads: preloads.BAD_ELEMENT,
      cases: [
        {
          msg: 'target child is exists',
          rem: '(but is nor an object nor an array)',
          values: {
            name: 'item',
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: true,
          __unwrap: undefined,
        },
        assertQty: 2,
        before: undefined,
        after: {
          obj: {
            __attr: {},
            __text: '',
          },
          options: undefined,
          __unwrap: undefined,
        },
      },
    }, { auto: true }),
  ],
};

module.exports = {
  descr: [
    valueTestDescr,
    resultTestDescr,
  ],
  tests: [
    valueTestDescr,
    resultTestDescr,
  ],
};
