// [v0.1.005-20240701]

/* class:    TXmlElementController
 * method:   addChild
 * property: ---
 */

const { genTestCase } = require('#test-dir/test-hfunc.js');

const { TXmlElementController } = require('#lib/xmldoc-lib.js');

const __unwrap_ec = TXmlElementController.__unwrap;

const preloads = {
  EMPTY_ELEMENT: {
    get obj(){
      return {
        __attr: {},
        __text: '',
      };
    },
    get options(){
      return undefined;
    },
    status: {
      obj: {
        __attr: {},
        __text: '',
      },
      options: undefined,
      __unwrap: undefined,
    },
  },
  TEST_ELEMENT: {
    get obj(){
      return {
        __attr: {},
        __text: '',
        item: {
          __attr: { id: '1' },
          __text: 'first',
        },
        tools: [
          {
            __attr: { id: '2' },
            __text: 'second',
          },
        ],
      };
    },
    get options(){
      return undefined;
    },
    status: {
      obj: {
        __attr: {},
        __text: '',
        item: {
          __attr: { id: '1' },
          __text: 'first',
        },
        tools: [
          {
            __attr: { id: '2' },
            __text: 'second',
          },
        ],
      },
      options: undefined,
      __unwrap: undefined,
    },
  },
  BAD_ELEMENT: {
    get obj(){
      return {
        __attr: {},
        __text: '',
        item: 'first',
      };
    },
    get options(){
      return undefined;
    },
    status: {
      obj: {
        __attr: {},
        __text: '',
        item: 'first',
      },
      options: undefined,
      __unwrap: undefined,
    },
  },
};

const valueTestDescr = {
  msg: 'run tests against "value" param',
  rem: '',
  param: [
    ...genTestCase({
      inGroup: 'no args are given',
      preloads: preloads.EMPTY_ELEMENT,
      cases: [
        {
          msg: 'undefined value is passed',
          rem: '',
          values: {
            name: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: null,
          __unwrap: __unwrap_ec,
        },
        assertQty: 2,
        before: undefined,
        after: preloads.EMPTY_ELEMENT.status,
      },
    }, { auto: true }),
    ...genTestCase({
      inGroup: 'a non-valid args are given',
      preloads: preloads.EMPTY_ELEMENT,
      cases: [
        {
          msg: 'value is a "null"',
          rem: '',
          values: {
            name: null,
          },
        }, {
          msg: 'value is a boolean',
          rem: '',
          values: {
            name: true,
          },
        }, {
          msg: 'value is a number',
          rem: '',
          values: {
            name: 2,
          },
        }, {
          msg: 'value is an empty string',
          rem: '',
          values: {
            name: '',
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: null,
          __unwrap: __unwrap_ec,
        },
        assertQty: 2,
        before: undefined,
        after: preloads.EMPTY_ELEMENT.status,
      },
    }, { auto: true }),
    ...genTestCase({
      inGroup: 'a valid args are given',
      preloads: preloads.EMPTY_ELEMENT,
      cases: [
        {
          msg: 'value is a string',
          rem: '(non-empty)',
          values: {
            name: 'tools',
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: 'TXmlElementController',
          value: {},
          __unwrap: __unwrap_ec,
        },
        assertQty: 3,
        before: undefined,
        after: {
          obj: {
            __attr: {},
            __text: '',
            tools: {},
          },
          options: undefined,
          __unwrap: undefined,
        },
      },
    }, { auto: true }),
    ...genTestCase({
      inGroup: 'a special args are given',
      preloads: preloads.EMPTY_ELEMENT,
      cases: [
        {
          msg: 'value is a "NaN"',
          rem: '',
          values: {
            name: NaN,
          },
        }, {
          msg: 'value is a number',
          rem: '',
          values: {
            name: 2,
          },
        }, {
          msg: 'value is an string',
          rem: '(convertable to a positive number)',
          values: {
            name: '3',
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: null,
          __unwrap: __unwrap_ec,
        },
        assertQty: 2,
        before: undefined,
        after: preloads.EMPTY_ELEMENT.status,
      },
    }, { auto: true }),
  ],
};

const resultTestDescr = {
  msg: 'run tests against result values',
  rem: '(constructor: default options is used)',
  param: [
    ...genTestCase({
      inGroup: 'constructor: host is an element',
      preloads: preloads.EMPTY_ELEMENT,
      cases: [
        {
          msg: 'none child is exists',
          rem: '',
          values: {
            name: 'note',
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: 'TXmlElementController',
          value: {},
          __unwrap: __unwrap_ec,
        },
        assertQty: 3,
        before: undefined,
        after: {
          obj: {
            __attr: {},
            __text: '',
            note: {},
          },
          options: undefined,
          __unwrap: undefined,
        },
      },
    }, { auto: true }),
    ...genTestCase({
      inGroup: 'constructor: host is an element',
      preloads: preloads.TEST_ELEMENT,
      cases: [
        {
          msg: 'target child is exists',
          rem: '(child is an object)',
          values: {
            name: 'item',
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: 'TXmlElementController',
          value: {},
          __unwrap: __unwrap_ec,
        },
        assertQty: 3,
        before: undefined,
        after: {
          obj: {
            __attr: {},
            __text: '',
            item: [
              {
                __attr: { id: '1' },
                __text: 'first',
              },
              {},
            ],
            tools: [
              {
                __attr: { id: '2' },
                __text: 'second',
              },
            ],
          },
          options: undefined,
          __unwrap: undefined,
        },
      },
    }, { auto: true }),
    ...genTestCase({
      inGroup: 'constructor: host is an element',
      preloads: preloads.TEST_ELEMENT,
      cases: [
        {
          msg: 'target child is exists',
          rem: '(child is an array)',
          values: {
            name: 'tools',
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: 'TXmlElementController',
          value: {},
          __unwrap: __unwrap_ec,
        },
        assertQty: 3,
        before: undefined,
        after: {
          obj: {
            __attr: {},
            __text: '',
            item: {
              __attr: { id: '1' },
              __text: 'first',
            },
            tools: [
              {
                __attr: { id: '2' },
                __text: 'second',
              },
              {},
            ],
          },
          options: undefined,
          __unwrap: undefined,
        },
      },
    }, { auto: true }),
    ...genTestCase({
      inGroup: 'constructor: host is an element',
      preloads: preloads.BAD_ELEMENT,
      cases: [
        {
          // // TODO: consider if that is suitable
          msg: 'target child is exists',
          rem: '(but is nor an object nor an array) [*]',
          values: {
            name: 'item',
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: null,
          __unwrap: undefined,
        },
        assertQty: 2,
        before: undefined,
        after: {
          obj: {
            __attr: {},
            __text: '',
            item: 'first',
          },
          options: undefined,
          __unwrap: undefined,
        },
      },
    }, { auto: true }),
  ],
};

module.exports = {
  descr: [
    valueTestDescr,
    resultTestDescr,
  ],
  tests: [
    valueTestDescr,
    resultTestDescr,
  ],
};
