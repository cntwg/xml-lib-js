// [v0.1.001-20240918]

/* class:    TXmlElementController
 * method:   loadFromXMLString
 * property: ---
 */

const { genTestCase } = require('#test-dir/test-hfunc.js');

//const { TXmlElementController } = require('#lib/xmldoc-lib.js');

//const __unwrap_ec = TXmlElementController.__unwrap;

const preloads = {
  EMPTY_ELEMENT: {
    get obj(){
      return {
        __attr: {},
        __text: '',
      };
    },
    get options(){
      return undefined;
    },
    status: {
      obj: {
        __attr: {},
        __text: '',
      },
      options: undefined,
      __unwrap: undefined,
    },
  },
  TEST_ELEMENT: {
    get obj(){
      return {
        __attr: {},
        __text: '',
        item: {
          __attr: { id: '1' },
          __text: 'first',
        },
        tools: [
          {
            __attr: { id: '2' },
            __text: 'second',
          },
        ],
      };
    },
    get options(){
      return undefined;
    },
    status: {
      obj: {
        __attr: {},
        __text: '',
        item: {
          __attr: { id: '1' },
          __text: 'first',
        },
        tools: [
          {
            __attr: { id: '2' },
            __text: 'second',
          },
        ],
      },
      options: undefined,
      __unwrap: undefined,
    },
  },
};

const valueTestDescr = {
  msg: 'run tests against "value" param',
  rem: '',
  param: [
    ...genTestCase({
      inGroup: 'no args are given',
      preloads: preloads.EMPTY_ELEMENT,
      cases: [
        {
          msg: 'undefined value is passed',
          rem: '',
          values: {
            name: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: false,
          __unwrap: undefined,//__unwrap_ec,
        },
        assertQty: 2,
        before: undefined,
        after: preloads.EMPTY_ELEMENT.status,
      },
    }, { auto: true }),
    ...genTestCase({
      inGroup: 'a non-valid args are given',
      preloads: preloads.EMPTY_ELEMENT,
      cases: [
        {
          msg: 'value is a "null"',
          rem: '',
          values: {
            name: null,
          },
        }, {
          msg: 'value is an empty string',
          rem: '',
          values: {
            name: '',
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: false,
          __unwrap: undefined,//__unwrap_ec,
        },
        assertQty: 2,
        before: undefined,
        after: preloads.EMPTY_ELEMENT.status,
      },
    }, { auto: true }),
    ...genTestCase({
      inGroup: 'a valid args are given',
      preloads: preloads.EMPTY_ELEMENT,
      cases: [
        {
          msg: 'value is a string',
          rem: '(non-empty)',
          values: {
            name: [
              '<tool size="big" color="black">big black tool</tool>',
            ].join(''),
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,//'TXmlElementController',
          value: true,
          __unwrap: undefined,//__unwrap_ec,
        },
        assertQty: 2,
        before: undefined,
        after: {
          obj: {
            __attr: { size: 'big', color: 'black' },
            __text: 'big black tool',
            //tools: {},
          },
          options: undefined,
          __unwrap: undefined,
        },
      },
    }, { auto: true }),
  ],
};

module.exports = {
  //descr: [],
  tests: [
    valueTestDescr,
  ],
};
