// [v0.1.020-20240929]

/* class:    TXmlElementController
 * method:   ---
 * property: ---
 */

module.exports = {
  ['$constructor']: require('./constructor.data.js'),
  main: {
    hasAttribute: require('./hasAttribute.data.js'),
    getAttribute: require('./getAttribute.data.js'),
    setAttribute: require('./setAttribute.data.js'),
    delAttribute: require('./delAttribute.data.js'),
    renameAttribute: require('./renameAttribute.data.js'),
    getTextValue: require('./getTextValue.data.js'),
    setTextValue: require('./setTextValue.data.js'),
    hasChild: require('./hasChild.data.js'),
    getChild: require('./getChild.data.js'),
    addChild: require('./addChild.data.js'),
    delChild: require('./delChild.data.js'),
    clear: require('./clear.data.js'),
    loadFromXMLString: require('./loadFromXMLString.data.js'),
  },
  class_static: {
    create: require('./create.data.js'),
    __getChildRaw: require('./__getChildRaw.data.js'),
    __addChildRaw: require('./__addChildRaw.data.js'),
    __setChildRaw: require('./__setChildRaw.data.js'),
  },
  prop: {
    ['$getter']: {
      attributes: require('./getProp.attributes.data.js'),
    },
    ['$setter']: {
      textValue: require('./setProp.textValue.data.js'),
    },
  },
  getProp: {
    textValue: require('./getProp.textValue.data.js'),
  },
  setProp: {},
};
