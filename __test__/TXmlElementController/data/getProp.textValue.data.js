// [v0.1.004-20230222]

/* class:    TXmlElementController
 * method:   $getter
 * property: textValue
 */

//const preloads = {};

const resultTestDescr = {
  msg: 'run tests against result values',
  param: [
    {
      msg: 'constructor: default options is used',
      param: [
        ({
          msg: 'constructor: host is an empty object',
          rem: '',
          preloads: {
            get obj(){
              return {};
            },
            get options(){
              return undefined;
            },
            status: {
              obj: {},
              options: undefined,
              __unwrap: undefined,
            },
          },
          values: undefined,
          status: {
            ops: {
              isERR: false,
              value: '',
            },
            before: undefined,
            after: undefined,
          },
          initTest(){
            this.status.before = this.preloads.status;
            return this;
          },
        }).initTest(),
        ({
          msg: 'constructor: host is an empty element',
          rem: '',
          preloads: {
            get obj(){
              return {
                __attr: {},
                __text: '',
              };
            },
            get options(){
              return undefined;
            },
            status: {
              obj: {
                __attr: {},
                __text: '',
              },
              options: undefined,
              __unwrap: undefined,
            },
          },
          values: undefined,
          status: {
            ops: {
              isERR: false,
              value: '',
            },
            before: undefined,
            after: undefined,
          },
          initTest(){
            this.status.before = this.preloads.status;
            return this;
          },
        }).initTest(),
        ({
          msg: 'constructor: host is an element',
          rem: '("textValue" is set)',
          preloads: {
            get obj(){
              return {
                __attr: {},
                __text: 'some text value',
              };
            },
            get options(){
              return undefined;
            },
            status: {
              obj: {
                __attr: {},
                __text: 'some text value',
              },
              options: undefined,
              __unwrap: undefined,
            },
          },
          values: undefined,
          status: {
            ops: {
              isERR: false,
              value: 'some text value',
            },
            before: undefined,
            after: undefined,
          },
          initTest(){
            this.status.before = this.preloads.status;
            return this;
          },
        }).initTest(),
        ({
          msg: 'constructor: host is an element',
          rem: '("textValue" and "lang" attribute are set)',
          preloads: {
            get obj(){
              return {
                __attr: { lang: 'en' },
                __text: 'some text value',
              };
            },
            get options(){
              return undefined;
            },
            status: {
              obj: {
                __attr: { lang: 'en' },
                __text: 'some text value',
              },
              options: undefined,
              __unwrap: undefined,
            },
          },
          values: undefined,
          status: {
            ops: {
              isERR: false,
              value: 'some text value',
            },
            before: undefined,
            after: undefined,
          },
          initTest(){
            this.status.before = this.preloads.status;
            return this;
          },
        }).initTest(),
      ],
    },
  ],
};

module.exports = {
  //preloads: preloads,
  descr: [
    resultTestDescr,
  ],
};
