// [v0.1.001-20240929]

/* class:    TXmlElementController
 * method:   __getChildRaw
 * property: ---
 */

const { genTestCase } = require('#test-dir/test-hfunc.js');

const { TXmlElementController } = require('#lib/xmldoc-lib.js');

const __unwrap_ec = TXmlElementController.__unwrap;

const preloads = {
  TEST_ELEMENT_2: {
    get obj() {
      return {
        __attr: {},
        __text: '',
        item: {
          __attr: { id: '1' },
          __text: 'first',
        },
        tools: [
          {
            __attr: { id: '2' },
            __text: 'second',
          },
        ],
      };
    },
    get options(){
      return undefined;
    },
    status: {
      obj: {
        __attr: {},
        __text: '',
        item: {
          __attr: { id: '1' },
          __text: 'first',
        },
        tools: [
          {
            __attr: { id: '2' },
            __text: 'second',
          },
        ],
      },
      options: undefined,
      __unwrap: undefined,
    },
  },
};

const nodeTestDescr = {
  msg: 'run tests against "node" param',
  rem: '',
  param: [
    ...genTestCase({
      inGroup: 'no args are given',
      preloads: undefined,
      cases: [
        {
          msg: 'undefined value is passed',
          rem: '',
          values: {
            node: undefined,
            name: 'new_item',
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: null,
          __unwrap: undefined,
        },
        assertQty: 2,
        before: {
          node: null,
        },
        after: {
          node: null,
        },
      },
    }, { auto: true }),
    ...genTestCase({
      inGroup: 'non-valid args are given',
      preloads: undefined,
      cases: [
        {
          msg: 'value is a "null"',
          rem: '',
          values: {
            node: null,
            name: 'new_item',
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: null,
          __unwrap: undefined,
        },
        assertQty: 2,
        before: {
          node: null,
        },
        after: {
          node: null,
        },
      },
    }, { auto: true }),
    ...genTestCase({
      inGroup: 'non-valid args are given',
      preloads: undefined,
      cases: [
        {
          msg: 'value is an object',
          rem: '',
          values: {
            node: {},
            name: 'new_item',
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: null,
          __unwrap: undefined,
        },
        assertQty: 2,
        before: {
          node: null,
        },
        after: {
          node: null,
        },
      },
    }, { auto: true }),
    ...genTestCase({
      inGroup: 'non-valid args are given',
      preloads: undefined,
      cases: [
        {
          msg: 'value is an array',
          rem: '',
          values: {
            node: [],
            name: 'new_item',
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: null,
          __unwrap: undefined,
        },
        assertQty: 2,
        before: {
          node: null,
        },
        after: {
          node: null,
        },
      },
    }, { auto: true }),
    ...genTestCase({
      inGroup: 'valid args are given',
      preloads: undefined,
      cases: [
        {
          msg: 'value is an instance of "TXmlElementController"',
          rem: '',
          values: {
            node: new TXmlElementController({
              target_item: { __text: 'target item value' },
            }),
            name: 'target_item',
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: { __text: 'target item value' },
          __unwrap: undefined,
        },
        assertQty: 2,
        before: {
          node: {
            target_item: { __text: 'target item value' },
          },
        },
        after: {
          node: {
            target_item: { __text: 'target item value' },
          },
        },
      },
    }, { auto: true }),
  ],
};

const nameTestDescr = {
  msg: 'run tests against "name" param',
  rem: '',
  param: [
    ...genTestCase({
      inGroup: 'no args are given',
      preloads: undefined,
      cases: [
        {
          msg: 'undefined value is passed',
          rem: '',
          values: {
            node: new TXmlElementController({
              target_item: { __text: 'target item value' },
            }),
            name: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: null,
          __unwrap: undefined,
        },
        assertQty: 2,
        after: {
          node: {
            target_item: { __text: 'target item value' },
          },
        },
        before: {
          node: {
            target_item: { __text: 'target item value' },
          },
        },
      },
    }, { auto: true }),
    ...genTestCase({
      inGroup: 'non-valid args are given',
      preloads: undefined,
      cases: [
        {
          msg: 'value is a "null"',
          rem: '',
          values: {
            node: new TXmlElementController({
              target_item: { __text: 'target item value' },
            }),
            name: null,
          },
        }, {
          msg: 'value is an empty string',
          rem: '',
          values: {
            node: new TXmlElementController({
              target_item: { __text: 'target item value' },
            }),
            name: '',
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: null,
          __unwrap: undefined,
        },
        assertQty: 2,
        before: {
          node: {
            target_item: { __text: 'target item value' },
          },
        },
        after: {
          node: {
            target_item: { __text: 'target item value' },
          },
        },
      },
    }, { auto: true }),
    ...genTestCase({
      inGroup: 'valid args are given',
      preloads: undefined,
      cases: [
        {
          msg: 'value is a non-empty string',
          rem: '',
          values: {
            node: new TXmlElementController({
              target_item: { __text: 'target item value' },
            }),
            name: 'target_item',
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: { __text: 'target item value' },
          __unwrap: undefined,
        },
        assertQty: 2,
        before: {
          node: {
            target_item: { __text: 'target item value' },
          },
        },
        after: {
          node: {
            target_item: { __text: 'target item value' },
          },
        },
      },
    }, { auto: true }),
  ],
};

const resultTestDescr = {
  msg: 'run tests against result values',
  rem: '',
  param: [
    ...genTestCase({
      inGroup: 'tests against target child existance',
      preloads: undefined,
      cases: [
        {
          msg: 'child is not exists',
          rem: '',
          values: {
            node: new TXmlElementController({}),
            name: 'target_item',
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: null,
          __unwrap: undefined,
        },
        assertQty: 2,
        before: {
          node: {},
        },
        after: {
          node: {},
        },
      },
    }, { auto: true }),
    ...genTestCase({
      inGroup: 'tests against target child existance',
      preloads: undefined,
      cases: [
        {
          msg: 'child is exists',
          rem: '',
          values: {
            node: new TXmlElementController({
              target_item: { __text: 'target item value' },
            }),
            name: 'target_item',
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: { __text: 'target item value' },
          __unwrap: undefined,
        },
        assertQty: 2,
        before: {
          node: {
            target_item: { __text: 'target item value' },
          },
        },
        after: {
          node: {
            target_item: { __text: 'target item value' },
          },
        },
      },
    }, { auto: true }),
    ...genTestCase({
      inGroup: 'tests against kind of a target element',
      preloads: undefined,
      cases: [
        {
          msg: 'target is exists but its value is undefined',
          rem: '',
          values: {
            node: new TXmlElementController({
              target_item: undefined,
            }),
            name: 'target_item',
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: null,
          __unwrap: undefined,
        },
        assertQty: 2,
        before: {
          node: {
            target_item: undefined,
          },
        },
        after: {
          node: {
            target_item: undefined,
          },
        },
      },
    }, { auto: true }),
    ...genTestCase({
      inGroup: 'tests against kind of a target element',
      preloads: undefined,
      cases: [
        {
          msg: 'target is an object',
          rem: '',
          values: {
            node: new TXmlElementController({
              target_item: { __text: 'target item value' },
            }),
            name: 'target_item',
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: { __text: 'target item value' },
          __unwrap: undefined,
        },
        assertQty: 2,
        before: {
          node: {
            target_item: { __text: 'target item value' },
          },
        },
        after: {
          node: {
            target_item: { __text: 'target item value' },
          },
        },
      },
    }, { auto: true }),
    ...genTestCase({
      inGroup: 'tests against kind of a target element',
      preloads: undefined,
      cases: [
        {
          msg: 'target is an array',
          rem: '',
          values: {
            node: new TXmlElementController({
              target_item: [
                { __text: 'target item value 1' },
                { __text: 'target item value 2' },
                { __text: 'target item value 3' },
              ],
            }),
            name: 'target_item',
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: [
            { __text: 'target item value 1' },
            { __text: 'target item value 2' },
            { __text: 'target item value 3' },
          ],
          __unwrap: undefined,
        },
        assertQty: 2,
        before: {
          node: {
            target_item: [
              { __text: 'target item value 1' },
              { __text: 'target item value 2' },
              { __text: 'target item value 3' },
            ],
          },
        },
        after: {
          node: {
            target_item: [
              { __text: 'target item value 1' },
              { __text: 'target item value 2' },
              { __text: 'target item value 3' },
            ],
          },
        },
      },
    }, { auto: true }),
  ],
};

module.exports = {
  //descr: [],
  tests: [
    nodeTestDescr,
    nameTestDescr,
    resultTestDescr,
  ],
};
