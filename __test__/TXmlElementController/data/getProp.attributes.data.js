// [v0.1.002-20240922]

/* class:    TXmlElementController
 * method:   $getter
 * property: attributes
 */

const { genTestCase } = require('#test-dir/test-hfunc.js');

const { TXmlAttributesMapper } = require('#lib/xmldoc-lib.js');

const __unwrap_obj = (obj) => { return obj.entries };

const preloads = {
  EMPTY_HOST: {
    get obj() {
      return {};
    },
    get options() {
      return undefined;
    },
    status: {
      obj: {},
      options: undefined,
      __unwrap: undefined,
    },
  },
  EMPTY_ELEMENT: {
    get obj() {
      return {
        __attr: {},
        __text: '',
      };
    },
    get options() {
      return undefined;
    },
    status: {
      obj: {
        __attr: {},
        __text: '',
      },
      options: undefined,
      __unwrap: undefined,
    },
  },
  TEST_ELEMENT: {
    get obj() {
      return {
        __attr: { id: '2', name: 'second' },
        __text: '',
        item: {
          __attr: { id: '1' },
          __text: 'first',
        },
      };
    },
    get options() {
      return undefined;
    },
    status: {
      obj: {
        __attr: { id: '2', name: 'second' },
        __text: '',
        item: {
          __attr: { id: '1' },
          __text: 'first',
        },
      },
      options: undefined,
      __unwrap: undefined,
    },
  },
  BAD_ELEMENT: {
    get obj() {
      return {
        __attr: 'atributes',
        __text: '',
      };
    },
    get options(){
      return undefined;
    },
    status: {
      obj: {
        __attr: 'atributes',
        __text: '',
      },
      options: undefined,
      __unwrap: undefined,
    },
  },
};

const resultTestDescr = {
  msg: 'run tests against result values',
  rem: '',
  param: [
    ...genTestCase({
      inGroup: 'constructor: default options is used',
      preloads: preloads.EMPTY_HOST,
      cases: [
        {
          msg: 'constructor: host is an empty object',
          rem: '',
          values: undefined,
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: 'TXmlAttributesMapper',
          value: [],
          __unwrap: __unwrap_obj,
        },
        assertQty: 3,
        before: undefined,
        after: preloads.EMPTY_HOST.status,
      },
    }, { auto: true }),
    ...genTestCase({
      inGroup: 'constructor: default options is used',
      preloads: preloads.EMPTY_ELEMENT,
      cases: [
        {
          msg: 'constructor: host is an empty element',
          rem: '',
          values: undefined,
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: 'TXmlAttributesMapper',
          value: [],
          __unwrap: __unwrap_obj,
        },
        assertQty: 3,
        before: undefined,
        after: preloads.EMPTY_ELEMENT.status,
      },
    }, { auto: true }),
    ...genTestCase({
      inGroup: 'constructor: default options is used',
      preloads: preloads.TEST_ELEMENT,
      cases: [
        {
          msg: 'constructor: host is an element',
          rem: '(some attributes are set)',
          values: undefined,
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: 'TXmlAttributesMapper',
          value: [ [ 'id', '2' ], [ 'name', 'second' ] ],
          __unwrap: __unwrap_obj,
        },
        assertQty: 3,
        before: undefined,
        after: preloads.TEST_ELEMENT.status,
      },
    }, { auto: true }),
    ...genTestCase({
      inGroup: 'constructor: default options is used',
      preloads: preloads.BAD_ELEMENT,
      cases: [
        {
          msg: 'constructor: host is an element',
          rem: '(attributes section is broken)',
          values: undefined,
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: 'TXmlAttributesMapper',
          value: [],
          __unwrap: __unwrap_obj,
        },
        assertQty: 3,
        before: undefined,
        after: preloads.BAD_ELEMENT.status,
      },
    }, { auto: true }),
  ],
};

module.exports = {
  //descr: [],
  tests: [
    resultTestDescr,
  ],
};
