// [v0.1.001-20240918]

/* class:    TXmlAttributesMapper
 * method:   renameAttribute
 * property: ---
 */

const {
  genTestCase,
} = require('#test-dir/test-hfunc.js');

/*const {
  initOptions,
} = require('../test-hfunc.js');
*/

const EMPTY_STRING = '';
const EMPTY_OBJECT = {};

const preloads = {
  TEST_ELEMENT_E0: {
    get obj(){
      return {
        __attr: {
          key_321: 'value_321',
          key_432: 'value_234',
        },
        __text: '',
      };
    },
    get options(){
      return undefined;
    },
    status: {
      obj: {
        __attr: {
          key_321: 'value_321',
          key_432: 'value_234',
        },
        __text: '',
      },
      options: undefined,
      entries: [
        [ 'key_321', 'value_321' ],
        [ 'key_432', 'value_234' ],
      ],
    },
  },
};

const nameTestDescr = {
  msg: 'run tests against "name" param',
  rem: '',
  param: [
    ...genTestCase({
      inGroup: 'no args are given',
      preloads: preloads.TEST_ELEMENT_E0,
      cases: [
        {
          msg: 'undefined value is passed',
          rem: '',
          values: {
            name: undefined,
            value: 'new_name',
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: false,
          __unwrap: undefined,
        },
        assertQty: 2,
        before: undefined,
        after: preloads.TEST_ELEMENT_E0.status,
      },
    }, { auto: true }),
    ...genTestCase({
      inGroup: 'non-valid args are given',
      preloads: preloads.TEST_ELEMENT_E0,
      cases: [
        {
          msg: 'value is a "null"',
          rem: '',
          values: {
            name: null,
            value: 'new_name',
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: false,
          __unwrap: undefined,
        },
        assertQty: 2,
        before: undefined,
        after: preloads.TEST_ELEMENT_E0.status,
      },
    }, { auto: true }),
    ...genTestCase({
      inGroup: 'valid args are given',
      preloads: preloads.TEST_ELEMENT_E0,
      cases: [
        {
          msg: 'value is a string',
          rem: '(non-empty and valid)',
          values: {
            name: 'key_432',
            value: 'new_name',
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: true,
          __unwrap: undefined,
        },
        assertQty: 2,
        before: undefined,
        after: {
          obj: {
            __attr: {
              key_321: 'value_321',
              new_name: 'value_234',
            },
            __text: '',
          },
          options: undefined,
          entries: [
            [ 'key_321', 'value_321' ],
            [ 'new_name', 'value_234' ],
          ],
        },
      },
    }, { auto: true }),
  ],
};

const valueTestDescr = {
  msg: 'run tests against "value" param',
  rem: '',
  param: [
    ...genTestCase({
      inGroup: 'no args are given',
      preloads: preloads.TEST_ELEMENT_E0,
      cases: [
        {
          msg: 'undefined value is passed',
          rem: '',
          values: {
            name: 'key_432',
            value: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: false,
          __unwrap: undefined,
        },
        assertQty: 2,
        before: undefined,
        after: preloads.TEST_ELEMENT_E0.status,
      },
    }, { auto: true }),
    ...genTestCase({
      inGroup: 'non-valid args are given',
      preloads: preloads.TEST_ELEMENT_E0,
      cases: [
        {
          msg: 'value is a "null"',
          rem: '',
          values: {
            name: 'key_432',
            value: null,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: false,
          __unwrap: undefined,
        },
        assertQty: 2,
        before: undefined,
        after: preloads.TEST_ELEMENT_E0.status,
      },
    }, { auto: true }),
    ...genTestCase({
      inGroup: 'valid args are given',
      preloads: preloads.TEST_ELEMENT_E0,
      cases: [
        {
          msg: 'value is a string',
          rem: '(non-empty and valid)',
          values: {
            name: 'key_432',
            value: 'new_name',
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: true,
          __unwrap: undefined,
        },
        assertQty: 2,
        before: undefined,
        after: {
          obj: {
            __attr: {
              key_321: 'value_321',
              new_name: 'value_234',
            },
            __text: '',
          },
          options: undefined,
          entries: [
            [ 'key_321', 'value_321' ],
            [ 'new_name', 'value_234' ],
          ],
        },
      },
    }, { auto: true }),
  ],
};

const resultTestDescr = {
  msg: 'run tests against a result values',
  rem: '(defaults used)',
  param: [
    ...genTestCase({
      inGroup: 'tests against attribute existence',
      preloads: preloads.TEST_ELEMENT_E0,
      cases: [
        {
          msg: 'target attribute exists',
          rem: '',
          values: {
            name: 'key_432',
            value: 'new_name',
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: true,
          __unwrap: undefined,
        },
        assertQty: 2,
        before: undefined,
        after: {
          obj: {
            __attr: {
              key_321: 'value_321',
              new_name: 'value_234',
            },
            __text: '',
          },
          options: undefined,
          entries: [
            [ 'key_321', 'value_321' ],
            [ 'new_name', 'value_234' ],
          ],
        },
      },
    }, { auto: true }),
    ...genTestCase({
      inGroup: 'tests against attribute existence',
      preloads: preloads.TEST_ELEMENT_E0,
      cases: [
        {
          msg: 'target attribute is not exists',
          rem: '',
          values: {
            name: 'key_1232',
            value: 'new_name',
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: false,
          __unwrap: undefined,
        },
        assertQty: 2,
        before: undefined,
        after: preloads.TEST_ELEMENT_E0.status,
      },
    }, { auto: true }),
  ],
};

module.exports = {
  //descr: {},
  tests: [
    nameTestDescr,
    valueTestDescr,
    resultTestDescr,
  ],
};
