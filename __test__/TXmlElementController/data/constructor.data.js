// [v0.1.002-20240921]

/* class:    TXmlElementController
 * method:   $constructor
 * property: ---
 */

const { genTestCase } = require('#test-dir/test-hfunc.js');

const { TXmlElementController } = require('#lib/xmldoc-lib.js');

const __unwrap_ec = TXmlElementController.__unwrap;

//const EMPTY_ELEM = { __attr: {}, __text: '' };
//const initHost = () => { return { __attr: {}, __text: '' } };

const valueTestDescr = {
  msg: 'run tests against "obj" param',
  rem: '(defaults used)',
  param: [
    ...genTestCase({
      inGroup: 'no args are given',
      preloads: undefined,
      cases: [
        {
          msg: 'undefined value is passed',
          rem: '',
          values: {
            obj: undefined,
            opt: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: true,
          errType: TypeError,
          errCode: undefined,
          className: undefined,
          value: undefined,
          __unwrap: __unwrap_ec,
        },
        assertQty: 3,
        before: undefined,
        after: {
          obj: undefined,
          opt: undefined,
        },
      },
    }, { auto: true }),
    ...genTestCase({
      inGroup: 'a non-valid args are given',
      preloads: undefined,
      cases: [
        {
          msg: 'value is a "null"',
          rem: '',
          values: {
            obj: null,
            opt: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: true,
          errType: TypeError,
          errCode: undefined,
          className: undefined,
          value: undefined,
          __unwrap: __unwrap_ec,
        },
        assertQty: 3,
        before: undefined,
        after: {
          obj: null,
          opt: undefined,
        },
      },
    }, { auto: true }),
    ...genTestCase({
      inGroup: 'a non-valid args are given',
      preloads: undefined,
      cases: [
        {
          msg: 'value is an array',
          rem: '',
          values: {
            obj: [],
            opt: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: true,
          errType: TypeError,
          errCode: undefined,
          className: undefined,
          value: undefined,
          __unwrap: __unwrap_ec,
        },
        assertQty: 3,
        before: undefined,
        after: {
          obj: [],
          opt: undefined,
        },
      },
    }, { auto: true }),
    ...genTestCase({
      inGroup: 'a valid args are given',
      preloads: undefined,
      cases: [
        {
          msg: 'value is an object',
          rem: '',
          values: {
            obj: {},
            opt: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: 'TXmlElementController',
          value: {},
          __unwrap: __unwrap_ec,
        },
        assertQty: 3,
        before: undefined,
        after: {
          obj: {},
          opt: undefined,
        },
      },
    }, { auto: true }),
  ],
};

module.exports = {
  //descr: [],
  tests: [
    valueTestDescr,
  ],
};
