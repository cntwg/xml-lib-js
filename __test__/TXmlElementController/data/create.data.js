// [v0.1.001-20240920]

/* class:    TXmlElementController
 * method:   create
 * property: ---
 */

const { genTestCase } = require('#test-dir/test-hfunc.js');

const { TXmlElementController } = require('#lib/xmldoc-lib.js');

const __unwrap_ec = TXmlElementController.__unwrap;

const preloads = {
  TEST_ELEMENT_2: {
    get obj(){
      return {
        __attr: {},
        __text: '',
        item: {
          __attr: { id: '1' },
          __text: 'first',
        },
        tools: [
          {
            __attr: { id: '2' },
            __text: 'second',
          },
        ],
      };
    },
    get options(){
      return undefined;
    },
    status: {
      obj: {
        __attr: {},
        __text: '',
        item: {
          __attr: { id: '1' },
          __text: 'first',
        },
        tools: [
          {
            __attr: { id: '2' },
            __text: 'second',
          },
        ],
      },
      options: undefined,
      __unwrap: undefined,
    },
  },
};

const valueTestDescr = {
  msg: 'run tests against "obj" param',
  rem: '(defaults used)',
  param: [
    ...genTestCase({
      inGroup: 'no args are given',
      preloads: undefined,
      cases: [
        {
          msg: 'undefined value is passed',
          rem: '',
          values: {
            obj: undefined,
            opt: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: null,
          __unwrap: __unwrap_ec,
        },
        assertQty: 2,
        before: undefined,
        after: undefined,
      },
    }, { auto: true }),
    ...genTestCase({
      inGroup: 'a non-valid args are given',
      preloads: undefined,
      cases: [
        {
          msg: 'value is a "null"',
          rem: '',
          values: {
            obj: null,
            opt: undefined,
          },
        }, {
          msg: 'value is a boolean',
          rem: '',
          values: {
            obj: true,
            opt: undefined,
          },
        }, {
          msg: 'value is a number',
          rem: '',
          values: {
            obj: 2,
            opt: undefined,
          },
        }, {
          msg: 'value is a string',
          rem: '',
          values: {
            obj: 'some string',
            opt: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: null,
          __unwrap: __unwrap_ec,
        },
        assertQty: 2,
        before: undefined,
        after: undefined,
      },
    }, { auto: true }),
    ...genTestCase({
      inGroup: 'a valid args are given',
      preloads: undefined,
      cases: [
        {
          msg: 'value is an object',
          rem: '',
          values: {
            obj: {},
            opt: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: 'TXmlElementController',
          value: {},
          __unwrap: __unwrap_ec,
        },
        assertQty: 3,
        before: undefined,
        after: undefined,
      },
    }, { auto: true }),
    ...genTestCase({
      inGroup: 'a valid args are given',
      preloads: undefined,
      cases: [
        {
          msg: 'value is an array',
          rem: '',
          values: {
            obj: [],
            opt: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: 'TXmlElementsListController',
          value: null,
          __unwrap: __unwrap_ec,
        },
        assertQty: 3,
        before: undefined,
        after: undefined,
      },
    }, { auto: true }),
  ],
};

module.exports = {
  //descr: [],
  tests: [
    valueTestDescr,
    //resultTestDescr,
  ],
};
