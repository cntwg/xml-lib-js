// [v0.1.004-20240625]

/* class:    TXmlElementController
 * method:   setTextValue
 * property: ---
 */

const preloads = {
  EMPTY_OBJECT: {
    get obj(){
      return {};
    },
    get options(){
      return undefined;
    },
    status: {
      obj: {},
      options: undefined,
      __unwrap: undefined,
    },
  },
  EMPTY_ELEMENT: {
    get obj(){
      return {
        __attr: {},
        __text: '',
      };
    },
    get options(){
      return undefined;
    },
    status: {
      obj: {
        __attr: {},
        __text: '',
      },
      options: undefined,
      __unwrap: undefined,
    },
  },
};

const valueTestDescr = {
  msg: 'run tests against "value" param',
  param: [
    {
      msg: 'no args are given',
      param: [
        ({
          msg: 'undefined value is passed',
          rem: '(element must not be changed)',
          preloads: preloads.EMPTY_OBJECT,
          value: undefined,
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              className: undefined,
              value: false,
              entries: undefined,
            },
            before: undefined,
            after: {
              obj: {},
              options: undefined,
              __unwrap: undefined,
            },
          },
          initTest(){
            this.status.before = this.preloads.status;
            return this;
          },
        }).initTest(),
      ],
    }, {
      msg: 'non-valid args are given',
      param: [
        ({
          msg: 'value is a "null"',
          rem: '(element will not be changed)',
          preloads: preloads.EMPTY_OBJECT,
          value: null,
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              className: undefined,
              value: false,
              entries: undefined,
            },
            before: undefined,
            after: {
              obj: {},
              options: undefined,
              __unwrap: undefined,
            },
          },
          initTest(){
            this.status.before = this.preloads.status;
            return this;
          },
        }).initTest(),
      ],
    }, {
      msg: 'a valid args are given',
      //preloads: preloads,
      param: [
        ({
          msg: 'value is a "boolean"',
          rem: '(value will be converted to a string)',
          preloads: preloads.EMPTY_OBJECT,
          value: true,
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              className: undefined,
              value: true,
              entries: undefined,
            },
            before: undefined,
            after: {
              obj: {
                __text: 'true',
              },
              options: undefined,
              __unwrap: undefined,
            },
          },
          initTest(){
            this.status.before = this.preloads.status;
            return this;
          },
        }).initTest(),
        ({
          msg: 'value is a number',
          rem: '(value will be converted to a string)',
          preloads: preloads.EMPTY_OBJECT,
          value: 125,
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              className: undefined,
              value: true,
              entries: undefined,
            },
            before: undefined,
            after: {
              obj: {
                __text: '125',
              },
              options: undefined,
              __unwrap: undefined,
            },
          },
          initTest(){
            this.status.before = this.preloads.status;
            return this;
          },
        }).initTest(),
        ({
          msg: 'value is a string',
          rem: '',
          preloads: preloads.EMPTY_OBJECT,
          value: 'test value',
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              className: undefined,
              value: true,
              entries: undefined,
            },
            before: undefined,
            after: {
              obj: {
                __text: 'test value',
              },
              options: undefined,
              __unwrap: undefined,
            },
          },
          initTest(){
            this.status.before = this.preloads.status;
            return this;
          },
        }).initTest(),
        ({
          msg: 'value is an array of a "Lang-Value" pair',
          rem: '',
          preloads: preloads.EMPTY_OBJECT,
          value: [ 'en', 'some text value' ],
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              className: undefined,
              value: true,
              entries: undefined,
            },
            before: undefined,
            after: {
              obj: {
                __attr: { lang: 'en' },
                __text: 'some text value',
              },
              options: undefined,
              __unwrap: undefined,
            },
          },
          initTest(){
            this.status.before = this.preloads.status;
            return this;
          },
        }).initTest(),
      ],
    }, {
      msg: 'tests against special cases',
      param: [
        ({
          msg: 'value is an object',
          rem: '[*]',
          preloads: preloads.EMPTY_OBJECT,
          value: {
            a: 'some value',
            b: [ 125, false, null ],
          },
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              className: undefined,
              value: true,
              entries: undefined,
            },
            before: undefined,
            after: {
              obj: {
                __text: '',
              },
              options: undefined,
              __unwrap: undefined,
            },
          },
          initTest(){
            this.status.before = this.preloads.status;
            return this;
          },
        }).initTest(),
      ],
    }, {
      msg: 'tests against special cases',
      param: [
        ({
          msg: 'value is an array',
          rem: '[*]',
          preloads: preloads.EMPTY_OBJECT,
          value: [ false, 'some text value', 125, false ],
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              className: undefined,
              value: true,
              entries: undefined,
            },
            before: undefined,
            after: {
              obj: {
                __attr: { lang: 'false' },
                __text: 'some text value',
              },
              options: undefined,
              __unwrap: undefined,
            },
          },
          initTest(){
            this.status.before = this.preloads.status;
            return this;
          },
        }).initTest(),
      ],
    },
  ],
};

const resultTestDescr = {
  msg: 'run tests against special cases',
  param: [
    {
      msg: 'constructor: default options is used',
      param: [
        ({
          msg: 'constructor: host is an empty object',
          rem: '',
          preloads: preloads.EMPTY_OBJECT,
          value: 'test value',
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              className: undefined,
              value: true,
              entries: undefined,
            },
            before: undefined,
            after: {
              obj: {
                __text: 'test value',
              },
              options: undefined,
              __unwrap: undefined,
            },
          },
          initTest(){
            this.status.before = this.preloads.status;
            return this;
          },
        }).initTest(),
        ({
          msg: 'constructor: host is an empty element',
          rem: '',
          preloads: preloads.EMPTY_ELEMENT,
          value: 'test value',
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              className: undefined,
              value: true,
              entries: undefined,
            },
            before: undefined,
            after: {
              obj: {
                __attr: {},
                __text: 'test value',
              },
              options: undefined,
              __unwrap: undefined,
            },
          },
          initTest(){
            this.status.before = this.preloads.status;
            return this;
          },
        }).initTest(),
        ({
          msg: 'constructor: host is an element (case 1)',
          rem: '("textValue" and "lang" attribute are set)',
          preloads: {
            get obj(){
              return {
                __attr: { lang: 'en' },
                __text: 'some text value',
              };
            },
            get options(){
              return undefined;
            },
            status: {
              obj: {
                __attr: { lang: 'en' },
                __text: 'some text value',
              },
              options: undefined,
              __unwrap: undefined,
            },
          },
          value: 'test value',
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              className: undefined,
              value: true,
              entries: undefined,
            },
            before: undefined,
            after: {
              obj: {
                __attr: {},
                __text: 'test value',
              },
              options: undefined,
              __unwrap: undefined,
            },
          },
          initTest(){
            this.status.before = this.preloads.status;
            return this;
          },
        }).initTest(),
        ({
          msg: 'constructor: host is an element (case 2)',
          rem: '("textValue" and "lang" attribute are set)',
          preloads: {
            get obj(){
              return {
                __attr: { lang: 'en' },
                __text: 'some text value',
              };
            },
            get options(){
              return undefined;
            },
            status: {
              obj: {
                __attr: { lang: 'en' },
                __text: 'some text value',
              },
              options: undefined,
              __unwrap: undefined,
            },
          },
          value: [ 'fr', 'test value' ],
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              className: undefined,
              value: true,
              entries: undefined,
            },
            before: undefined,
            after: {
              obj: {
                __attr: { lang: 'fr' },
                __text: 'test value',
              },
              options: undefined,
              __unwrap: undefined,
            },
          },
          initTest(){
            this.status.before = this.preloads.status;
            return this;
          },
        }).initTest(),
      ],
    },
  ],
};

module.exports = {
  //preloads: preloads,
  descr: [
    valueTestDescr,
    resultTestDescr,
  ],
};
