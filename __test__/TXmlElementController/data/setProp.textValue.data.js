// [v0.1.005-20240923]

/* class:    TXmlElementController
 * method:   $setter
 * property: textValue
 */

const { genTestCase } = require('#test-dir/test-hfunc.js');

const { TXmlElementController } = require('#lib/xmldoc-lib.js');

const __unwrap_ec = TXmlElementController.__unwrap;

const preloads = {
  EMPTY_HOST: {
    get obj() {
      return {};
    },
    get options() {
      return undefined;
    },
    status: {
      obj: {},
      options: undefined,
      __unwrap: undefined,
    },
  },
  EMPTY_ELEMENT: {
    get obj() {
      return {
        __attr: {},
        __text: '',
      };
    },
    get options() {
      return undefined;
    },
    status: {
      obj: {
        __attr: {},
        __text: '',
      },
      options: undefined,
      __unwrap: undefined,
    },
  },
  TEST_ELEMENT: {
    get obj() {
      return {
        __attr: {},
        __text: 'some text value',
      };
    },
    get options() {
      return undefined;
    },
    status: {
      obj: {
        __attr: {},
        __text: 'some text value',
      },
      options: undefined,
      __unwrap: undefined,
    },
  },
  BAD_ELEMENT: {
    get obj() {
      return {
        __attr: {},
        __text: [ 'some text value' ],
      };
    },
    get options() {
      return undefined;
    },
    status: {
      obj: {
        __attr: {},
        __text: [ 'some text value' ],
      },
      options: undefined,
      __unwrap: undefined,
    },
  },
};

const valueTestDescr = {
  msg: 'run tests against "value" param',
  rem: '(constructor: default options is used)',
  param: [
    ...genTestCase({
      inGroup: 'no args are given',
      preloads: preloads.EMPTY_HOST,
      cases: [
        {
          msg: 'undefined value is passed',
          rem: '(failed: element not changed)',
          values: {
            value: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: undefined,
          __unwrap: undefined,
        },
        assertQty: 2,
        before: undefined,
        after: preloads.EMPTY_HOST.status,
      },
    }, { auto: true }),
    ...genTestCase({
      inGroup: 'non-valid args are given',
      preloads: preloads.EMPTY_HOST,
      cases: [
        {
          msg: 'value is a "null"',
          rem: '(failed: element not changed)',
          values: {
            value: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: undefined,
          __unwrap: undefined,
        },
        assertQty: 2,
        before: undefined,
        after: preloads.EMPTY_HOST.status,
      },
    }, { auto: true }),
    ...genTestCase({
      inGroup: 'non-valid args are given',
      preloads: preloads.EMPTY_HOST,
      cases: [
        {
          msg: 'value is an array',
          rem: '(failed: element not changed) [*]',
          values: {
            value: [ 'some value', 125, false, null ],
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: [ 'some value', 125, false, null ],
          __unwrap: undefined,
        },
        assertQty: 2,
        before: undefined,
        after: {
          obj: {
            __text: '',
          },
          options: undefined,
          __unwrap: undefined,
        },
      },
    }, { auto: true }),
    ...genTestCase({
      inGroup: 'non-valid args are given',
      preloads: preloads.EMPTY_HOST,
      cases: [
        {
          msg: 'value is an object',
          rem: '(failed: element not changed) [*]',
          values: {
            value: {
              a: 'some value',
              b: [ 125, false, null ],
            },
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: {
            a: 'some value',
            b: [ 125, false, null ],
          },
          __unwrap: undefined,
        },
        assertQty: 2,
        before: undefined,
        after: {
          obj: {
            __text: '',
          },
          options: undefined,
          __unwrap: undefined,
        },
      },
    }, { auto: true }),
    ...genTestCase({
      inGroup: 'valid args are given',
      preloads: preloads.EMPTY_HOST,
      cases: [
        {
          msg: 'value is a boolean',
          rem: '(succeed: value will be converted to a string)',
          values: {
            value: true,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: true,
          __unwrap: undefined,
        },
        assertQty: 2,
        before: undefined,
        after: {
          obj: {
            __text: 'true',
          },
          options: undefined,
          __unwrap: undefined,
        },
      },
    }, { auto: true }),
    ...genTestCase({
      inGroup: 'valid args are given',
      preloads: preloads.EMPTY_HOST,
      cases: [
        {
          msg: 'value is a number',
          rem: '(succeed: value will be converted to a string)',
          values: {
            value: 125,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: 125,
          __unwrap: undefined,
        },
        assertQty: 2,
        before: undefined,
        after: {
          obj: {
            __text: '125',
          },
          options: undefined,
          __unwrap: undefined,
        },
      },
    }, { auto: true }),
    ...genTestCase({
      inGroup: 'valid args are given',
      preloads: preloads.EMPTY_HOST,
      cases: [
        {
          msg: 'value is an empty string',
          rem: '(succeed: value passed)',
          values: {
            value: '',
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: '',
          __unwrap: undefined,
        },
        assertQty: 2,
        before: undefined,
        after: {
          obj: {
            __text: '',
          },
          options: undefined,
          __unwrap: undefined,
        },
      },
    }, { auto: true }),
    ...genTestCase({
      inGroup: 'valid args are given',
      preloads: preloads.EMPTY_HOST,
      cases: [
        {
          msg: 'value is a non-empty string',
          rem: '',
          values: {
            value: 'some value',
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: 'some value',
          __unwrap: undefined,
        },
        assertQty: 2,
        before: undefined,
        after: {
          obj: {
            __text: 'some value',
          },
          options: undefined,
          __unwrap: undefined,
        },
      },
    }, { auto: true }),
  ],
};

const resultTestDescr = {
  msg: 'run tests against result values',
  rem: '(constructor: default options is used)',
  param: [
    ...genTestCase({
      inGroup: 'constructor: host is an element with text value set',
      preloads: preloads.TEST_ELEMENT,
      cases: [
        {
          msg: 'ensure a value is overidden',
          rem: '',
          values: {
            value: 'new value',
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: 'new value',
          __unwrap: undefined,
        },
        assertQty: 2,
        before: undefined,
        after: {
          obj: {
            __attr: {},
            __text: 'new value',
          },
          options: undefined,
          __unwrap: undefined,
        },
      },
    }, { auto: true }),
    ...genTestCase({
      inGroup: 'constructor: host is a bad element',
      preloads: preloads.TEST_ELEMENT,
      cases: [
        {
          msg: 'ensure a value is overidden',
          rem: '',
          values: {
            value: 'new value',
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: 'new value',
          __unwrap: undefined,
        },
        assertQty: 2,
        before: undefined,
        after: {
          obj: {
            __attr: {},
            __text: 'new value',
          },
          options: undefined,
          __unwrap: undefined,
        },
      },
    }, { auto: true }),
  ],
};

module.exports = {
  //descr: [],
  tests: [
    valueTestDescr,
    resultTestDescr,
  ],
};
