// [v0.1.004-20240929]

const { runTestFn } = require('#test-dir/test-hfunc.js');

const xmlDoc = require('#lib/xmldoc-lib.js');
const TXmlAttributesMapper = xmlDoc.TXmlAttributesMapper;
const TXmlElementController = xmlDoc.TXmlElementController;
const TXmlElementsListController = xmlDoc.TXmlElementsListController;

const t_Dat = require('./data/index.data.js');

const __unwrap_ec = TXmlElementController.__unwrap;

// class: 'TXmlElementController'
describe('class: TXmlElementController', () => {
  describe('1. run tests against class static methods', () => {
    describe.each([
      {
        msg: '1.1 - method',
        method: 'create',
      },
    ])(`$msg: $method()`, ({ method }) => {
      const testData = t_Dat.class_static[method];
      describe.each(testData.tests)('$msg $rem', ({ param }) => {
        describe.each(param)('$msg', ({ param }) => {
          describe.each(param)('$msg $rem', ({ preloads, values, status }) => {
            const { before: stat_b, after: stat_a } = status;
            let testInst = null; let obj = null; let options = undefined;
            beforeAll(() => {
              //({ obj, options } = preloads);
              //testInst = new TXmlElementController(obj, options);
              testInst = TXmlElementController;
            });
            //it('check instance status (before)', () => {
            //  expect(testInst instanceof TXmlElementController).toBe(true);
            //  //expect(item.textValue).toBe(result);
            //  expect(__unwrap_ec(testInst)).toStrictEqual(stat_b.obj);
            //});
            it('perform ops', () => {
              const { ops, assertQty } = status;
              let result = null; let isERR = false;
              try {
                result = runTestFn({ testInst, method }, values);
                //console.log('CHECK: => NO_ERR');
              } catch (err) {
                //console.log('CHECK: IS_ERR => '+err);
                isERR = true;
                result = err;
              } finally {
                expect.assertions(assertQty);
                expect(isERR).toBe(ops.isERR);
                if (isERR) {
                  const { errType, errCode } = ops;
                  expect(result instanceof errType).toBe(true);
                  expect(result.code).toStrictEqual(errCode);
                } else {
                  const { className, value, __unwrap } = ops;
                  switch (typeof className) {
                    case 'string' : {
                      expect(result instanceof xmlDoc[className]).toBe(true);
                      if (className !== 'HTMLElement') {
                        if (typeof __unwrap !== 'function') break;
                        result = __unwrap(result);
                      };
                    }
                    default : {
                      expect(result).toStrictEqual(value);
                    }
                  };
                };
              };
            });
            //it('check instance status (after)', () => {
            //  expect(testInst instanceof TXmlElementController).toBe(true);
            //  //expect(item.textValue).toBe(result);
            //  expect(__unwrap_ec(testInst)).toStrictEqual(stat_a.obj);
            //});
          });
        });
      });
    });
    //===
  });

  describe('2. run tests against class static methods (special)', () => {
    describe.each([
      {
        msg: '2.1 - method',
        method: '__getChildRaw',
      }, {
        msg: '2.2 - method',
        method: '__addChildRaw',
      }, {
        msg: '2.3 - method',
        method: '__setChildRaw',
      },
    ])(`$msg: $method()`, ({ method }) => {
      const testData = t_Dat.class_static[method];
      describe.each(testData.tests)('$msg $rem', ({ param }) => {
        describe.each(param)('$msg', ({ param }) => {
          describe.each(param)('$msg $rem', ({ preloads, values, status }) => {
            const { before: stat_b, after: stat_a } = status;
            let testInst = null; let node = null; let options = undefined;
            beforeAll(() => {
              ({ node } = values);
              testInst = TXmlElementController;
            });
            it('check instance status (before)', () => {
              expect(__unwrap_ec(node)).toStrictEqual(stat_b.node);
            });
            it('perform ops', () => {
              const { ops, assertQty } = status;
              let result = null; let isERR = false;
              try {
                result = runTestFn({ testInst, method }, values);
                //console.log('CHECK: => NO_ERR');
              } catch (err) {
                //console.log('CHECK: IS_ERR => '+err);
                isERR = true;
                result = err;
              } finally {
                expect.assertions(assertQty);
                expect(isERR).toBe(ops.isERR);
                if (isERR) {
                  const { errType, errCode } = ops;
                  expect(result instanceof errType).toBe(true);
                  expect(result.code).toStrictEqual(errCode);
                } else {
                  const { className, value, __unwrap } = ops;
                  switch (typeof className) {
                    case 'string' : {
                      expect(result instanceof xmlDoc[className]).toBe(true);
                      if (className !== 'HTMLElement') {
                        if (typeof __unwrap !== 'function') break;
                        result = __unwrap(result);
                      };
                    }
                    default : {
                      expect(result).toStrictEqual(value);
                    }
                  };
                };
              };
            });
            it('check instance status (after)', () => {
              expect(__unwrap_ec(node)).toStrictEqual(stat_a.node);
            });
          });
        });
      });
    });
    //===
  });

  //===
});
