// [v0.1.038-20240929]

const { runTestFn } = require('#test-dir/test-hfunc.js');

const xmlDoc = require('#lib/xmldoc-lib.js');
const TXmlAttributesMapper = xmlDoc.TXmlAttributesMapper;
const TXmlElementController = xmlDoc.TXmlElementController;
const TXmlElementsListController = xmlDoc.TXmlElementsListController;

const t_Dat = require('./data/index.data.js');

const __unwrap_ec = TXmlElementController.__unwrap;

// class: 'TXmlElementController'
describe('class: TXmlElementController', () => {
  describe(`1. create a new instance (auto mode)`, () => {
    const method = '$constructor';
    const testData = t_Dat[method];
    describe.each(testData.tests)('$msg $rem', ({ param }) => {
      describe.each(param)('$msg', ({ param }) => {
        describe.each(param)('$msg $rem', ({ preloads, values, status }) => {
          const { before: stat_b, after: stat_a } = status;
          let testInst = null; let obj = null; let options = undefined;
          beforeAll(() => {
            ({ obj, options } = values);
            testInst = TXmlElementController;
          });
          /*it('check instance status (before)', () => {
            expect(testInst instanceof TXmlElementController).toBe(true);
            //expect(item.textValue).toBe(result);
            expect(__unwrap_ec(testInst)).toStrictEqual(stat_b.obj);
          });*/
          it('perform ops', () => {
            const { ops, assertQty } = status;
            let result = null; let isERR = false;
            try {
              result = runTestFn({ testInst, method }, values);
              //console.log('CHECK: => NO_ERR');
            } catch (err) {
              //console.log('CHECK: IS_ERR => '+err);
              isERR = true;
              result = err;
            } finally {
              expect.assertions(assertQty);
              expect(isERR).toBe(ops.isERR);
              if (isERR) {
                const { errType, errCode } = ops;
                expect(result instanceof errType).toBe(true);
                expect(result.code).toStrictEqual(errCode);
              } else {
                const { className, value, __unwrap } = ops;
                switch (typeof className) {
                  case 'string' : {
                    expect(result instanceof xmlDoc[className]).toBe(true);
                    if (className !== 'HTMLElement') {
                      if (typeof __unwrap !== 'function') break;
                      result = __unwrap(result);
                    };
                  }
                  default : {
                    expect(result).toStrictEqual(value);
                  }
                };
              };
            };
          });
          it('check instance status (after)', () => {
            //expect(testInst instanceof TXmlElementController).toBe(true);
            expect(obj).toStrictEqual(stat_a.obj);
            expect(options).toStrictEqual(stat_a.options);
          });
        });
      });
    });
  });

  describe('2. run tests aganst instance properties/methods', () => {
    describe('2.1. process element attributes', () => {
      describe.each([
        {
          msg: '2.1.1 - property',
          method: '$getter',
          prop: 'attributes',
        },
      ])(`$msg: $prop (read)`, ({ method, prop }) => {
        const data = t_Dat.prop[method][prop];
        describe.each(data.tests)('$msg $rem', ({ param }) => {
          describe.each(param)('+ $msg', ({ param }) => {
            describe.each(param)('+$msg $rem', ({ preloads, values, status }) => {
              const { before: stat_b, after: stat_a } = status;
              let testInst = null; let obj = null; let options = null;
              beforeAll(() => {
                ({ obj, options } = preloads);
                testInst = new TXmlElementController(obj, options);
              });
              it('check instance status (before)', () => {
                expect(testInst instanceof TXmlElementController).toBe(true);
                expect(obj).toStrictEqual(stat_b.obj);
                expect(options).toStrictEqual(stat_b.options);
              });
              it('perform ops', () => {
                const { ops, assertQty } = status;
                let result = null; let isERR = false;
                try {
                  result = runTestFn({ testInst, method, prop }, values);
                  //console.log('CHECK: => NO_ERR');
                } catch (err) {
                  //console.log('CHECK: IS_ERR => '+err);
                  isERR = true;
                  result = err;
                } finally {
                  expect.assertions(assertQty);
                  expect(isERR).toBe(ops.isERR);
                  if (isERR) {
                    const { errType, errCode } = ops;
                    expect(result instanceof errType).toBe(true);
                    expect(result.code).toStrictEqual(errCode);
                  } else {
                    const { className, value, __unwrap } = ops;
                    switch (typeof className) {
                      case 'string' : {
                        expect(result instanceof xmlDoc[className]).toBe(true);
                        if (className !== 'HTMLElement') {
                          if (typeof __unwrap !== 'function') break;
                          result = __unwrap(result);
                        };
                      }
                      default : {
                        expect(result).toStrictEqual(value);
                      }
                    };
                  };
                };
              });
              if (stat_a) {
                it('check instance status (after)', () => {
                  expect(testInst instanceof TXmlElementController).toBe(true);
                  expect(obj).toStrictEqual(stat_a.obj);
                  expect(options).toStrictEqual(stat_a.options);
                });
              };
            });
          });
        });
      });

      //==
    });

    describe('2.2. process element text value', () => {
      describe.each([
        {
          msg: '2.2.1 - property',
          method: '$setter',
          prop: 'textValue',
        },
      ])(`$msg: $prop (write)`, ({ method, prop }) => {
        const data = t_Dat.prop[method][prop];
        describe.each(data.tests)('$msg $rem', ({ param }) => {
          describe.each(param)('+ $msg', ({ param }) => {
            describe.each(param)('+ $msg $rem', ({ preloads, values, status }) => {
              const { before: stat_b, after: stat_a } = status;
              let testInst = null; let obj = null; let options = null;
              beforeAll(() => {
                ({ obj, options } = preloads);
                testInst = new TXmlElementController(obj, options);
              });
              it('check instance status (before)', () => {
                expect(testInst instanceof TXmlElementController).toBe(true);
                expect(obj).toStrictEqual(stat_b.obj);
                expect(options).toStrictEqual(stat_b.options);
              });
              it('perform ops', () => {
                const { ops, assertQty } = status;
                let result = null; let isERR = false;
                try {
                  result = runTestFn({ testInst, method, prop }, values);
                  //console.log('CHECK: => NO_ERR');
                } catch (err) {
                  //console.log('CHECK: IS_ERR => '+err);
                  isERR = true;
                  result = err;
                } finally {
                  expect.assertions(assertQty);
                  expect(isERR).toBe(ops.isERR);
                  if (isERR) {
                    const { errType, errCode } = ops;
                    expect(result instanceof errType).toBe(true);
                    expect(result.code).toStrictEqual(errCode);
                  } else {
                    const { className, value, __unwrap } = ops;
                    switch (typeof className) {
                      case 'string' : {
                        expect(result instanceof xmlDoc[className]).toBe(true);
                        if (className !== 'HTMLElement') {
                          if (typeof __unwrap !== 'function') break;
                          result = __unwrap(result);
                        };
                      }
                      default : {
                        expect(result).toStrictEqual(value);
                      }
                    };
                  };
                };
              });
              if (stat_a) {
                it('check instance status (after)', () => {
                  expect(testInst instanceof TXmlElementController).toBe(true);
                  expect(obj).toStrictEqual(stat_a.obj);
                  expect(options).toStrictEqual(stat_a.options);
                });
              };
            });
          });
        });
      });

      describe('2.2.2 - property: "textValue" (read) ', () => {
        const prop = 'textValue';
        const testData = t_Dat.getProp[prop];
        describe.each(testData.descr)('$msg', ({ param }) => {
          describe.each(param)('$msg', ({ param }) => {
            describe.each(param)('$msg $rem', ({ preloads, status }) => {
              const testFn = () => { return testInst[prop]; };
              const { before: stat_b, after: stat_a } = status;
              let testInst = null; let obj = null; let options = null;
              beforeAll(() => {
                ({ obj, options } = preloads);
                testInst = new TXmlElementController(obj, options);
              });
              it('check instance status (before)', () => {
                expect(testInst instanceof TXmlElementController).toBe(true);
                expect(obj).toStrictEqual(stat_b.obj);
                expect(options).toStrictEqual(stat_b.options);
              });
              it('perform ops', () => {
                const { ops } = status;
                let result = undefined;
                expect(() => { result = testFn(); }).not.toThrow(Error);
                expect(ops.isERR).toBe(false);
                expect(result).toStrictEqual(ops.value);
              });
            });
          });
        });
      });

      describe('2.2.3 - method getTextValue()', () => {
        const method = 'getTextValue';
        const testData = t_Dat.main[method];
        describe.each(testData.descr)('$msg', ({ param }) => {
          describe.each(param)('$msg', ({ param }) => {
            describe.each(param)('$msg', ({ preloads, status }) => {
              const testFn = (...args) => { return testInst[method](...args); };
              const { before: stat_b, after: stat_a } = status;
              let testInst = null; let obj = null; let options = null;
              beforeAll(() => {
                ({ obj, options } = preloads);
                testInst = new TXmlElementController(obj, options);
              });
              it('check instance status (before)', () => {
                expect(testInst instanceof TXmlElementController).toBe(true);
                expect(obj).toStrictEqual(stat_b.obj);
                expect(options).toStrictEqual(stat_b.options);
              });
              it('perform ops', () => {
                const { ops } = status;
                let result = undefined;
                expect(() => { result = testFn(); }).not.toThrow(Error);
                expect(ops.isERR).toBe(false);
                expect(result).toStrictEqual(ops.value);
              });
            });
          });
        });
      });

      describe('2.2.4 - method setTextValue()', () => {
        const method = 'setTextValue';
        const testData = t_Dat.main[method];
        describe.each(testData.descr)('$msg $rem', ({ param }) => {
          describe.each(param)('$msg', ({ param }) => {
            describe.each(param)('$msg $rem', ({ preloads, value, status }) => {
              const testFn = (...args) => { return testInst[method](...args); };
              const { before: stat_b, after: stat_a } = status;
              let testInst = null; let obj = null; let options = null;
              beforeAll(() => {
                ({ obj, options } = preloads);
                testInst = new TXmlElementController(obj, options);
              });
              it('check instance status (before)', () => {
                expect(testInst instanceof TXmlElementController).toBe(true);
                expect(obj).toStrictEqual(stat_b.obj);
                expect(options).toStrictEqual(stat_b.options);
              });
              it('perform ops', () => {
                const { ops } = status;
                let result = undefined;
                expect(() => { result = testFn(value); }).not.toThrow(Error);
                expect(ops.isERR).toBe(false);
                expect(result).toStrictEqual(ops.value);
              });
              it('check instance status (after)', () => {
                expect(testInst instanceof TXmlElementController).toBe(true);
                expect(obj).toStrictEqual(stat_a.obj);
                expect(options).toStrictEqual(stat_a.options);
              });
            });
          });
        });
      });
    });

    describe('2.3. process child elements', () => {
      describe.each([
        {
          msg: '2.3.1 - method: hasChild()',
          method: 'hasChild',
        }, {
          msg: '2.3.2 - method: getChild()',
          method: 'getChild',
        }, {
          msg: '2.3.3 - method: addChild()',
          method: 'addChild',
        }, {
          msg: '2.3.4 - method: delChild()',
          method: 'delChild',
        },
      ])('$msg', ({ method }) => {
        const data = t_Dat.main[method];
        describe.each(data.tests)('$msg $rem', ({ param }) => {
          describe.each(param)('+ $msg', ({ param }) => {
            describe.each(param)('+$msg $rem', ({ preloads, values, status }) => {
              const { before: stat_b, after: stat_a } = status;
              let testInst = null; let obj = null; let options = null;
              beforeAll(() => {
                ({ obj, options } = preloads);
                testInst = new TXmlElementController(obj, options);
              });
              it('check instance status (before)', () => {
                expect(testInst instanceof TXmlElementController).toBe(true);
                expect(obj).toStrictEqual(stat_b.obj);
                expect(options).toStrictEqual(stat_b.options);
              });
              it('perform ops', () => {
                const { ops, assertQty } = status;
                let result = null; let isERR = false;
                try {
                  result = runTestFn({ testInst, method }, values);
                  //console.log('CHECK: => NO_ERR');
                } catch (err) {
                  //console.log('CHECK: IS_ERR => '+err);
                  isERR = true;
                  result = err;
                } finally {
                  expect.assertions(assertQty);
                  expect(isERR).toBe(ops.isERR);
                  if (isERR) {
                    const { errType, errCode } = ops;
                    expect(result instanceof errType).toBe(true);
                    expect(result.code).toStrictEqual(errCode);
                  } else {
                    const { className, value, __unwrap } = ops;
                    switch (typeof className) {
                      case 'string' : {
                        expect(result instanceof xmlDoc[className]).toBe(true);
                        if (className !== 'HTMLElement') {
                          if (typeof __unwrap !== 'function') break;
                          result = __unwrap(result);
                        };
                      }
                      default : {
                        expect(result).toStrictEqual(value);
                      }
                    };
                  };
                };
              });
              if (stat_a) {
                it('check instance status (after)', () => {
                  expect(testInst instanceof TXmlElementController).toBe(true);
                  expect(obj).toStrictEqual(stat_a.obj);
                  expect(options).toStrictEqual(stat_a.options);
                });
              };
            });
          });
        });
      });

      describe.each([
        {
          msg: '2.4 - method: clear()',
          method: 'clear',
        },
      ])('$msg', ({ method }) => {
        const testData = t_Dat.main[method];
        describe.each(testData.descr)('$msg $rem', ({ param }) => {
          describe.each(param)('$msg', ({ param }) => {
            describe.each(param)('$msg $rem', ({ preloads, values, status }) => {
              const testFn = (...args) => { return testInst[method](...args); };
              const { before: stat_b, after: stat_a } = status;
              let testInst = null; let obj = null; let options = undefined;
              beforeAll(() => {
                ({ obj, options } = preloads);
                testInst = new TXmlElementController(obj, options);
              });
              it('check instance status (before)', () => {
                expect(testInst instanceof TXmlElementController).toBe(true);
                //expect(item.textValue).toBe(result);
                expect(__unwrap_ec(testInst)).toStrictEqual(stat_b.obj);
              });
              it('perform ops', () => {
                const { ops, assertQty } = status;
                let result = null; let isERR = false;
                try {
                  result = runTestFn({ testInst, method }, values);
                  //console.log('CHECK: => NO_ERR');
                } catch (err) {
                  //console.log('CHECK: IS_ERR => '+err);
                  isERR = true;
                  result = err;
                } finally {
                  expect.assertions(assertQty);
                  expect(isERR).toBe(ops.isERR);
                  if (isERR) {
                    const { errType, errCode } = ops;
                    expect(result instanceof errType).toBe(true);
                    expect(result.code).toStrictEqual(errCode);
                  } else {
                    const { className, value } = ops;
                    if (typeof className === 'string') {
                      expect(result instanceof xmlDoc[className]).toBe(true);
                    } else {
                      expect(result).toStrictEqual(value);
                    };
                  };
                };
              });
              it('check instance status (after)', () => {
                expect(testInst instanceof TXmlElementController).toBe(true);
                //expect(item.textValue).toBe(result);
                expect(__unwrap_ec(testInst)).toStrictEqual(stat_a.obj);
              });
            });
          });
        });
      });
      //===
    });

    describe.each([
      {
        msg: '4.1.2.6 - method',
        method: 'loadFromXMLString',
      },
    ])(`$msg: $method()`, ({ method }) => {
      const testData = t_Dat.main[method];
      describe.each(testData.tests)('$msg $rem', ({ param }) => {
        describe.each(param)('$msg', ({ param }) => {
          describe.each(param)('$msg $rem', ({ preloads, values, status }) => {
            const { before: stat_b, after: stat_a } = status;
            let testInst = null; let obj = null; let options = undefined;
            beforeAll(() => {
              ({ obj, options } = preloads);
              testInst = new TXmlElementController(obj, options);
            });
            it('check instance status (before)', () => {
              expect(testInst instanceof TXmlElementController).toBe(true);
              //expect(item.textValue).toBe(result);
              expect(__unwrap_ec(testInst)).toStrictEqual(stat_b.obj);
            });
            it('perform ops', () => {
              const { ops, assertQty } = status;
              let result = null; let isERR = false;
              try {
                result = runTestFn({ testInst, method }, values);
                //console.log('CHECK: => NO_ERR');
              } catch (err) {
                //console.log('CHECK: IS_ERR => '+err);
                isERR = true;
                result = err;
              } finally {
                expect.assertions(assertQty);
                expect(isERR).toBe(ops.isERR);
                if (isERR) {
                  const { errType, errCode } = ops;
                  expect(result instanceof errType).toBe(true);
                  expect(result.code).toStrictEqual(errCode);
                } else {
                  const { className, value } = ops;
                  if (typeof className === 'string') {
                    expect(result instanceof xmlDoc[className]).toBe(true);
                  } else {
                    expect(result).toStrictEqual(value);
                  };
                };
              };
            });
            it('check instance status (after)', () => {
              expect(testInst instanceof TXmlElementController).toBe(true);
              //expect(item.textValue).toBe(result);
              expect(__unwrap_ec(testInst)).toStrictEqual(stat_a.obj);
            });
          });
        });
      });
    });
    //===
  });

  //===
});
