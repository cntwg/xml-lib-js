// [v0.1.037-20240929]

const { runTestFn } = require('#test-dir/test-hfunc.js');

const xmlDoc = require('#lib/xmldoc-lib.js');
const TXmlAttributesMapper = xmlDoc.TXmlAttributesMapper;
const TXmlElementController = xmlDoc.TXmlElementController;
const TXmlElementsListController = xmlDoc.TXmlElementsListController;

const t_Dat = require('./data/index.data.js');

const __unwrap_ec = TXmlElementController.__unwrap;

// class: 'TXmlElementController'
describe('class: TXmlElementController', () => {
  describe('1. run tests aganst instance properties/methods', () => {
    describe('1.1 - run ops aganst instance atributes', () => {
      describe.each([
        {
          msg: '1.1.1 - method',
          method: 'hasAttribute',
        }, {
          msg: '1.1.2 - method',
          method: 'getAttribute',
        }, {
          msg: '1.1.3 - method',
          method: 'setAttribute',
        }, {
          msg: '1.1.4 - method',
          method: 'delAttribute',
        }, {
          msg: '1.1.5 - method',
          method: 'renameAttribute',
        },
      ])(`$msg: $method()`, ({ method }) => {
        const testData = t_Dat.main[method];
        describe.each(testData.tests)('$msg $rem', ({ param }) => {
          describe.each(param)('$msg', ({ param }) => {
            describe.each(param)('$msg $rem', ({ preloads, values, status }) => {
              const { before: stat_b, after: stat_a } = status;
              let testInst = null; let obj = null; let options = undefined;
              beforeAll(() => {
                ({ obj, options } = preloads);
                testInst = new TXmlElementController(obj, options);
              });
              it('check instance status (before)', () => {
                expect(testInst instanceof TXmlElementController).toBe(true);
                //expect(item.textValue).toBe(result);
                expect(__unwrap_ec(testInst)).toStrictEqual(stat_b.obj);
              });
              it('perform ops', () => {
                const { ops, assertQty } = status;
                let result = null; let isERR = false;
                try {
                  result = runTestFn({ testInst, method }, values);
                  //console.log('CHECK: => NO_ERR');
                } catch (err) {
                  //console.log('CHECK: IS_ERR => '+err);
                  isERR = true;
                  result = err;
                } finally {
                  expect.assertions(assertQty);
                  expect(isERR).toBe(ops.isERR);
                  if (isERR) {
                    const { errType, errCode } = ops;
                    expect(result instanceof errType).toBe(true);
                    expect(result.code).toStrictEqual(errCode);
                  } else {
                    const { className, value } = ops;
                    if (typeof className === 'string') {
                      expect(result instanceof xmlDoc[className]).toBe(true);
                    } else {
                      expect(result).toStrictEqual(value);
                    };
                  };
                };
              });
              it('check instance status (after)', () => {
                expect(testInst instanceof TXmlElementController).toBe(true);
                //expect(item.textValue).toBe(result);
                expect(__unwrap_ec(testInst)).toStrictEqual(stat_a.obj);
              });
            });
          });
        });
      });
      //===
    });

    //===
  });

  //===
});
