>|***rev.*:**|0.0.12|
>|:---|---:|
>|date:|2024-07-31|

## Introduction

This paper describes a tests shipped within the package.

## Provided tests

- `xmldoc-lib` module:
  - class `TXmlAttributesMapper`
  - class `TXmlElementController`
  - class `TXmlElementsListController`
  - class `TXmlContentRootElement`
  - class `TXmlContentDeclaration`
  - class `TXmlContentContainer`
  - some base functions

- `xml-helper` module.

## Use cases

### All tests

To run all tests available use the next command:

`npm test` or `npm run test` or `npm run test-xml`

### Separate module tests

To run separate tests for each module or class you can use `--object` option:

|option value|module or class name|
|---|---|
|`xml:amap`|`TXmlAttributesMapper`|
|`xml:ec`|`TXmlElementController`|
|`xml:lc`|`TXmlElementsListController`|
|`xml:re`|`TXmlContentRootElement`|
|`xml:cdec`|`TXmlContentDeclaration`|
|`xml:cc`|`TXmlContentContainer`|
|`xml:helper`|`xmlHelper` (`xml-helper`)|
|`xml:bsf`|base functions (`xmldoc-lib`)|

> Example: `npm test -- --object=xml:amap`

To run a test for a `TXmlAttributesMapper` class use:

`npm run test-xml:amap`

To run a test for a `TXmlElementController` class use:

`npm run test-xml:ec`

To run a test for a `TXmlElementsListController` class use:

`npm run test-xml:lc`

To run a test for a `TXmlContentRootElement` class use:

`npm run test-xml:re`

To run a test for a `TXmlContentDeclaration` class use:

`npm run test-xml:cdec`

To run a test for a `TXmlContentContainer` class use:

`npm run test-xml:cc`
