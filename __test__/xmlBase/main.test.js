// [v0.1.002-20240801]

/**
 * @test-environment jest
 */

const { runTestFn } = require('#test-dir/test-hfunc.js');

const t_Mod = require('#lib/xmldoc-lib.js');

const t_Dat = require('./data/index.data.js');

describe.each([
  {
    msg: '1.1',
    method: 'readAsTagName',
  }, {
    msg: '1.2',
    method: 'readAsAttrName',
  /*}, {
    msg: '1.3',
    method: 'readAsAttrValue',
  }, {*/
    msg: '1.4',
    method: 'valueToElementID',
  },
])('$msg - function: xmldoc-lib.$method()', ({ method }) => {
  const testData = t_Dat[method];
  describe.each(testData.tests)('+ $msg $rem', ({ param }) => {
    describe.each(param)('+ $msg', ({ param }) => {
      it.each(param)('+ $msg $rem', ({ preloads, values, status }) => {
        const { ops, assertQty } = status;
        let result = undefined; let isERR = false;
        try {
          result = runTestFn({ testInst: t_Mod, method }, values);
          //console.log('CHECK: > NO_ERR');
        } catch (err) {
          //console.log('CHECK: IS_ERR => '+err);
          isERR = true;
          result = err;
        } finally {
          expect.assertions(assertQty);
          expect(isERR).toBe(ops.isERR);
          if (isERR) {
            const { errType, errCode } = ops;
            expect(err instanceof errType).toBe(true);
            expect(err.code).toStrictEqual(errCode);
          } else {
            const { className, value, __unwrap } = ops;
            switch (typeof className) {
              case 'string' : {
                expect(result instanceof globalThis[className]).toBe(true);
                if (className === 'HTMLElement') {
                  result = result.outerHTML;
                } else {
                  if (typeof __unwrap !== 'function') break;
                  result = __unwrap(result);
                };
              }
              default : {
                expect(result).toStrictEqual(value);
              }
            };
          };
        };
      });
    });
  });
});
