// [v0.1.003-20240801]

/* module:   `xmldoc-lib`
 * function: ---
 */

module.exports = {
  readAsTagName: require('./readAsTagName.data'),
  readAsAttrName: require('./readAsAttrName.data'),
  //readAsAttrValue: require('./readAsAttrValue.data'),
  valueToElementID: require('./valueToElementID.data'),
};
