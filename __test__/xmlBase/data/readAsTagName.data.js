// [v0.1.001-20240801]

/* module:   `xmldoc-lib`
 * function: readAsTagName()
 */

const { genTestCase } = require('#test-dir/test-hfunc.js');

const TEST_FUNC = (value) => { return value; };

const valueTestData = {
  msg: 'run tests against a "value" param',
  rem: '',
  param: [
    ...genTestCase({
      inGroup: 'no args are given',
      preloads: undefined,
      cases: [
        {
          msg: 'value is undefined',
          rem: '',
          values: {
            value: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          __unwrap: undefined,
          value: "",
        },
        assertQty: 2,
        before: undefined,
        after: undefined,
      },
    }, { auto: true }),
    ...genTestCase({
      inGroup: 'non-valid args are given',
      preloads: undefined,
      cases: [
        {
          msg: 'value is a "null"',
          rem: '',
          values: {
            value: null,
          },
        }, {
          msg: 'value is a boolean',
          rem: '',
          values: {
            value: true,
          },
        }, {
          msg: 'value is a number',
          rem: '',
          values: {
            value: 10,
          },
        }, {
          msg: 'value is a function',
          rem: '',
          values: {
            value: TEST_FUNC,
          },
        }, {
          msg: 'value is an array',
          rem: '',
          values: {
            value: [],
          },
        }, {
          msg: 'value is an object',
          rem: '',
          values: {
            value: {},
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          __unwrap: undefined,
          value: "",
        },
        assertQty: 2,
        before: undefined,
        after: undefined,
      },
    }, { auto: true }),
    ...genTestCase({
      inGroup: 'valid args are given',
      preloads: undefined,
      cases: [
        {
          msg: 'value is a string',
          rem: '',
          values: {
            value: 'active',
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          __unwrap: undefined,
          value: 'active',
        },
        assertQty: 2,
        before: undefined,
        after: undefined,
      },
    }, { auto: true }),
  ],
};

const resultTestData = {
  msg: 'run tests against a result values',
  rem: '',
  param: [
    ...genTestCase({
      inGroup: 'valid args are given',
      preloads: undefined,
      cases: [
        {
          msg: 'value is a string convertable to a number',
          rem: '(value must be ignored)',
          values: {
            value: '10',
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          __unwrap: undefined,
          value: '',
        },
        assertQty: 2,
        before: undefined,
        after: undefined,
      },
    }, { auto: true }),
    ...genTestCase({
      inGroup: 'tests againts a special cases',
      preloads: undefined,
      cases: [
        {
          msg: 'value is a string padded with spaces',
          rem: '(value must be trimmed)',
          values: {
            value: '  \t active  \n ',
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          __unwrap: undefined,
          value: 'active',
        },
        assertQty: 2,
        before: undefined,
        after: undefined,
      },
    }, { auto: true }),
  ],
};

module.exports = {
  descr: [
    valueTestData,
    resultTestData,
  ],
  tests: [
    valueTestData,
    resultTestData,
  ],
};
