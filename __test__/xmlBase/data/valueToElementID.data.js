// [v0.1.001-20240801]

/* module:   `xmldoc-lib`
 * function: valueToElementID()
 */

const { genTestCase } = require('#test-dir/test-hfunc.js');

const TEST_FUNC = (value) => { return value; };

const valueTestData = {
  msg: 'run tests against "value" param',
  rem: '',
  param: [
    ...genTestCase({
      inGroup: 'no args are given',
      preloads: undefined,
      cases: [
        {
          msg: 'value is undefined',
          rem: '',
          values: {
            value: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          __unwrap: undefined,
          value: '',
        },
        assertQty: 2,
        before: undefined,
        after: undefined,
      },
    }, { auto: true }),
    ...genTestCase({
      inGroup: 'non-valid args are given',
      preloads: undefined,
      cases: [
        {
          msg: 'value is a "null"',
          rem: '',
          values: {
            value: null,
          },
        }, {
          msg: 'value is a boolean',
          rem: '',
          values: {
            value: true,
          },
        }, {
          msg: 'value is a function',
          rem: '',
          values: {
            value: TEST_FUNC,
          },
        }, {
          msg: 'value is an array',
          rem: '',
          values: {
            value: [],
          },
        }, {
          msg: 'value is an object',
          rem: '',
          values: {
            value: {},
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          __unwrap: undefined,
          value: '',
        },
        assertQty: 2,
        before: undefined,
        after: undefined,
      },
    }, { auto: true }),
    ...genTestCase({
      inGroup: 'valid args are given',
      preloads: undefined,
      cases: [
        {
          msg: 'value is a number',
          rem: '',
          values: {
            value: 10,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          __unwrap: undefined,
          value: '10',
        },
        assertQty: 2,
        before: undefined,
        after: undefined,
      },
    }, { auto: true }),
    ...genTestCase({
      inGroup: 'valid args are given',
      preloads: undefined,
      cases: [
        {
          msg: 'value is a string',
          rem: '',
          values: {
            value: 'active',
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          __unwrap: undefined,
          value: 'active',
        },
        assertQty: 2,
        before: undefined,
        after: undefined,
      },
    }, { auto: true }),
  ],
};

const resultTestData = {
  msg: 'run tests against a result values',
  rem: '',
  param: [
    ...genTestCase({
      inGroup: 'cases when a strings convertable to a number',
      preloads: undefined,
      cases: [
        {
          msg: 'value is a represents a positive integer',
          rem: '',
          values: {
            value: '125',
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          __unwrap: undefined,
          value: '125',
        },
        assertQty: 2,
        before: undefined,
        after: undefined,
      },
    }, { auto: true }),
    ...genTestCase({
      inGroup: 'cases when a strings convertable to a number',
      preloads: undefined,
      cases: [
        {
          msg: 'value is a represents a negative number',
          rem: '(value must be ignored)',
          values: {
            value: '-125',
          },
        }, {
          msg: 'value is a represents a positive but not integer',
          rem: '(value must be ignored)',
          values: {
            value: '125.2567',
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          __unwrap: undefined,
          value: '',
        },
        assertQty: 2,
        before: undefined,
        after: undefined,
      },
    }, { auto: true }),
    ...genTestCase({
      inGroup: 'cases when value is a number and must be ignored',
      preloads: undefined,
      cases: [
        {
          msg: 'value is a negative number',
          rem: '',
          values: {
            value: -125,
          },
        }, {
          msg: 'value is a positive but not integer',
          rem: '(value must be ignored)',
          values: {
            value: 125.2567,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          __unwrap: undefined,
          value: '',
        },
        assertQty: 2,
        before: undefined,
        after: undefined,
      },
    }, { auto: true }),
  ],
};

const specialTestData = {
  msg: 'run tests against a special cases',
  rem: '',
  param: [
    ...genTestCase({
      inGroup: 'cases when value is not valid identifier',
      preloads: undefined,
      cases: [
        {
          msg: 'value contains a spaces',
          rem: '',
          values: {
            value: 'some string',
          },
        },
        // // TODO: add other cases
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          __unwrap: undefined,
          value: '',
        },
        assertQty: 2,
        before: undefined,
        after: undefined,
      },
    }, { auto: true }),
  ],
};

module.exports = {
  descr: [
    valueTestData,
    //resultTestData,
  ],
  tests: [
    valueTestData,
    resultTestData,
    specialTestData,
  ],
};
