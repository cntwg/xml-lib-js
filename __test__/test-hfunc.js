// [v0.1.010-20240923]

// === module init block ===

// === module extra block (helper functions) ===

const isArray = Array.isArray;

function isPlainObject(obj) {
  return (
    obj !== null
    && typeof obj === 'object'
    && !isArray(obj)
  );
};

const initOptions = (obj, options) => {
  /* place code that instantiates a preloads */
  return obj;
};

// === module main block ===

/***
 * (* constant definitions *)
 */

const DEF_XML_DECL_STR = '<?xml version="1.0" encoding="UTF-8"?>';

/***
 * (* function definitions *)
 */

const initElem = () => { return { __attr: {}, __text: '' } };

const initEmptyDoc = (rootETagName) => {
  if (
    typeof rootETagName !== 'string'
    || ((rootETagName = rootETagName.trim()) === '')
  ) {
    rootETagName = 'root';
  };
  return [
    DEF_XML_DECL_STR,
    [ '<', rootETagName, '></', rootETagName, '>' ].join(''),
  ].join('\n');
};

/**
 * @callback initTestProcEx
 * @param {object} obj
 * @param {object} [opt]
 * @returns {object}
 * @description user defined procedure to initialize a given test-object
 */

/**
 * @function initTest
 * @param {object} obj
 * @param {object} [opt]
 * @param {initTestProcEx} [cbio]
 * @returns {object}
 * @description initializes a given test-object
 */
const initTest = (obj, opt, cbio) => {
  const preloads = obj.preloads;
  if (isPlainObject(preloads) && isPlainObject(preloads.status)) {
    if (!isPlainObject(obj.status)) obj.status = {};
    obj.status.before = preloads.status;
  };
  if (opt) {
    return typeof cbio !== 'function' ? cbio(obj, opt) : initOptions(obj, opt);
  };
  return obj;
  //if (typeof cbio !== 'function') return obj;
  //return opt ? cbio(obj, opt) : obj;
  //return opt ? initOptions(obj, opt) : obj;
};

/**
 * @typedef testFuncDescr
 * @type {object}
 * @property {object} testInst
 * @property {string} method
 * @property {string} [prop]
 * @description description of a test function
 */

/**
 * @function runTestFn
 * @param {testFuncDescr} obj
 * @param {any} [args]
 * @returns {any}
 * @description runs a method of a given test-object
 */
function runTestFn(obj, args) {
  /**
   * @function getArgsList
   * @param {any} [args]
   * @returns {Array}
   * @private
   * @description returns a list of an arguments
   */
  const getArgsList = (args) => {
    if (args !== null && typeof args === 'object') {
      if (isArray(args)) return args;
      return Object.values(args);
    };
    return [];
  };
  if (isPlainObject(obj)) {
    const { testInst, method, prop } = obj;
    if (
      testInst !== null
      && typeof testInst === 'object'
    ) {
      if (method === '$getter') {
        //* call getter on instance property */
        return testInst[prop];
      } else if (method === '$setter') {
        //* call setter on instance property */
        return testInst[prop] = getArgsList(args)[0];
      } else if (typeof testInst[method] === 'function') {
        //* call instance method */
        return testInst[method](...getArgsList(args));
      };
    } else if (typeof testInst === 'function') {
      if (method === '$constructor') {
        //* call class constructor method */
        return new testInst(...getArgsList(args));
      } else if (typeof testInst[method] === 'function') {
        //* call class static method */
        return testInst[method](...getArgsList(args));
      }
    };
  };
};

/**
 * @function genTestCase
 * @param {object} data
 * @param {object} [opt]
 * @returns {?object}
 * @description generates a given test-object by a given conditions
 */
function genTestCase(data, opt) {
  if (isPlainObject(data)) {
    const {
      cases,
      preloads,
      status,
    } = data;
    let {
      inGroup,
    } = data;
    if (
      typeof inGroup === 'string'
      && ((inGroup = inGroup.trim()) !== '')
      && Array.isArray(cases)
    ) {
      const doRunInit = (
        isPlainObject(opt)
        && (typeof opt.auto === 'boolean' ? opt.auto : false)
      );
      let result = [];
      cases.forEach((state) => {
        if (isPlainObject(state)) {
          const {
            msg,
            rem,
            values,
          } = state;
          let caseDescr = {
            msg,
            rem,
            preloads,
            values,
            status,
            initTest(options, cb){ return initTest(this, options, cb); },
          };
          if (doRunInit) {
            const {
              param,
              initOptionsFn,
            } = opt;
            caseDescr = caseDescr.initTest(param, initOptionsFn);
          };
          result.push(caseDescr);
        };
      });
      if (result.length) {
        result = [{
          msg: inGroup,
          param: result,
        }];
        return result;
      };
    };
  };
};

/***
 * (* class definitions *)
 */

// === module exports block ===

module.exports.DEF_XML_DECL_STR = DEF_XML_DECL_STR;
module.exports.initElem = initElem;
module.exports.initEmptyDoc = initEmptyDoc;

module.exports.runTestFn = runTestFn;
module.exports.initTest = initTest;
module.exports.genTestCase = genTestCase;
