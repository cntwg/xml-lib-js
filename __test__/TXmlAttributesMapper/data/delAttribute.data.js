// [v0.1.005-20240801]

/* class:    TXmlAttributesMapper
 * method:   delAttribute
 * property: ---
 */

const {
  initElem: initHost,
  genTestCase,
} = require('#test-dir/test-hfunc.js');

const {
  initOptions,
} = require('../test-hfunc.js');

const EMPTY_STRING = '';
const EMPTY_OBJECT = {};

const preloads = {
  TEST_ELEMENT_E0: {
    get obj(){
      return {
        __attr: {
          key_432: 'value_234',
          key_434: 'value_434',
        },
        __text: '',
      };
    },
    get options(){
      return undefined;
    },
    status: {
      obj: {
        __attr: {
          key_432: 'value_234',
          key_434: 'value_434',
        },
        __text: '',
      },
      options: undefined,
      __unwrap: undefined,
      entries: [
        [ 'key_432', 'value_234' ],
        [ 'key_434', 'value_434' ],
      ],
    },
  },
  TEST_ELEMENT_E1: {
    get obj(){
      return {
        test_key: {
          key_432: 'value_234',
          key_434: 'value_434',
        },
        __text: '',
      };
    },
    get options(){
      return {
        parseOptions: {
          settings: {
            attributesKey: 'test_key',
          },
        },
      };
    },
    status: {
      obj: {
        test_key: {
          key_432: 'value_234',
          key_434: 'value_434',
        },
        __text: '',
      },
      options: {
        parseOptions: {
          settings: {
            attributesKey: 'test_key',
          },
        },
      },
      __unwrap: undefined,
      entries: [
        [ 'key_432', 'value_234' ],
        [ 'key_434', 'value_434' ],
      ],
    },
  },
};

const valueTestDescr = {
  msg: 'run tests against "attr" param',
  rem: '',
  param: [
    ...genTestCase({
      inGroup: 'no args are given',
      preloads: preloads.TEST_ELEMENT_E0,
      cases: [
        {
          msg: 'undefined value is passed',
          rem: '',
          values: {
            value: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: false,
          __unwrap: undefined,
        },
        assertQty: 2,
        before: undefined,
        after: preloads.TEST_ELEMENT_E0.status,
      },
    }, { auto: true }),
    ...genTestCase({
      inGroup: 'non-valid args are given',
      preloads: preloads.TEST_ELEMENT_E0,
      cases: [
        {
          msg: 'value is a "null"',
          rem: '',
          values: {
            value: null,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: false,
          __unwrap: undefined,
        },
        assertQty: 2,
        before: undefined,
        after: preloads.TEST_ELEMENT_E0.status,
      },
    }, { auto: true }),
    ...genTestCase({
      inGroup: 'valid args are given',
      preloads: preloads.TEST_ELEMENT_E0,
      cases: [
        {
          msg: 'value is a string',
          rem: '(non-empty and valid)',
          values: {
            value: 'key_432',
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: true,
          __unwrap: undefined,
        },
        assertQty: 2,
        before: undefined,
        after: {
          obj: {
            __attr: {
              key_434: 'value_434',
            },
            __text: '',
          },
          options: undefined,
          __unwrap: undefined,
          entries: [
            [ 'key_434', 'value_434' ],
          ],
        },
      },
    }, { auto: true }),
  ],
};

const resultTestDescr = {
  msg: 'run tests against a result values',
  rem: '(defaults used)',
  param: [
    ...genTestCase({
      inGroup: 'value is a string',
      preloads: preloads.TEST_ELEMENT_E0,
      cases: [
        {
          msg: 'value is a valid name',
          rem: '(attribute exists)',
          values: {
            value: 'key_432',
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: true,
          __unwrap: undefined,
        },
        assertQty: 2,
        before: undefined,
        after: {
          obj: {
            __attr: {
              key_434: 'value_434',
            },
            __text: '',
          },
          options: undefined,
          __unwrap: undefined,
          entries: [
            [ 'key_434', 'value_434' ],
          ],
        },
      },
    }, { auto: true }),
    ...genTestCase({
      inGroup: 'value is a string',
      preloads: preloads.TEST_ELEMENT_E0,
      cases: [
        {
          msg: 'value is a valid name',
          rem: '(attribute not exists)',
          values: {
            value: 'key_1232',
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: true,
          __unwrap: undefined,
        },
        assertQty: 2,
        before: undefined,
        after: preloads.TEST_ELEMENT_E0.status,
      },
    }, { auto: true }),
  ],
};

const extraTestDescr = {
  msg: 'run tests against result values',
  rem: '(constructor options defined)',
  param: [
    ...genTestCase({
      inGroup: 'options: "attributesKey" is set',
      preloads: preloads.TEST_ELEMENT_E1,
      cases: [
        {
          msg: 'perform test',
          rem: '',
          values: {
            value: 'key_434',
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: true,
          __unwrap: undefined,
        },
        assertQty: 2,
        before: undefined,
        after: {
          obj: {
            test_key: {
              key_432: 'value_234',
            },
            __text: '',
          },
          options: {
            parseOptions: {
              settings: {
                attributesKey: 'test_key',
              },
            },
          },
          entries: [
            [ 'key_432', 'value_234' ],
          ],
        },
      },
    }, { auto: true }),
  ],
};

module.exports = {
  descr: {
    valueTestDescr,
    resultTestDescr,
  },
  tests: [
    valueTestDescr,
    resultTestDescr,
    extraTestDescr,
  ],
};
