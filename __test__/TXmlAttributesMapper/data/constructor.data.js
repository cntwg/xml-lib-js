// [v0.1.003-20240624]

/* class:    TXmlAttributesMapper
 * method:   constructor
 * property: ---
 */

const { initElem } = require('#test-dir/test-hfunc.js');

const EMPTY_STRING = '';
const EMPTY_OBJECT = {};
const EMPTY_ELEM = { __attr: {}, __text: '' };

const initHost = () => { return { __attr: {}, __text: '' } };

const def_status = {
  get obj(){
    return initHost();
  },
  get options(){
    return {
      parseOptions: {
        settings: {
          attributesKey: '__attr',
        },
      },
    };
  },
  get entries(){
    return [];
  },
};

const objTestDescr = {
  msg: 'run tests aganst "obj" param (use default options)',
  param: [
    {
      msg: 'no args are given',
      preloads: {
        get obj(){ return undefined; },
        get options(){ return undefined; },
      },
      param: [
        {
          msg: 'undefined value is passed',
          rem: '("TypeError" thrown)',
          values: {
            get obj(){ return undefined; },
            get options(){ return undefined; },
          },
          status: {
            isERR: true,
            errType: TypeError,
            assertQty: 5,
            before: undefined,
            after: {
              obj: undefined,
              options: undefined,
              entries: undefined,
            },
          },
        },
      ],
    }, {
      msg: 'non-valid args are given',
      preloads: {
        get obj(){ return undefined; },
        get options(){ return undefined; },
      },
      param: [
        {
          msg: 'null value is passed',
          rem: '("TypeError" thrown)',
          values: {
            get obj(){ return null; },
            get options(){ return undefined; },
          },
          status: {
            isERR: true,
            errType: TypeError,
            assertQty: 5,
            before: undefined,
            after: {
              obj: null,
              options: undefined,
              entries: undefined,
            },
          },
        },
      ],
    }, {
      msg: 'an object value is passed',
      preloads: {
        get obj(){ return undefined; },
        get options(){ return undefined; },
      },
      param: [
        {
          msg: 'an empty object is given',
          rem: '',
          values: {
            get obj(){ return {}; },
            get options(){ return undefined; },
          },
          status: {
            isERR: false,
            errType: undefined,
            assertQty: 5,
            before: undefined,
            after: {
              obj: {},
              options: undefined,
              entries: [],
            },
          },
        }, {
          msg: 'an empty element is given (a pre-defined object)',
          rem: '',
          values: {
            get obj(){ return initHost(); },
            get options(){ return undefined; },
          },
          status: {
            isERR: false,
            errType: undefined,
            assertQty: 5,
            before: undefined,
            after: {
              obj: initHost(),
              options: undefined,
              entries: [],
            },
          },
        },
      ],
    },
  ],
};

const optTestDescr = {
  msg: 'run tests aganst "options" param',
  param: [
    {
      msg: 'no args are given',
      preloads: {
        get obj(){ return initHost(); },
        get options(){ return undefined; },
      },
      param: [
        {
          msg: 'undefined value is passed',
          rem: '(defaults is used)',
          values: {
            get obj(){ return undefined; },
            get options(){ return undefined; },
          },
          status: {
            isERR: false,
            errType: undefined,
            assertQty: 5,
            before: undefined,
            after: {
              obj: initHost(),
              options: undefined,
              entries: [],
            },
          },
        },
      ],
    }, {
      msg: 'non-valid args are given',
      preloads: {
        get obj(){ return initHost(); },
        get options(){ return undefined; },
      },
      param: [
        {
          msg: 'null value is passed',
          rem: '(value ignored, defaults is used)',
          values: {
            get obj(){ return undefined; },
            get options(){ return null; },
          },
          status: {
            isERR: false,
            errType: undefined,
            assertQty: 5,
            before: undefined,
            after: {
              obj: initHost(),
              options: null,
              entries: [],
            },
          },
        },
      ],
    }, {
      msg: 'a valid args are given',
      preloads: {
        get obj(){ return initHost(); },
        get options(){ return undefined; },
      },
      param: [
        {
          msg: 'an empty object is passed',
          rem: '',
          values: {
            get obj(){ return undefined; },
            get options(){ return {}; },
          },
          status: {
            isERR: false,
            errType: undefined,
            assertQty: 5,
            before: undefined,
            after: def_status,
          },
        }, {
          msg: 'an non-empty object is passed (template is used)',
          rem: '',
          values: {
            get obj(){ return undefined; },
            get options(){ return { parseOptions: {} }; },
          },
          status: {
            isERR: false,
            errType: undefined,
            assertQty: 5,
            before: undefined,
            after: def_status,
          },
        },
      ],
    },
  ],
};

module.exports = {
  descr: [
    objTestDescr,
    optTestDescr,
  ],
};
