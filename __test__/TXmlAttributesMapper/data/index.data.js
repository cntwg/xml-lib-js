// [v0.1.008-20240918]

/* class:    TXmlAttributesMapper
 * method:   ---
 * property: ---
 */

module.exports = {
  d1t: require('./constructor.data.js').descr,
  hasAttribute: require('./hasAttribute.data.js'),
  getAttribute: require('./getAttribute.data.js'),
  setAttribute: require('./setAttribute.data.js'),
  delAttribute: require('./delAttribute.data.js'),
  renameAttribute: require('./renameAttribute.data.js'),
  clear: require('./clear.data.js'),
  getProp: {},
  setProp: {},
};
