// [v0.1.005-20240801]

/* class:    TXmlAttributesMapper
 * method:   clear
 * property: ---
 */

const {
  initElem: initHost,
  genTestCase,
} = require('#test-dir/test-hfunc.js');

const EMPTY_STRING = '';
const EMPTY_OBJECT = {};

const preloads = {
  TEST_ELEMENT_E0: {
    get obj(){
      return {
        __attr: {
          key_432: 'value_234',
        },
        __text: '',
      };
    },
    get options(){
      return undefined;
    },
    status: {
      obj: {
        __attr: {
          key_432: 'value_234',
        },
        __text: '',
      },
      options: undefined,
      __unwrap: undefined,
      entries: [[ 'key_432', 'value_234' ]],
    },
  },
  TEST_ELEMENT_E1: {
    get obj(){
      return {
        test_key: {
          key_432: 'value_234',
          key_434: 'value_434',
        },
        __text: '',
      };
    },
    get options(){
      return {
        parseOptions: {
          settings: {
            attributesKey: 'test_key',
          },
        },
      };
    },
    status: {
      obj: {
        test_key: {
          key_432: 'value_234',
          key_434: 'value_434',
        },
        __text: '',
      },
      options: {
        parseOptions: {
          settings: {
            attributesKey: 'test_key',
          },
        },
      },
      __unwrap: undefined,
      entries: [
        [ 'key_432', 'value_234' ],
        [ 'key_434', 'value_434' ],
      ],
    },
  },
};

const resultTestDescr = {
  msg: 'run tests against result values',
  rem: '(defaults used)',
  param: [
    ...genTestCase({
      inGroup: 'some attributes exists',
      preloads: preloads.TEST_ELEMENT_E0,
      cases: [
        {
          msg: 'perform test',
          rem: '',
          values: {
            value: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: undefined,
          __unwrap: undefined,
        },
        assertQty: 2,
        before: undefined,
        after: {
          obj: {
            __attr: {},
            __text: '',
          },
          options: undefined,
          __unwrap: undefined,
          entries: [],
        },
      },
    }, { auto: true }),
  ],
};

const extraTestDescr = {
  msg: 'run tests against result values',
  rem: '(constructor options defined)',
  param: [
    ...genTestCase({
      inGroup: 'options: "attributesKey" is set',
      preloads: preloads.TEST_ELEMENT_E1,
      cases: [
        {
          msg: 'some attributes exists',
          rem: '',
          values: {
            value: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: undefined,
          __unwrap: undefined,
        },
        assertQty: 2,
        before: undefined,
        after: {
          obj: {
            test_key: {},
            __text: '',
          },
          options: {
            parseOptions: {
              settings: {
                attributesKey: 'test_key',
              },
            },
          },
          entries: [],
        },
      },
    }, { auto: true }),
  ],
};

module.exports = {
  descr: {
    resultTestDescr,
    extraTestDescr,
  },
  tests: [
    resultTestDescr,
    extraTestDescr,
  ],
};
