// [v0.1.008-20240801]

/* class:    TXmlAttributesMapper
 * method:   setAttribute
 * property: ---
 */

const {
  initElem: initHost,
  genTestCase,
} = require('#test-dir/test-hfunc.js');

const {
  initOptions,
} = require('../test-hfunc.js');

const EMPTY_STRING = '';
const EMPTY_OBJECT = {};

const preloads = {
  EMPTY_ELEMENT: {
    get obj(){
      return {};
    },
    get options(){
      return undefined;
    },
    status: {
      obj: {},
      options: undefined,
      __unwrap: undefined,
      entries: [],
    },
  },
  TEST_ELEMENT_E1: {
    get obj(){
      return {};
    },
    get options(){
      return {
        parseOptions: {
          settings: {
            attributesKey: 'test_key',
          },
        },
      };
    },
    status: {
      obj: {},
      options: {
        parseOptions: {
          settings: {
            attributesKey: 'test_key',
          },
        },
      },
      __unwrap: undefined,
      entries: [],
    },
  },
};

const attrTestDescr = {
  msg: 'run tests against "attr" param',
  rem: '(defaults used)',
  param: [
    ...genTestCase({
      inGroup: 'no args are given',
      preloads: preloads.EMPTY_ELEMENT,
      cases: [
        {
          msg: 'undefined value is passed',
          rem: '',
          values: {
            name: undefined,
            value: 'some value',
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: false,
          __unwrap: undefined,
        },
        assertQty: 2,
        before: undefined,
        after: preloads.EMPTY_ELEMENT.status,
      },
    }, { auto: true }),
    ...genTestCase({
      inGroup: 'non-valid args are given',
      preloads: preloads.EMPTY_ELEMENT,
      cases: [
        {
          msg: 'value is a "null"',
          rem: '',
          values: {
            name: null,
            value: 'some value',
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: false,
          __unwrap: undefined,
        },
        assertQty: 2,
        before: undefined,
        after: preloads.EMPTY_ELEMENT.status,
      },
    }, { auto: true }),
    ...genTestCase({
      inGroup: 'a valid args are given',
      preloads: preloads.EMPTY_ELEMENT,
      cases: [
        {
          msg: 'value is a non-empty string',
          rem: '',
          values: {
            name: 'key_123',
            value: 'some value',
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: true,
          __unwrap: undefined,
        },
        assertQty: 2,
        before: undefined,
        after: {
          obj: {
            __attr: {
              key_123: 'some value',
            },
          },
          options: undefined,
          entries: [
            [ 'key_123', 'some value' ],
          ],
        },
      },
    }, { auto: true }),
  ],
};

const valueTestDescr = {
  msg: 'run tests against "value" param',
  rem: '(defaults used)',
  param: [
    ...genTestCase({
      inGroup: 'no args are given',
      preloads: preloads.EMPTY_ELEMENT,
      cases: [
        {
          msg: 'undefined value is passed',
          rem: '',
          values: {
            name: 'key_123',
            value: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: false,
          __unwrap: undefined,
        },
        assertQty: 2,
        before: undefined,
        after: preloads.EMPTY_ELEMENT.status,
      },
    }, { auto: true }),
    ...genTestCase({
      inGroup: 'some args are given',
      preloads: preloads.EMPTY_ELEMENT,
      cases: [
        {
          msg: 'value is a "null"',
          rem: '',
          values: {
            name: 'key_123',
            value: null,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: true,
          __unwrap: undefined,
        },
        assertQty: 2,
        before: undefined,
        after: {
          obj: {
            __attr: {
              key_123: EMPTY_STRING,
            },
          },
          options: undefined,
          entries: [
            [ 'key_123', EMPTY_STRING ],
          ],
        },
      },
    }, { auto: true }),
    ...genTestCase({
      inGroup: 'some args are given',
      preloads: preloads.EMPTY_ELEMENT,
      cases: [
        {
          msg: 'value is a boolean',
          rem: '',
          values: {
            name: 'key_123',
            value: true,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: true,
          __unwrap: undefined,
        },
        assertQty: 2,
        before: undefined,
        after: {
          obj: {
            __attr: {
              key_123: 'true',
            },
          },
          options: undefined,
          entries: [
            [ 'key_123', 'true' ],
          ],
        },
      },
    }, { auto: true }),
    ...genTestCase({
      inGroup: 'some args are given',
      preloads: preloads.EMPTY_ELEMENT,
      cases: [
        {
          msg: 'value is a number',
          rem: '',
          values: {
            name: 'key_123',
            value: 125,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: true,
          __unwrap: undefined,
        },
        assertQty: 2,
        before: undefined,
        after: {
          obj: {
            __attr: {
              key_123: '125',
            },
          },
          options: undefined,
          entries: [
            [ 'key_123', '125' ],
          ],
        },
      },
    }, { auto: true }),
    ...genTestCase({
      inGroup: 'some args are given',
      preloads: preloads.EMPTY_ELEMENT,
      cases: [
        {
          msg: 'value is a string',
          rem: 'non-empty',
          values: {
            name: 'key_123',
            value: 'some value',
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: true,
          __unwrap: undefined,
        },
        assertQty: 2,
        before: undefined,
        after: {
          obj: {
            __attr: {
              key_123: 'some value',
            },
          },
          options: undefined,
          entries: [
            [ 'key_123', 'some value' ],
          ],
        },
      },
    }, { auto: true }),
  ],
};

const resultTestDescr = {
  msg: 'run tests against result values',
  rem: '(defaults used)',
  param: [
    ...genTestCase({
      inGroup: 'tests against special cases upon "value" param',
      preloads: preloads.EMPTY_ELEMENT,
      cases: [
        {
          /* [!] check implementation */
          msg: 'given value is an array [*]',
          rem: '',
          values: {
            name: 'key_123',
            value: [ 'some value' ],
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: true,
          __unwrap: undefined,
        },
        assertQty: 2,
        before: undefined,
        after: {
          obj: {
            __attr: {
              key_123: EMPTY_STRING,
            },
          },
          options: undefined,
          entries: [
            [ 'key_123', EMPTY_STRING ],
          ],
        },
      },
    }, { auto: true }),
    ...genTestCase({
      inGroup: 'tests against special cases upon "value" param',
      preloads: preloads.EMPTY_ELEMENT,
      cases: [
        {
          /* [!] check implementation */
          msg: 'given value is an object [*]',
          rem: '',
          values: {
            name: 'key_123',
            value: { a: 'some value' },
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: true,
          __unwrap: undefined,
        },
        assertQty: 2,
        before: undefined,
        after: {
          obj: {
            __attr: {
              key_123: EMPTY_STRING,
            },
          },
          options: undefined,
          entries: [
            [ 'key_123', EMPTY_STRING ],
          ],
        },
      },
    }, { auto: true }),
  ],
};

const extraTestDescr = {
  msg: 'run tests against result values',
  rem: '(constructor options defined)',
  param: [
    ...genTestCase({
      inGroup: 'options: "attributesKey" is set',
      preloads: preloads.TEST_ELEMENT_E1,
      cases: [
        {
          msg: 'perform test',
          rem: '',
          values: {
            name: 'key_123',
            value: 'some value',
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: true,
          __unwrap: undefined,
        },
        assertQty: 2,
        before: undefined,
        after: {
          obj: {
            test_key: {
              key_123: 'some value',
            },
          },
          options: {
            parseOptions: {
              settings: {
                attributesKey: 'test_key',
              },
            },
          },
          entries: [
            [ 'key_123', 'some value' ],
          ],
        },
      },
    }, { auto: true }),
  ],
};

const specialTestDescr = {
  msg: 'run tests against special cases',
  rem: '(defaults used)',
  param: [
    ...genTestCase({
      inGroup: 'cases where "attr" param is a string',
      preloads: preloads.EMPTY_ELEMENT,
      cases: [
        {
          msg: 'ensure a given value is trimmed',
          rem: '',
          values: {
            name: '   \t key_123   \n   ',
            value: 'some value',
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: true,
          __unwrap: undefined,
        },
        assertQty: 2,
        before: undefined,
        after: {
          obj: {
            __attr: {
              key_123: 'some value',
            },
          },
          options: undefined,
          entries: [
            [ 'key_123', 'some value' ],
          ],
        },
      },
    }, { auto: true }),
    ...genTestCase({
      inGroup: 'cases where "attr" param is a string',
      preloads: preloads.EMPTY_ELEMENT,
      cases: [
        {
          msg: 'ensure a space between is not allowed',
          rem: '',
          values: {
            name: 'key_123   key_223',
            value: 'some value',
          },
        }, {
          msg: 'ensure an "=" character is not allowed',
          rem: '',
          values: {
            name: 'key_123=key_223',
            value: 'some value',
          },
        }, {
          msg: 'ensure an "<" character is not allowed',
          rem: '',
          values: {
            name: 'key_123<key_223',
            value: 'some value',
          },
        }, {
          msg: 'ensure an ">" character is not allowed',
          rem: '',
          values: {
            name: 'key_123>key_223',
            value: 'some value',
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: false,
          __unwrap: undefined,
        },
        assertQty: 2,
        before: undefined,
        after: preloads.EMPTY_ELEMENT.status,
      },
    }, { auto: true }),
  ],
};

module.exports = {
  descr: {
    attrTestDescr,
    valueTestDescr,
    resultTestDescr,
  },
  tests: [
    attrTestDescr,
    valueTestDescr,
    resultTestDescr,
    extraTestDescr,
    specialTestDescr,
  ],
};
