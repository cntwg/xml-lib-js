// [v0.1.020-20240918]

const { runTestFn } = require('#test-dir/test-hfunc.js');

const xmlDoc = require('#lib/xmldoc-lib.js');
const TXmlAttributesMapper = xmlDoc.TXmlAttributesMapper;

const t_Dat = require('./data/index.data.js');

// class: 'TXmlAttributesMapper'
describe('class: TXmlAttributesMapper', () => {
  describe('1. create a new instance', () => {
    describe.each(t_Dat.d1t)('- $msg', ({ param }) => {
      describe.each(param)('$msg', ({ preloads, param }) => {
        describe.each(param)('$msg $rem', ({ values, status }) => {
          const testFn = (...args) => { return new TXmlAttributesMapper(...args); };
          const { before: stat_b, after: stat_a } = status;
          let { obj, options } = values;
          let testInst = null;
          beforeAll(() => {
            testInst = undefined;
            if (obj === undefined) obj = preloads.obj;
            if (options === undefined) options = preloads.options;
          });
          it('perform ops', () => {
            try {
              testInst = testFn(obj, options);
              expect(status.isERR).toBe(false);
              expect(testInst instanceof TXmlAttributesMapper).toBe(true);
              expect(testInst.entries).toStrictEqual(stat_a.entries);
            } catch (err) {
              expect(status.isERR).toBe(true);
              expect(err instanceof status.errType).toBe(true);
              expect(testInst).toBeUndefined();
            };
            expect(obj).toStrictEqual(stat_a.obj);
            expect(options).toStrictEqual(stat_a.options);
            expect.assertions(status.assertQty);
          });
        });
      });
    });
  });

  describe('2. run tests aganst instance properties/methods', () => {
    describe.each([
      {
        msg: '2.1',
        method: 'hasAttribute',
      }, {
        msg: '2.2',
        method: 'getAttribute',
      }, {
        msg: '2.3',
        method: 'setAttribute',
      }, {
        msg: '2.4',
        method: 'delAttribute',
      }, {
        msg: '2.5',
        method: 'renameAttribute',
      }, {
        msg: '2.6',
        method: 'clear',
      },
    ])('$msg - method: $method()', ({ method }) => {
      const data = t_Dat[method];
      describe.each(data.tests)('$msg $rem', ({ param }) => {
        describe.each(param)('+ $msg', ({ param }) => {
          describe.each(param)('+ $msg $rem', ({ preloads, values, status }) => {
            const { before: stat_b, after: stat_a } = status;
            let testInst = null; let obj = null; let options = undefined;
            beforeAll(() => {
              ({ obj, options } = preloads);
              testInst = new TXmlAttributesMapper(obj, options);
            });
            it('check instance status (before)', () => {
              expect(testInst instanceof TXmlAttributesMapper).toBe(true);
              expect(obj).toStrictEqual(stat_b.obj);
              expect(options).toStrictEqual(stat_b.options);
              expect(testInst.entries).toStrictEqual(stat_b.entries);
            });
            it('perform ops', () => {
              const { ops, assertQty } = status;
              let result = null; let isERR = false;
              try {
                result = runTestFn({ testInst, method }, values);
                //console.log('CHECK: => NO_ERR');
              } catch (err) {
                //console.log('CHECK: IS_ERR => '+err);
                isERR = true;
                result = err;
              } finally {
                expect.assertions(assertQty);
                expect(isERR).toBe(ops.isERR);
                if (isERR) {
                  const { errType, errCode } = ops;
                  expect(result instanceof errType).toBe(true);
                  expect(result.code).toStrictEqual(errCode);
                } else {
                  const { className, value, __unwrap } = ops;
                  switch (typeof className) {
                    case 'string' : {
                      expect(result instanceof xmlDoc[className]).toBe(true);
                      if (className !== 'HTMLElement') {
                        if (typeof __unwrap !== 'function') break;
                        result = __unwrap(result);
                      };
                    }
                    default : {
                      expect(result).toStrictEqual(value);
                    }
                  };
                };
              };
            });
            it('check instance status (after)', () => {
              expect(testInst instanceof TXmlAttributesMapper).toBe(true);
              expect(obj).toStrictEqual(stat_a.obj);
              expect(options).toStrictEqual(stat_a.options);
              expect(testInst.entries).toStrictEqual(stat_a.entries);
            });
          });
        });
      });
    });

    /**/
  });

  /**/
});
