// [v0.1.001-20240731]

// === module init block ===

// === module extra block (helper functions) ===

// === module main block ===

/***
 * (* constant definitions *)
 */

/***
 * (* function definitions *)
 */

const initOptions = (obj, opt) => {
  if (
    opt !== null
    && typeof opt === 'object'
    && !Array.isArray(opt)
    && obj !== null
    && typeof obj === 'object'
    && !Array.isArray(obj)
  ) {
    const { preloads } = obj;
    if (
      preloads !== null
      && typeof preloads === 'object'
      && !Array.isArray(preloads)
    ) {
      const {
        obj: host,
        status,
      } = preloads;
      const { options } = opt;
      obj.preloads = {
        obj: host,
        options,
        status,
      };
    };
  };
  return obj;
};

/***
 * (* class definitions *)
 */

// === module exports block ===

module.exports.initOptions = initOptions;
