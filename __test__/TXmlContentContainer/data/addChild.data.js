// [v0.1.005-20240624]

/* class:    TXmlContentContainer
 * method:   addChild
 * property: ---
 */

const { TXmlContentParseOptions } = require('@ygracs/xobj-lib-js');

const {
  DEF_XML_DECL_STR,
  initEmptyDoc,
} = require('#test-dir/test-hfunc.js');

const preloads = {
  get options(){ return {}; },
  content: [
    DEF_XML_DECL_STR,
    '<root>',
    '  <note></note>',
    '</root>',
  ].join('\n'),
  status: {
    content: [
      DEF_XML_DECL_STR,
      '<root>',
      '  <note></note>',
      '</root>',
    ].join('\n'),
    options: {
      parseOptions: new TXmlContentParseOptions(),
      proxyModeEnable: false,
      rootETagName: 'root',
      autoBindRoot: true,
      //isNullItemsAllowed: false,
    },
    root: {
      note: {},
    },
  },
};

const resultTestDescr = [
  {
    msg: 'no elements exists',
    preloads: {
      options: preloads.options,
      content: undefined,
    },
    param: [
      {
        msg: 'add first element',
        value: 'note',
        status: {
          child: {
            class: 'TXmlElementController',
            obj: {},
          },
          before: {
            content: initEmptyDoc(),
            options: preloads.status.options,
            root: {},
          },
          after: {
            content: [
              DEF_XML_DECL_STR,
              '<root>',
              '  <note></note>',
              '</root>',
            ].join('\n'),
            options: preloads.status.options,
            root: {
              note: {},
            },
          },
        },
      },
    ],
  }, {
    msg: 'some elements exists',
    preloads: preloads,
    param: [
      {
        msg: 'same element is not exists',
        value: 'autor',
        status: {
          child: {
            class: 'TXmlElementController',
            obj: {},
          },
          before: preloads.status,
          after: {
            content: [
              DEF_XML_DECL_STR,
              '<root>',
              '  <note></note>',
              '  <autor></autor>',
              '</root>',
            ].join('\n'),
            options: preloads.status.options,
            root: {
              note: {},
              autor: {},
            },
          },
        },
      }, {
        msg: 'same element is exists',
        value: 'note',
        status: {
          child: {
            class: 'TXmlElementController',
            obj: {},
          },
          before: preloads.status,
          after: {
            content: [
              DEF_XML_DECL_STR,
              '<root>',
              '  <note></note>',
              '  <note></note>',
              '</root>',
            ].join('\n'),
            options: preloads.status.options,
            root: {
              note: [
                {},
                {},
              ],
            },
          },
        },
      },
    ],
  },
];

module.exports = {
  descr: resultTestDescr,
};
