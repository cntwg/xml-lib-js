// [v0.1.004-20240624]

/* class:    TXmlContentContainer
 * method:   getChild
 * property: ---
 */

const { TXmlContentParseOptions } = require('@ygracs/xobj-lib-js');

const {
  DEF_XML_DECL_STR,
  initEmptyDoc,
} = require('#test-dir/test-hfunc.js');

const preloads = {
  get options(){ return {}; },
  content: [
    DEF_XML_DECL_STR,
    '<root>',
    '  <record>1235</record>',
    '  <item>apple</item>',
    '  <item>sword</item>',
    '  <item>rock</item>',
    '</root>',
  ].join('\n'),
  status: {
    content: [
      DEF_XML_DECL_STR,
      '<root>',
      '  <record>1235</record>',
      '  <item>apple</item>',
      '  <item>sword</item>',
      '  <item>rock</item>',
      '</root>',
    ].join('\n'),
    options: {
      parseOptions: new TXmlContentParseOptions(),
      proxyModeEnable: false,
      rootETagName: 'root',
      autoBindRoot: true,
      //isNullItemsAllowed: false,
    },
    root: {
      record: { __text: '1235' },
      item: [
        { __text: 'apple' },
        { __text: 'sword' },
        { __text: 'rock' },
      ],
    },
  },
};

const resultTestDescr = [
  {
    msg: 'no elements exists',
    preloads: {
      options: preloads.options,
      content: undefined,
    },
    param: [
      {
        msg: 'get some element',
        value: 'note',
        status: {
          child: {
            class: undefined,
            obj: null,
          },
          before: {
            content: initEmptyDoc(),
            options: preloads.status.options,
            root: {},
          },
        },
      },
    ],
  }, {
    msg: 'some elements exists',
    preloads: preloads,
    param: [
      {
        msg: 'get some element that is not exists',
        value: 'note',
        status: {
          child: {
            class: undefined,
            obj: null,
          },
          before: preloads.status,
        },
      }, {
        msg: 'get some element that is exists',
        value: 'record',
        status: {
          child: {
            class: 'TXmlElementController',
            obj: { __text: '1235' },
          },
          before: preloads.status,
        },
      }, {
        msg: 'get some element that is exists (an array like)',
        value: 'item',
        status: {
          child: {
            class: 'TXmlElementsListController',
            obj: null,
          },
          before: preloads.status,
        },
      },
    ],
  },
];

module.exports = {
  descr: resultTestDescr,
};
