// [v0.1.007-20240625]

/* class:    TXmlContentContainer
 * method:   loadFromFileSync, loadFromFile
 * property: ---
 */

const { TXmlContentParseOptions } = require('@ygracs/xobj-lib-js');

const { initEmptyDoc } = require('#test-dir/test-hfunc.js');

const { errCodeMap } = require('#test-dir/test-sys-helper.js');

const path = require('path');

const OS_OEL = require('os').EOL;

const preloads = {
  get options(){ return {}; },
  status: {
    content: initEmptyDoc(),
    options: {
      parseOptions: new TXmlContentParseOptions(),
      proxyModeEnable: false,
      rootETagName: 'root',
      autoBindRoot: true,
      //isNullItemsAllowed: false,
    },
  },
};

const valueTestDescr = {
  msg: 'run tests aganst "value" parameter',
  param: [
    {
      msg: 'no args are given',
      preloads: preloads,
      param: [
        {
          msg: 'undefined value is passed',
          value: undefined,
          status: {
            ops: {
              value: {
                isLoaded: false,
                isERR: true,
                errEvent: 'NO_SRCLINK',
                source: '',
                content: '',
              },
              isERR: false,
              assertQty: 2,
            },
            get before(){
              return valueTestDescr.param[0].preloads.status;
            },
            get after(){
              return valueTestDescr.param[0].preloads.status;
            },
          },
        },
      ],
    }, {
      msg: 'non-valid args are given',
      preloads: preloads,
      param: [
        {
          msg: 'value is a "null"',
          value: null,
          status: {
            ops: {
              value: {
                isLoaded: false,
                isERR: true,
                errEvent: 'NO_SRCLINK',
                source: '',
                content: '',
              },
              isERR: false,
              assertQty: 2,
            },
            get before(){
              return valueTestDescr.param[1].preloads.status;
            },
            get after(){
              return valueTestDescr.param[1].preloads.status;
            },
          },
        },
      ],
    }, {
      msg: 'valid args are given',
      preloads: preloads,
      param: [
        {
          msg: 'value is a string (non-empty)',
          value: path.resolve(
            '__test__/xml.samples',
            'doc-sample.xml',
          ),
          status: {
            ops: {
              value: {
                isLoaded: true,
                isERR: false,
                errEvent: 'OPS_IS_SUCCEED',
                source: path.resolve(
                  '__test__/xml.samples',
                  'doc-sample.xml',
                ),
                content: [
                  '<?xml version="1.0" encoding="UTF-8"?>', OS_OEL,
                  '<tv></tv>', OS_OEL,
                ].join(''),
              },
              isERR: false,
              assertQty: 2,
            },
            get before(){
              return valueTestDescr.param[2].preloads.status;
            },
            after: {
              content: [
                '<?xml version="1.0" encoding="UTF-8"?>\n',
                '<tv></tv>',
              ].join(''),
              options: {
                parseOptions: new TXmlContentParseOptions(),
                proxyModeEnable: false,
                rootETagName: 'tv',
                autoBindRoot: true,
                //isNullItemsAllowed: false,
              },
            },
          },
        },
      ],
    },
  ],
};

const resultTestDescr = {
  msg: 'run tests aganst special cases',
  param: [
    {
      msg: 'non-valid args are given',
      preloads: preloads,
      param: [
        {
          msg: 'value is an empty string',
          value: '',
          status: {
            ops: {
              value: {
                isLoaded: false,
                isERR: true,
                errEvent: 'EMPTY_SRCLINK',
                source: '',
                content: '',
              },
              isERR: false,
              assertQty: 2,
            },
            get before(){
              return resultTestDescr.param[0].preloads.status;
            },
            after: {
              content: initEmptyDoc(),
              options: {
                parseOptions: new TXmlContentParseOptions(),
                proxyModeEnable: false,
                rootETagName: 'root',
                autoBindRoot: true,
                //isNullItemsAllowed: false,
              },
            },
          },
        }, {
          msg: 'value is a string filled with a spaces only',
          value: '  \n  \t \n',
          status: {
            ops: {
              value: {
                isLoaded: false,
                isERR: true,
                errEvent: 'EMPTY_SRCLINK',
                source: '',
                content: '',
              },
              isERR: false,
              assertQty: 2,
            },
            get before(){
              return resultTestDescr.param[0].preloads.status;
            },
            after: {
              content: initEmptyDoc(),
              options: {
                parseOptions: new TXmlContentParseOptions(),
                proxyModeEnable: false,
                rootETagName: 'root',
                autoBindRoot: true,
                //isNullItemsAllowed: false,
              },
            },
          },
        },
      ],
    }, {
      msg: 'valid args are given (value is non-empty string)',
      preloads: preloads,
      param: [
        {
          msg: 'target file is not exists',
          value: path.resolve(
            '__test__/temp/xml.samples',
            'doc-sample.xml',
          ),
          status: {
            ops: {
              value: {
                isLoaded: false,
                isERR: true,
                errCode: errCodeMap.getCode('ENOENT'),
                errEvent: 'ENOENT',
                errMsg: 'ERR_FILE_ENOENT',
                source: path.resolve(
                  '__test__/temp/xml.samples',
                  'doc-sample.xml',
                ),
                content: '',
              },
              isERR: false,
              assertQty: 2,
            },
            get before(){
              return resultTestDescr.param[1].preloads.status;
            },
            after: {
              content: initEmptyDoc(),
              options: {
                parseOptions: new TXmlContentParseOptions(),
                proxyModeEnable: false,
                rootETagName: 'root',
                autoBindRoot: true,
                //isNullItemsAllowed: false,
              },
            },
          },
        }, {
          msg: 'target file has no content',
          value: path.resolve(
            '__test__/xml.samples',
            'empty-sample.xml',
          ),
          status: {
            ops: {
              value: {
                isLoaded: false,
                isERR: true,
                errEvent: 'NO_CONTENT',
                source: path.resolve(
                  '__test__/xml.samples',
                  'empty-sample.xml',
                ),
                content: '',
              },
              isERR: false,
              assertQty: 2,
            },
            get before(){
              return resultTestDescr.param[1].preloads.status;
            },
            after: {
              content: initEmptyDoc(),
              options: {
                parseOptions: new TXmlContentParseOptions(),
                proxyModeEnable: false,
                rootETagName: 'root',
                autoBindRoot: true,
                //isNullItemsAllowed: false,
              },
            },
          },
        },
      ],
    },
  ],
};

module.exports = {
  preloads: preloads,
  descr: [
    valueTestDescr,
    resultTestDescr,
  ],
};
