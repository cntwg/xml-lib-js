// [v0.1.009-20240925]

/* class:    TXmlContentContainer
 * method:   loadFromXMLString
 * property: ---
 */

const { genTestCase } = require('#test-dir/test-hfunc.js');

const { TXmlContentParseOptions } = require('@ygracs/xobj-lib-js');

const {
  DEF_XML_DECL_STR,
  initEmptyDoc,
} = require('#test-dir/test-hfunc.js');

const OS_OEL = require('os').EOL;

const preloads = {
  DEF_OPTIONS: {
    get options() { return {}; },
    status: {
      content: initEmptyDoc(),
      options: {
        parseOptions: new TXmlContentParseOptions(),
        proxyModeEnable: false,
        rootETagName: 'root',
        autoBindRoot: true,
        //isNullItemsAllowed: false,
      },
    },
  },
  TEST_CONTAINER: {
    get text() {
      return [
        DEF_XML_DECL_STR,
        '<note>',
        '<id>NOTE_001</id>',
        '<label>event note</label>',
        '<date>2024-09-25T20:20:00+02:00</date>',
        '</note>',
      ].join('');
    },
    get options() { return {}; },
    status: {
      content: [
        DEF_XML_DECL_STR,
        '<note>',
        '  <id>NOTE_001</id>',
        '  <label>event note</label>',
        '  <date>2024-09-25T20:20:00+02:00</date>',
        '</note>',
      ].join('\n'),
      options: {
        parseOptions: new TXmlContentParseOptions(),
        proxyModeEnable: false,
        rootETagName: 'note',
        autoBindRoot: true,
        //isNullItemsAllowed: false,
      },
    },
  },
};

const valueTestDescr = {
  msg: 'run tests against "value" parameter',
  rem: '',
  param: [
    ...genTestCase({
      inGroup: 'no args are given',
      preloads: preloads.DEF_OPTIONS,
      cases: [
        {
          msg: 'undefined value is passed',
          rem: '',
          values: {
            text: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: false,
          __unwrap: undefined,
        },
        assertQty: 2,
        before: undefined,
        after: preloads.DEF_OPTIONS.status,
      },
    }, { auto: true }),
    ...genTestCase({
      inGroup: 'non-valid args are given',
      preloads: preloads.DEF_OPTIONS,
      cases: [
        {
          msg: 'value is a "null"',
          rem: '',
          values: {
            text: null,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: false,
          __unwrap: undefined,
        },
        assertQty: 2,
        before: undefined,
        after: preloads.DEF_OPTIONS.status,
      },
    }, { auto: true }),
    ...genTestCase({
      inGroup: 'valid args are given',
      preloads: preloads.DEF_OPTIONS,
      cases: [
        {
          msg: 'value is a string with valid content',
          rem: '',
          values: {
            text: [
              DEF_XML_DECL_STR,
              '<note>',
              '<id>NOTE_001</id>',
              '<label>event note</label>',
              '<date>2024-09-25T20:20:00+02:00</date>',
              '</note>',
            ].join(''),
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: true,
          __unwrap: undefined,
        },
        assertQty: 2,
        before: undefined,
        after: {
          content: [
            DEF_XML_DECL_STR,
            '<note>',
            '  <id>NOTE_001</id>',
            '  <label>event note</label>',
            '  <date>2024-09-25T20:20:00+02:00</date>',
            '</note>',
          ].join('\n'),
          options: {
            parseOptions: new TXmlContentParseOptions(),
            proxyModeEnable: false,
            rootETagName: 'note',
            autoBindRoot: true,
            //isNullItemsAllowed: false,
          },
        },
      },
    }, { auto: true }),
  ],
};

const resultTestDescr = {
  msg: 'run tests against special cases',
  rem: '',
  param: [
    ...genTestCase({
      inGroup: 'load empty strings of any kind',
      preloads: preloads.DEF_OPTIONS,
      cases: [
        {
          msg: 'value is an empty string',
          rem: '',
          values: {
            text: '',
          },
        },
        {
          msg: 'value is a string filled with a spaces only',
          rem: '',
          values: {
            text: '  \n  \t \n',
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: true,
          __unwrap: undefined,
        },
        assertQty: 2,
        before: undefined,
        after: preloads.DEF_OPTIONS.status,
      },
    }, { auto: true }),
    ...genTestCase({
      inGroup: 'tests where previous content was loaded',
      preloads: preloads.TEST_CONTAINER,
      cases: [
        {
          msg: 'ensure that a content was reloaded',
          rem: '',
          values: {
            text: [
              DEF_XML_DECL_STR,
              '<record>',
              '<type>note</type>',
              '<id>NOTE_001</id>',
              '<label>journal event</label>',
              '<date>2024-09-25T20:20:00+02:00</date>',
              '</record>',
            ].join(''),
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: true,
          __unwrap: undefined,
        },
        assertQty: 2,
        before: undefined,
        after: {
          content: [
            DEF_XML_DECL_STR,
            '<record>',
            '  <type>note</type>',
            '  <id>NOTE_001</id>',
            '  <label>journal event</label>',
            '  <date>2024-09-25T20:20:00+02:00</date>',
            '</record>',
          ].join('\n'),
          options: {
            parseOptions: new TXmlContentParseOptions(),
            proxyModeEnable: false,
            rootETagName: 'record',
            autoBindRoot: true,
            //isNullItemsAllowed: false,
          },
        },
      },
    }, { auto: true }),
  ],
};

module.exports = {
  //descr: [],
  tests: [
    valueTestDescr,
    resultTestDescr,
  ],
};
