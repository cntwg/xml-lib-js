// [v0.1.006-20240625]

/* class:    TXmlContentContainer
 * method:   saveToFileSync, saveToFile
 * property: ---
 */

const { TXmlContentParseOptions } = require('@ygracs/xobj-lib-js');

const { initEmptyDoc } = require('#test-dir/test-hfunc.js');

const { errCodeMap } = require('#test-dir/test-sys-helper.js');

const path = require('path');

const OS_OEL = require('os').EOL;

const preloads = {
  get options(){ return {}; },
  status: {
    content: initEmptyDoc(),
    options: {
      parseOptions: new TXmlContentParseOptions(),
      proxyModeEnable: false,
      rootETagName: 'root',
      autoBindRoot: true,
      //isNullItemsAllowed: false,
    },
  },
};

const valueTestDescr = {
  msg: 'run tests aganst "value" parameter',
  param: [
    {
      msg: 'no args are given',
      param: [
        ({
          msg: 'undefined value is passed',
          rem: '',
          preloads: preloads,
          values: {
            source: undefined,
          },
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: {
                isSucceed: false,
                isERR: true,
                errEvent: 'NO_SRCLINK',
                source: '',
                content: '',
              },
            },
            assertQty: 2,
            before: undefined,
            after: undefined,
          },
          initTest(){
            this.status.before = this.preloads.status;
            return this;
          },
        }).initTest(),
      ],
    }, {
      msg: 'non-valid args are given',
      param: [
        ({
          msg: 'value is a "null"',
          rem: '',
          preloads: preloads,
          values: {
            source: null,
          },
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: {
                isSucceed: false,
                isERR: true,
                errEvent: 'NO_SRCLINK',
                source: '',
                content: '',
              },
            },
            assertQty: 2,
            before: undefined,
            after: undefined,
          },
          initTest(){
            this.status.before = this.preloads.status;
            return this;
          },
        }).initTest(),
      ],
    /*}, {
      msg: 'valid args are given',
      preloads: preloads,
      param: [
        {
          msg: 'value is a string (non-empty)',
          value: path.resolve(
            'test/xml.samples',
            'doc-sample.xml',
          ),
          status: {
            ops: {
              value: {
                isSucceed: true,
                isERR: false,
                errEvent: 'OPS_IS_SUCCEED',
                source: path.resolve(
                  'test/xml.samples',
                  'doc-sample.xml',
                ),
                content: [
                  '<?xml version="1.0" encoding="UTF-8"?>\r\n',
                  '<tv></tv>\r\n',
                ].join(''),
              },
              isERR: false,
              assertQty: 2,
            },
            get before(){
              return valueTestDescr.param[2].preloads.status;
            },
            after: undefined,
          },
        },
      ],*/
    },
  ],
};

const resultTestDescr = {
  msg: 'run tests aganst special cases',
  param: [
    {
      msg: 'non-valid args are given',
      preloads: preloads,
      param: [
        ({
          msg: 'value is an empty string',
          rem: '',
          preloads: preloads,
          values: {
            source: '',
          },
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: {
                isSucceed: false,
                isERR: true,
                errEvent: 'EMPTY_SRCLINK',
                source: '',
                content: '',
              },
            },
            assertQty: 2,
            before: undefined,
            after: undefined,
          },
          initTest(){
            this.status.before = this.preloads.status;
            return this;
          },
        }).initTest(),
        ({
          msg: 'value is a string filled with a spaces only',
          rem: '',
          preloads: preloads,
          values: {
            source: '  \n  \t \n',
          },
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: {
                isSucceed: false,
                isERR: true,
                errEvent: 'EMPTY_SRCLINK',
                source: '',
                content: '',
              },
            },
            assertQty: 2,
            before: undefined,
            after: undefined,
          },
          initTest(){
            this.status.before = this.preloads.status;
            return this;
          },
        }).initTest(),
      ],
    }, {
      msg: 'valid args are given (value is non-empty string)',
      preloads: preloads,
      param: [
        ({
          msg: 'target file is not exists',
          rem: '(no sub-dir in file path)',
          preloads: preloads,
          values: {
            source: path.resolve(
              '__test__/temp/xml.samples',
              'sample-1.save.xml',
            ),
          },
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: {
                isSucceed: false,
                isERR: true,
                errCode: errCodeMap.getCode('ENOENT'),
                errEvent: 'ENOENT',
                errMsg: 'ERR_FILE_ENOENT',
                source: path.resolve(
                  '__test__/temp/xml.samples',
                  'sample-1.save.xml',
                ),
                content: '',
              },
            },
            assertQty: 2,
            before: undefined,
            after: undefined,
          },
          initTest(){
            this.status.before = this.preloads.status;
            return this;
          },
        }).initTest(),
        /*{
          msg: 'target file has no content',
          value: path.resolve(
            'test/xml.samples',
            'empty-sample.xml',
          ),
          status: {
            ops: {
              value: {
                isSucceed: false,
                isERR: true,
                errEvent: 'NO_CONTENT',
                source: path.resolve(
                  'test/xml.samples',
                  'empty-sample.xml',
                ),
                content: '',
              },
              isERR: false,
              assertQty: 2,
            },
            get before(){
              return resultTestDescr.param[1].preloads.status;
            },
            after: undefined,
          },
        },*/
      ],
    },
  ],
};

module.exports = {
  preloads: preloads,
  descr: [
    valueTestDescr,
    resultTestDescr,
  ],
};
