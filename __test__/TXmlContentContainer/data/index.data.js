// [v0.1.011-20230305]

/* class:    TXmlContentContainer
 * method:   ---
 * property: ---
 */

module.exports = {
  d1t: require('./constructor.data.js').descr,
  main: {
    loadFromXMLString: require('./loadFromXMLString.data.js'),
    loadFromFileSync: require('./loadFromFile.data'),
    loadFromFile: require('./loadFromFile.data'),
    _bindContent: require('./_bindContent.data.js'),
    saveToXMLString: require('./saveToXMLString.data.js'),
    saveToFileSync: require('./saveToFile.data.js'),
    saveToFile: require('./saveToFile.data.js'),
  },
  root: {
    addChild: require('./addChild.data.js'),
    getChild: require('./getChild.data.js'),
  },
  parent: undefined,
  getProp: {},
  setProp: {},
};
