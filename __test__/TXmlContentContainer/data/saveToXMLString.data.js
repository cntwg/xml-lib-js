// [v0.1.002-20240624]

/* class:    TXmlContentContainer
 * method:   saveToXMLString
 * property: ---
 */

const { TXmlContentParseOptions } = require('@ygracs/xobj-lib-js');

const { initEmptyDoc } = require('#test-dir/test-hfunc.js');

const path = require('path');

const OS_OEL = require('os').EOL;

const preloads = {
  get options(){ return {}; },
  status: {
    content: initEmptyDoc(),
    options: {
      parseOptions: new TXmlContentParseOptions(),
      proxyModeEnable: false,
      rootETagName: 'root',
      autoBindRoot: true,
      //isNullItemsAllowed: false,
    },
  },
};

const resultTestDescr = {
  msg: 'run tests aganst result values',
  param: [
    {
      msg: 'instance has none elements after it was created',
      param: [
        ({
          msg: 'run test',
          rem: '',
          preloads: preloads,
          values: undefined,
          status: {
            ops: {
              isERR: false,
              errType: undefined,
              errCode: undefined,
              className: undefined,
              value: [
                '<?xml version="1.0" encoding="UTF-8"?>\n',
                '<root></root>',
              ].join(''),
            },
            assertQty: 2,
            before: undefined,
            after: undefined,
          },
          initTest(){
            this.status.before = this.preloads.status;
            return this;
          },
        }).initTest(),
      ],
    },
  ],
};

module.exports = {
  preloads: preloads,
  descr: [
    resultTestDescr,
  ],
};
