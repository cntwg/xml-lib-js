// [v0.1.002-20240624]

/* class:    TXmlContentContainer
 * method:   constructor
 * property: ---
 */

const { TXmlContentParseOptions } = require('@ygracs/xobj-lib-js');

const { initEmptyDoc } = require('#test-dir/test-hfunc.js');

const optTestDescr = {
  msg: 'run tests aganst "option" param',
  param: [
    {
      msg: 'no args are given',
      param: [
        {
          msg: 'undefined value is passed',
          value: {
            get options(){ return undefined; },
          },
          status: {
            content: initEmptyDoc(),
            options: undefined,
          },
        },
      ],
    }, {
      msg: 'non-valid args are given',
      param: [
        {
          msg: 'value is a "null"',
          value: {
            get options(){ return null; },
          },
          status: {
            content: initEmptyDoc(),
            options: null,
          },
        },
      ],
    }, {
      msg: 'a valid args are given',
      param: [
        {
          msg: 'value is an empty object',
          value: {
            get options(){ return {}; },
          },
          status: {
            content: initEmptyDoc(),
            options: {
              parseOptions: new TXmlContentParseOptions(),
              proxyModeEnable: false,
              rootETagName: 'root',
              autoBindRoot: true,
            },
          },
        },
      ],
    },
  ],
};

const resultTestDescr = {
  msg: 'run tests aganst special cases',
  param: [
    {
      msg: 'pre-defined tag for root element is used',
      param: [
        {
          msg: 'option: autoBindRoot => "default"',
          value: {
            get options(){
              return {
                rootETagName: 'article',
              };
            },
          },
          status: {
            content: initEmptyDoc('article'),
            options: {
              parseOptions: new TXmlContentParseOptions(),
              proxyModeEnable: false,
              rootETagName: 'article',
              autoBindRoot: true,
            },
          },
        }, {
          msg: 'option: autoBindRoot => "false"',
          value: {
            get options(){
              return {
                rootETagName: 'article',
                autoBindRoot: false,
              };
            },
          },
          status: {
            content: initEmptyDoc('article'),
            options: {
              parseOptions: new TXmlContentParseOptions(),
              proxyModeEnable: false,
              rootETagName: 'article',
              autoBindRoot: false,
            },
          },
        },
      ],
    },
  ],
};

module.exports = {
  descr: [
    optTestDescr,
    resultTestDescr,
  ],
};
