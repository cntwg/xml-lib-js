// [v0.1.006-20240624]

/* class:    TXmlContentContainer
 * method:   _bindContent
 * property: ---
 */

const { TXmlContentParseOptions } = require('@ygracs/xobj-lib-js');

const { initEmptyDoc } = require('#test-dir/test-hfunc.js');

const preloads = {
  get options(){ return {}; },
  status: {
    content: initEmptyDoc(),
    options: {
      parseOptions: new TXmlContentParseOptions(),
      proxyModeEnable: false,
      rootETagName: 'root',
      autoBindRoot: true,
      //isNullItemsAllowed: false,
    },
  },
};

const valueTestDescr = {
  msg: 'run tests aganst "value" parameter (defaults is used)',
  param: [
    {
      msg: 'no args are given',
      preloads: preloads,
      param: [
        {
          msg: 'undefined value is passed',
          value: {
            obj: undefined,
            options: undefined,
          },
          status: {
            ops: false,
            get before(){
              return valueTestDescr.param[0].preloads.status;
            },
            get after(){
              return valueTestDescr.param[0].preloads.status;
            },
          },
        },
      ],
    }, {
      msg: 'non-valid args are given',
      preloads: preloads,
      param: [
        {
          msg: 'value is a "null"',
          value: {
            obj: null,
            options: undefined,
          },
          status: {
            ops: false,
            get before(){
              return valueTestDescr.param[1].preloads.status;
            },
            get after(){
              return valueTestDescr.param[1].preloads.status;
            },
          },
        },
      ],
    }, {
      msg: 'valid args are given',
      preloads: preloads,
      param: [
        {
          msg: 'value is an empty object',
          value: {
            obj: {},
            options: undefined,
          },
          status: {
            ops: true,
            get before(){
              return valueTestDescr.param[2].preloads.status;
            },
            get after(){
              return valueTestDescr.param[2].preloads.status;
            },
          },
        }, {
          msg: 'value is a non-empty object (default root is used) [*]',
          // // TODO: check method implementation
          value: {
            obj: { root: {} },
            options: undefined,
          },
          status: {
            ops: true,
            get before(){
              return valueTestDescr.param[2].preloads.status;
            },
            after: {
              content: [
                '<root></root>', //* [?] not a 1st element
                '<?xml version="1.0" encoding="UTF-8"?>',//'\n',
              ].join(''),
              options: {
                parseOptions: new TXmlContentParseOptions(),
                proxyModeEnable: false,
                rootETagName: 'root',
                autoBindRoot: true,
                //isNullItemsAllowed: false,
              },
            },
          },
        },
      ],
    },
  ],
};

module.exports = {
  preloads: preloads,
  descr: [
    valueTestDescr,
    //resultTestDescr,
  ],
};
