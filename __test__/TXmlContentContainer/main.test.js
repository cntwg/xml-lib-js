// [v0.1.021-20240925]

const { runTestFn } = require('#test-dir/test-hfunc.js');

const xmlDoc = require('#lib/xmldoc-lib.js');
const TXmlElementController = xmlDoc.TXmlElementController;
//const TXmlElementsListController = xmlDoc.TXmlElementsListController;
//const TXmlAttributesMapper = xmlDoc.TXmlAttributesMapper;
const TXmlContentRootElement = xmlDoc.TXmlContentRootElement;
//const TXmlContentDeclaration = xmlDoc.TXmlContentDeclaration;
const TXmlContentContainer = xmlDoc.TXmlContentContainer;

const t_Dat = require('./data/index.data.js');

const __unwrap_ec = TXmlElementController.__unwrap;
const __unwrap_rc = TXmlContentRootElement.__unwrap;

// class: 'TXmlContentContainer'
describe('class: TXmlContentContainer', () => {
  describe('1. create a new instance (auto mode)', () => {
    const testData = t_Dat.d1t;
    describe.each(testData)('$msg', ({ param }) => {
      describe.each(param)('$msg', ({ param }) => {
        describe.each(param)('$msg', ({ value, status }) => {
          const testFn = (...args) => { return new TXmlContentContainer(...args); };
          let testInst = null; let options = null;
          beforeAll(() => {
            ({ options } = value);
          });
          it('done', () => {
            expect(() => { testInst = testFn(options); }).not.toThrow(Error);
          });
          it('check instance status (after)', () => {
            expect(testInst instanceof TXmlContentContainer).toBe(true);
            expect(options).toStrictEqual(status.options);
          });
          it('check instance root element', () => {
            let root = null;
            expect(() => { root = testInst.rootElement; }).not.toThrow(Error);
            expect(root instanceof TXmlContentRootElement).toBe(true);
            expect(root instanceof TXmlElementController).toBe(true);
          });
          it('check instance text content', () => {
            let content = '';
            expect(() => { content = testInst.saveToXMLString(); }).not.toThrow(Error);
            expect(content).toBe(status.content);
          });
        });
      });
    });
  });

  describe('2. run tests aganst an instance properties/methods', () => {
    describe.each([
      {
        msg: '2.4 - method',
        method: 'loadFromXMLString',
      },
    ])('$msg: $method()', ({ method }) => {
      const testData = t_Dat.main[method];
      describe.each(testData.tests)('+ $msg $rem', ({ param }) => {
        describe.each(param)('$msg', ({ param }) => {
          describe.each(param)('+ $msg $rem', ({ preloads, values, status }) => {
            const { before: stat_b, after: stat_a} = status;
            let testInst = null; let options = null;
            beforeAll(() => {
              ({ options } = preloads);
              testInst = new TXmlContentContainer(options);
              if (preloads.text !== undefined) {
                testInst.loadFromXMLString(preloads.text);
              };
            });
            it('check instance status (before)', () => {
              expect(testInst instanceof TXmlContentContainer).toBe(true);
              expect(options).toStrictEqual(stat_b.options);
            });
            it('perform ops', () => {
              const { ops, assertQty } = status;
              let result = null; let isERR = false;
              try {
                result = runTestFn({ testInst, method }, values);
                //console.log('CHECK: => NO_ERR');
              } catch (err) {
                //console.log('CHECK: IS_ERR => '+err);
                isERR = true;
                result = err;
              } finally {
                expect.assertions(assertQty);
                expect(isERR).toBe(ops.isERR);
                if (isERR) {
                  const { errType, errCode } = ops;
                  expect(result instanceof errType).toBe(true);
                  expect(result.code).toStrictEqual(errCode);
                } else {
                  const { className, value } = ops;
                  if (typeof className === 'string') {
                    expect(result instanceof xmlDoc[className]).toBe(true);
                  } else {
                    expect(result).toStrictEqual(value);
                  };
                };
              };
            });
            it('check instance status (after)', () => {
              expect(testInst instanceof TXmlContentContainer).toBe(true);
              expect(options).toStrictEqual(stat_a.options);
            });
            it('check instance content (after)', () => {
              let content = '';
              expect(() => {
                content = testInst.saveToXMLString();
              }).not.toThrow(Error);
              expect(content).toBe(stat_a.content);
            });
          });
        });
      });
    });

    describe('2.2 - method: loadFromFileSync()', () => {
      const method = 'loadFromFileSync';
      const testData = t_Dat.main[method];
      describe.each(testData.descr)('$msg', ({ param }) => {
        describe.each(param)('$msg', ({ preloads, param }) => {
          describe.each(param)('$msg', ({ value, status }) => {
            const testFn = (...args) => { return testInst[method](...args); };
            const { before: stat_b, after: stat_a} = status;
            let testInst = null; let options = null;
            beforeAll(() => {
              options = preloads.options;
              testInst = new TXmlContentContainer(options);
            });
            it('check instance status (before)', () => {
              expect(testInst instanceof TXmlContentContainer).toBe(true);
              expect(options).toStrictEqual(stat_b.options);
            });
            it('check instance content (before)', () => {
              let content = '';
              expect(() => { content = testInst.saveToXMLString(); }).not.toThrow(Error);
              expect(content).toBe(stat_b.content);
            });
            it('perform ops', () => {
              const { value: ops, isERR, assertQty } = status.ops;
              let result = undefined;
              if (isERR) {
                expect(() => { result = testFn(value); }).toThrow(Error);
              } else {
                expect(() => { result = testFn(value); }).not.toThrow(Error);
                expect(result).toStrictEqual(ops);
              };
              expect.assertions(assertQty);
            });
            it('check instance status (after)', () => {
              expect(testInst instanceof TXmlContentContainer).toBe(true);
              expect(options).toStrictEqual(stat_a.options);
            });
            it('check instance content (after)', () => {
              let content = '';
              expect(() => { content = testInst.saveToXMLString(); }).not.toThrow(Error);
              expect(content).toBe(stat_a.content);
            });
          });
        });
      });
    });

    describe('2.3 - method: loadFromFile()', () => {
      const method = 'loadFromFile';
      const testData = t_Dat.main[method];
      describe.each(testData.descr)('$msg', ({ param }) => {
        describe.each(param)('$msg', ({ preloads, param }) => {
          describe.each(param)('$msg', ({ value, status }) => {
            const testFn = (...args) => { return testInst[method](...args); };
            const { before: stat_b, after: stat_a} = status;
            let testInst = null; let options = null;
            beforeAll(() => {
              options = preloads.options;
              testInst = new TXmlContentContainer(options);
            });
            it('check instance status (before)', () => {
              expect(testInst instanceof TXmlContentContainer).toBe(true);
              expect(options).toStrictEqual(stat_b.options);
            });
            it('check instance content (before)', () => {
              let content = '';
              expect(() => { content = testInst.saveToXMLString(); }).not.toThrow(Error);
              expect(content).toBe(stat_b.content);
            });
            it('perform ops', async () => {
              const { value: ops, isERR, assertQty } = status.ops;
              let result = undefined;
              try {
                result = await testFn(value);
                expect(result).toStrictEqual(ops);
                expect(isERR).toStrictEqual(false);
              } catch (err) {
                console.log('[***]'+err);
                expect(isERR).toStrictEqual(true);
              };
              expect.assertions(assertQty);
            });
            it('check instance status (after)', () => {
              expect(testInst instanceof TXmlContentContainer).toBe(true);
              expect(options).toStrictEqual(stat_a.options);
            });
            it('check instance content (after)', () => {
              let content = '';
              expect(() => { content = testInst.saveToXMLString(); }).not.toThrow(Error);
              expect(content).toBe(stat_a.content);
            });
          });
        });
      });
    });

    describe('2.4 - method: saveToXMLString()', () => {
      const method = 'saveToXMLString';
      const testData = t_Dat.main[method];
      describe.each(testData.descr)('$msg', ({ param }) => {
        describe.each(param)('$msg', ({ param }) => {
          describe.each(param)('$msg', ({ preloads, values, status }) => {
            const testFn = (...args) => { return testInst[method](...args); };
            const { before: stat_b, after: stat_a} = status;
            let testInst = null; let options = null;
            beforeAll(() => {
              ({ options } = preloads);
              testInst = new TXmlContentContainer(options);
            });
            it('check instance status (before)', () => {
              expect(testInst instanceof TXmlContentContainer).toBe(true);
              expect(options).toStrictEqual(stat_b.options);
            });
            it('perform ops', () => {
              const { ops, assertQty } = status;
              let result = null; let isERR = false;
              try {
                result = testFn();
                //console.log('CHECK: => NO_ERR');
              } catch (err) {
                //console.log('CHECK: IS_ERR => '+err);
                isERR = true;
                result = err;
              } finally {
                expect.assertions(assertQty);
                expect(isERR).toBe(ops.isERR);
                if (isERR) {
                  const { errType, errCode } = ops;
                  expect(result instanceof errType).toBe(true);
                  expect(result.code).toStrictEqual(errCode);
                } else {
                  const { className, value } = ops;
                  if (typeof className === 'string') {
                    expect(result instanceof xmlDoc[className]).toBe(true);
                  } else {
                    expect(result).toStrictEqual(value);
                  };
                };
              };
            });
          });
        });
      });
    });

    describe('2.5 - method: saveToFileSync()', () => {
      const method = 'saveToFileSync';
      const testData = t_Dat.main[method];
      describe.each(testData.descr)('$msg', ({ param }) => {
        describe.each(param)('$msg', ({ param }) => {
          describe.each(param)('$msg $rem', ({ preloads, values, status }) => {
            const testFn = (...args) => { return testInst[method](...args); };
            const { before: stat_b, after: stat_a} = status;
            let testInst = null; let options = null;
            beforeAll(() => {
              ({ options } = preloads);
              testInst = new TXmlContentContainer(options);
            });
            it('check instance status (before)', () => {
              expect(testInst instanceof TXmlContentContainer).toBe(true);
              expect(options).toStrictEqual(stat_b.options);
            });
            it('perform ops', () => {
              const { ops, assertQty } = status;
              const { source } = values;
              let result = null; let isERR = false;
              try {
                result = testFn(source);
                //console.log('CHECK: => NO_ERR');
              } catch (err) {
                //console.log('CHECK: IS_ERR => '+err);
                isERR = true;
                result = err;
              } finally {
                expect.assertions(assertQty);
                expect(isERR).toBe(ops.isERR);
                if (isERR) {
                  const { errType, errCode } = ops;
                  expect(result instanceof errType).toBe(true);
                  expect(result.code).toStrictEqual(errCode);
                } else {
                  const { className, value } = ops;
                  if (typeof className === 'string') {
                    expect(result instanceof xmlDoc[className]).toBe(true);
                  } else {
                    expect(result).toStrictEqual(value);
                  };
                };
              };
            });
          });
        });
      });
    });

    describe('2.6 - method: saveToFile()', () => {
      const method = 'saveToFile';
      const testData = t_Dat.main[method];
      describe.each(testData.descr)('$msg', ({ param }) => {
        describe.each(param)('$msg', ({ param }) => {
          describe.each(param)('$msg $rem', ({ preloads, values, status }) => {
            const testFn = (...args) => { return testInst[method](...args); };
            const { before: stat_b, after: stat_a} = status;
            let testInst = null; let options = null;
            beforeAll(() => {
              options = preloads.options;
              testInst = new TXmlContentContainer(options);
            });
            it('check instance status (before)', () => {
              expect(testInst instanceof TXmlContentContainer).toBe(true);
              expect(options).toStrictEqual(stat_b.options);
            });
            it('perform ops', async () => {
              const { ops, assertQty } = status;
              const { source } = values;
              let result = null; let isERR = false;
              try {
                result = await testFn(source);
                //console.log('CHECK: => NO_ERR');
              } catch (err) {
                //console.log('CHECK: IS_ERR => '+err);
                isERR = true;
                result = err;
              } finally {
                expect.assertions(assertQty);
                expect(isERR).toBe(ops.isERR);
                if (isERR) {
                  const { errType, errCode } = ops;
                  expect(result instanceof errType).toBe(true);
                  expect(result.code).toStrictEqual(errCode);
                } else {
                  const { className, value } = ops;
                  if (typeof className === 'string') {
                    expect(result instanceof xmlDoc[className]).toBe(true);
                  } else {
                    expect(result).toStrictEqual(value);
                  };
                };
              };
            });
          });
        });
      });
    });
    //===
  });

  describe('3 - run test aganst an instance special methods', () => {
    describe('3.1 - method: _bindContent()', () => {
      const method = '_bindContent';
      const testData = t_Dat.main[method];
      describe.each(testData.descr)('$msg', ({ param }) => {
        describe.each(param)('$msg', ({ preloads, param }) => {
          describe.each(param)('$msg', ({ value, status }) => {
            const testFn = (...args) => { return testInst[method](...args); };
            const { before: stat_b, after: stat_a} = status;
            let testInst = null; let options = null;
            beforeAll(() => {
              options = preloads.options;
              testInst = new TXmlContentContainer(options);
            });
            it('check instance status (before)', () => {
              expect(testInst instanceof TXmlContentContainer).toBe(true);
              expect(options).toStrictEqual(stat_b.options);
            });
            it('check instance content (before)', () => {
              let content = '';
              expect(() => { content = testInst.saveToXMLString(); }).not.toThrow(Error);
              expect(content).toBe(stat_b.content);
            });
            it('perform ops', () => {
              const { obj, options } = value;
              let result = undefined;
              expect(() => { result = testFn(obj, options); }).not.toThrow(Error);
              expect(result).toStrictEqual(status.ops);
            });
            it('check instance status (after)', () => {
              expect(testInst instanceof TXmlContentContainer).toBe(true);
              expect(options).toStrictEqual(stat_a.options);
            });
            it('check instance content (after)', () => {
              let content = '';
              expect(() => { content = testInst.saveToXMLString(); }).not.toThrow(Error);
              expect(content).toBe(stat_a.content);
            });
          });
        });
      });
    });
  });

  describe('3. run test ops upon an instance root element', () => {
    describe('3.1 - method: addChild()', () => {
      const method = 'addChild';
      const testData = t_Dat.root[method];
      describe.each(testData.descr)('$msg', ({ preloads, param }) => {
        describe.each(param)('$msg', ({ value, status }) => {
          const testFn = (obj, ...args) => { return obj[method](...args); };
          const { before: stat_b, after: stat_a} = status;
          let testInst = null; let options = null;
          let root = null; let child = null;
          beforeAll(() => {
            let content = preloads.content;
            options = preloads.options;
            testInst = new TXmlContentContainer(options);
            if (typeof content === 'string') testInst.loadFromXMLString(content);
            root = testInst.rootElement;
            child = null;
          });
          it('check instance status (before)', () => {
            expect(testInst instanceof TXmlContentContainer).toBe(true);
            expect(root instanceof TXmlContentRootElement).toBe(true);
            expect(testInst.saveToXMLString()).toBe(stat_b.content);
            expect(options).toStrictEqual(stat_b.options);
            expect(__unwrap_rc(root)).toStrictEqual(stat_b.root);
          });
          it('perform ops', () => {
            const stat_c = status.child;
            expect(() => { child = testFn(root, value); }).not.toThrow(TypeError);
            if (stat_c.class) {
              expect(child instanceof xmlDoc[stat_c.class]).toBe(true);
            };
            expect(__unwrap_ec(child)).toStrictEqual(stat_c.obj);
          });
          it('check instance status (after)', () => {
            expect(testInst instanceof TXmlContentContainer).toBe(true);
            expect(root instanceof TXmlContentRootElement).toBe(true);
            expect(testInst.saveToXMLString()).toBe(stat_a.content);
            expect(options).toStrictEqual(stat_a.options);
            expect(__unwrap_rc(root)).toStrictEqual(stat_a.root);
          });
        });
      });
    });

    describe('3.1 - method: getChild()', () => {
      const method = 'getChild';
      const testData = t_Dat.root[method];
      describe.each(testData.descr)('$msg', ({ preloads, param }) => {
        describe.each(param)('$msg', ({ value, status }) => {
          const testFn = (obj, ...args) => { return obj[method](...args); };
          const { before: stat_b, after: stat_a} = status;
          let testInst = null; let options = null;
          let root = null; let child = null;
          beforeAll(() => {
            let content = preloads.content;
            options = preloads.options;
            testInst = new TXmlContentContainer(options);
            if (typeof content === 'string') testInst.loadFromXMLString(content);
            root = testInst.rootElement;
            child = null;
          });
          it('check instance status (before)', () => {
            expect(testInst instanceof TXmlContentContainer).toBe(true);
            expect(root instanceof TXmlContentRootElement).toBe(true);
            expect(testInst.saveToXMLString()).toBe(stat_b.content);
            expect(options).toStrictEqual(stat_b.options);
            expect(__unwrap_rc(root)).toStrictEqual(stat_b.root);
          });
          it('perform ops', () => {
            const stat_c = status.child;
            expect(() => { child = testFn(root, value); }).not.toThrow(TypeError);
            if (stat_c.class) {
              expect(child instanceof xmlDoc[stat_c.class]).toBe(true);
            };
            expect(__unwrap_ec(child)).toStrictEqual(stat_c.obj);
          });
        });
      });
    });
    //===
  });
  //===
});
