// [v0.1.002-20240701]

// === module init block ===

const sysID = process.platform;

// === module extra block (helper functions) ===

// === module main block ===

//console.log(`TEST: sysID=[${sysID}]`);
const errCodeMap = {
  ENOENT: {
    win32: -4058,
    linux: -2,
    def: -2,
  },
  EISDIR: {},
  EPERM: {},
  get getCode() {
    return function (id) {
      if (this[id]) return this[id][sysID];
    };
  },
};

// === module exports block ===

module.exports.errCodeMap = errCodeMap;
