// [v0.1.007-20220906]

/* class:    TXmlContentRootElement
 * method:   ---
 * property: ---
 */

const { TXmlContentParseOptions } = require('@ygracs/xobj-lib-js');

//const EMPTY_STRING = '';
//const EMPTY_OBJECT = {};
//const EMPTY_ELEM = { __attr: {}, __text: '' };

//const initHost = () => { return { __attr: {}, __text: '' } };

const data_d1t_obj = {
  msg: 'run tests aganst "obj" param (use default options)',
  param: [
    {
      msg: 'no args are given',
      param: [
        {
          msg: 'undefined value is passed',
          msgEx: '"Error" thrown',
          value: {
            get obj(){ return undefined; },
            get options(){ return undefined; },
          },
          status: {
            ops: {
              isERR: true,
              classNames: undefined,
              assertQty: 0,
            },
            econ: null,
            rcon: null,
            obj: undefined,
            options: undefined,
          },
        },
      ],
    }, {
      msg: 'non-valid args are given',
      param: [
        {
          msg: 'value is "null"',
          msgEx: '"Error" thrown',
          value: {
            get obj(){ return null; },
            get options(){ return undefined; },
          },
          status: {
            ops: {
              isERR: true,
              classNames: undefined,
              assertQty: 0,
            },
            econ: null,
            rcon: null,
            obj: null,
            options: undefined,
          },
        },
      ],
    }, {
      msg: 'a valid args are given',
      param: [
        {
          msg: 'value is an empty object',
          msgEx: 'done',
          value: {
            get obj(){ return {}; },
            get options(){ return undefined; },
          },
          status: {
            ops: {
              isERR: false,
              classNames: [
                'TXmlContentRootElement',
                'TXmlElementController',
              ],
              assertQty: 2,
            },
            econ: {},
            rcon: {},
            obj: { root: {} },
            options: undefined,
          },
        },
      ],
    },
  ],
};

const data_d1t_opt = {
  msg: 'run tests aganst "option" param',
  param: [
    {
      msg: 'no args are given',
      param: [
        {
          msg: 'defaults is used',
          msgEx: 'done',
          value: {
            get obj(){ return {}; },
            get options(){ return undefined; },
          },
          status: {
            ops: {
              isERR: false,
              classNames: [
                'TXmlContentRootElement',
                'TXmlElementController',
              ],
              assertQty: 2,
            },
            econ: {},
            rcon: {},
            obj: { root: {} },
            options: undefined,
          },
        },
      ],
    }, {
      msg: 'non-valid args are given',
      param: [
        {
          msg: 'value is "null"',
          msgEx: 'done',
          value: {
            get obj(){ return {}; },
            get options(){ return null; },
          },
          status: {
            ops: {
              isERR: false,
              classNames: [
                'TXmlContentRootElement',
                'TXmlElementController',
              ],
              assertQty: 2,
            },
            econ: {},
            rcon: {},
            obj: { root: {} },
            options: null,
          },
        },
      ],
    }, {
      msg: 'a valid args are given',
      param: [
        {
          msg: 'value is an empty object',
          msgEx: 'done',
          value: {
            get obj(){ return {}; },
            get options(){ return {}; },
          },
          status: {
            ops: {
              isERR: false,
              classNames: [
                'TXmlContentRootElement',
                'TXmlElementController',
              ],
              assertQty: 2,
            },
            econ: {},
            rcon: {},
            obj: { root: {} },
            options: {
              parseOptions: new TXmlContentParseOptions(),
              proxyModeEnable: false,
              rootETagName: 'root',
              autoBindRoot: true,
            },
          },
        },
      ],
    },
  ],
};

const data_d1t_res = {
  msg: 'run tests aganst special cases',
  param: [
    {
      msg: 'set a pre-defined root element on empty object',
      param: [
        {
          msg: 'a tag name is an empty string',
          msgEx: 'done',
          value: {
            get obj(){ return {}; },
            get options(){
              return {
                rootETagName: '',
              };
            },
          },
          status: {
            ops: {
              isERR: false,
              classNames: [
                'TXmlContentRootElement',
                'TXmlElementController',
              ],
              assertQty: 2,
            },
            econ: {},
            rcon: {},
            obj: { root: {} },
            options: {
              parseOptions: new TXmlContentParseOptions(),
              proxyModeEnable: false,
              rootETagName: 'root',
              autoBindRoot: true,
            },
          },
        }, {
          msg: 'a tag name is a non-empty string',
          msgEx: 'done',
          value: {
            get obj(){ return {}; },
            get options(){
              return {
                rootETagName: 'article',
              };
            },
          },
          status: {
            ops: {
              isERR: false,
              classNames: [
                'TXmlContentRootElement',
                'TXmlElementController',
              ],
              assertQty: 2,
            },
            econ: {},
            rcon: {},
            obj: { article: {} },
            options: {
              parseOptions: new TXmlContentParseOptions(),
              proxyModeEnable: false,
              rootETagName: 'article',
              autoBindRoot: true,
            },
          },
        },
      ],
    }, {
      msg: 'set a pre-defined root element on non-empty object',
      param: [
        {
          msg: 'a tag name is an empty string',
          msgEx: 'done',
          value: {
            get obj(){
              return { node: {}, };
            },
            get options(){
              return {
                rootETagName: '',
              };
            },
          },
          status: {
            ops: {
              isERR: false,
              classNames: [
                'TXmlContentRootElement',
                'TXmlElementController',
              ],
              assertQty: 2,
            },
            econ: {},
            rcon: {},
            obj: { node: {} },
            options: {
              parseOptions: new TXmlContentParseOptions(),
              proxyModeEnable: false,
              rootETagName: 'node',
              autoBindRoot: true,
            },
          },
        }, {
          msg: 'a tag name is a non-empty string (element exists)',
          msgEx: 'done',
          value: {
            get obj(){
              return { node: {}, };
            },
            get options(){
              return {
                rootETagName: 'node',
              };
            },
          },
          status: {
            ops: {
              isERR: false,
              classNames: [
                'TXmlContentRootElement',
                'TXmlElementController',
              ],
              assertQty: 2,
            },
            econ: {},
            rcon: {},
            obj: { node: {} },
            options: {
              parseOptions: new TXmlContentParseOptions(),
              proxyModeEnable: false,
              rootETagName: 'node',
              autoBindRoot: true,
            },
          },
        }, {
          msg: 'a tag name is a non-empty string (element not exists)',
          msgEx: 'done',
          value: {
            get obj(){
              return { node: {}, };
            },
            get options(){
              return {
                rootETagName: 'article',
              };
            },
          },
          status: {
            ops: {
              isERR: false,
              classNames: [
                'TXmlContentRootElement',
                'TXmlElementController',
              ],
              assertQty: 2,
            },
            econ: {},
            rcon: {},
            obj: { node: {} },
            options: {
              parseOptions: new TXmlContentParseOptions(),
              proxyModeEnable: false,
              rootETagName: 'node',
              autoBindRoot: true,
            },
          },
        },
      ],
    },
  ],
};

module.exports = {
  d1t: [
    data_d1t_obj,
    data_d1t_opt,
    data_d1t_res,
  ],
  main: {},
};
