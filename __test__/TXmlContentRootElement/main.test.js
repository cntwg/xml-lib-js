// [v0.1.012-20230709]

const xmlDoc = require('#lib/xmldoc-lib.js');
//const TXmlAttributesMapper = xmlDoc.TXmlAttributesMapper;
const TXmlElementController = xmlDoc.TXmlElementController;
//const TXmlElementsListController = xmlDoc.TXmlElementsListController;
const TXmlContentRootElement = xmlDoc.TXmlContentRootElement;

const t_Dat = require('./data/index.data.js');

const __unwrap_ec = TXmlElementController.__unwrap;
const __unwrap_rc = TXmlContentRootElement.__unwrap;

// class: 'TXmlContentRootElement'
describe('class: TXmlContentRootElement', () => {
  describe('1. create a new instance', () => {
    const testData = t_Dat.d1t;
    describe.each(testData)('$msg', ({ param }) => {
      describe.each(param)('$msg', ({ param }) => {
        describe.each(param)('$msg', ({ msgEx: opsMsgEx, value, status }) => {
          const testFn = (...args) => { return new TXmlContentRootElement(...args); };
          let testInst = null; let obj = null; let options = null;
          beforeAll(() => {
            ({ obj, options } = value);
          });
          it(`perform ops: ${opsMsgEx}`, () => {
            if (status.ops.isERR) {
              expect(() => { testInst = testFn(obj, options); }).toThrow(Error);
            } else {
              expect(() => { testInst = testFn(obj, options); }).not.toThrow(Error);
            };
          });
          it('check instance status', () => {
            if (Array.isArray(status.ops.classNames)) {
              status.ops.classNames.forEach((name) => {
                expect(testInst instanceof xmlDoc[name]).toBe(true);
              });
            };
            expect(obj).toStrictEqual(status.obj);
            expect(options).toStrictEqual(status.options);
            expect.assertions(status.ops.assertQty+2);
          });
          it('check instance content', () => {
            expect(__unwrap_ec(testInst)).toStrictEqual(status.econ);
            expect(__unwrap_rc(testInst)).toStrictEqual(status.rcon);
          });
        });
      });
    });
  });
});
