// [v0.1.003-20230709]

const xmlDoc = require('#lib/xmldoc-lib.js');
const TXmlElementController = xmlDoc.TXmlElementController;
const TXmlElementsListController = xmlDoc.TXmlElementsListController;
const TXmlAttributesMapper = xmlDoc.TXmlAttributesMapper;
const TXmlContentRootElement = xmlDoc.TXmlContentRootElement;
const TXmlContentDeclaration = xmlDoc.TXmlContentDeclaration;
const TXmlContentContainer = xmlDoc.TXmlContentContainer;

//const empty_elem_skel = { __attr: {}, __text: '' };

//const ees_arr = [ empty_elem_skel, empty_elem_skel ];

const def_decl_str = '<?xml version="1.0" encoding="UTF-8"?>';
//const empty_doc = [
//  def_decl_str, '<root></root>',
//].join('\n');
const initEmptyDoc = (rootETagName) => {
  if (typeof rootETagName !== 'string') rootETagName = 'root';
  return [
    def_decl_str,
    [ '<', rootETagName, '></', rootETagName, '>' ].join(''),
  ].join('\n');
};

const __unwrap_ec = TXmlElementController.__unwrap;

// class: 'TXmlContentDeclaration'
describe('class: TXmlContentDeclaration', () => {
  describe('1. create a new instance', () => {
    const testFn = (...args) => { return new TXmlContentDeclaration(...args); };
    it('no args to a constructor', () => {
      expect(() => { testFn(); }).toThrow(TypeError);
    });
    it('push an object to a constructor', () => {
      let item = null;
      expect(() => { item = testFn({}); }).not.toThrow(TypeError);
      expect(item).not.toBeNull();
      expect(item instanceof TXmlContentDeclaration).toBe(true);
      expect(item.version).toBe('1.0');
      expect(item.encoding).toBe('UTF-8');
    });
  });

});
