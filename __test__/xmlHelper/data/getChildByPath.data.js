// [v0.1.001-20240630]

/* module:   `xmlHelper`
 * function: getChildByPath
 */

const { genTestCase } = require('#test-dir/test-hfunc.js');

const {
  TXmlElementController,
  TXmlElementsListController,
} = require('#lib/xmldoc-lib.js');

const __unwrap_ec = TXmlElementController.__unwrap;

const TEST_FUNC = (value) => { return value; };

const preloads = {
  TEST_ELEMENT: {
    get obj(){
      return {
        __attr: {},
        __text: '',
        item: {
          __attr: { id: '1' },
          __text: 'first',
        },
        category: {
          value: {
            __text: 'comedy',
          },
        },
        tools: [
          {
            __attr: { id: '2' },
            __text: 'second',
          }, {
            __attr: { id: '3' },
            __text: 'third',
          },
        ],
      };
    },
    get options(){
      return undefined;
    },
    status: {
      obj: {
        __attr: {},
        __text: '',
        item: {
          __attr: { id: '1' },
          __text: 'first',
        },
        category: {
          value: {
            __text: 'comedy',
          },
        },
        tools: [
          {
            __attr: { id: '2' },
            __text: 'second',
          }, {
            __attr: { id: '3' },
            __text: 'third',
          },
        ],
      },
      options: undefined,
      __unwrap: undefined,
    },
  },
};

const valueTestDescr = {
  msg: 'run tests aganst "value" param',
  rem: '(defaults is used)',
  param: [
    ...genTestCase({
      inGroup: 'some args are given',
      preloads: undefined,
      cases: [
        {
          msg: 'value is a string',
          rem: '',
          values: {
            obj: new TXmlElementController(preloads.TEST_ELEMENT.obj),
            chain: 'item',
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: 'TXmlElementController',
          __unwrap: TXmlElementController.__unwrap,
          value: preloads.TEST_ELEMENT.obj.item,
        },
        assertQty: 3,
        before: undefined,
        after: undefined,
      },
    }, { auto: true }),
    ...genTestCase({
      inGroup: 'some args are given',
      preloads: undefined,
      cases: [
        {
          msg: 'value is an array of a strings',
          rem: '',
          values: {
            obj: new TXmlElementController(preloads.TEST_ELEMENT.obj),
            chain: [ 'category', 'value' ],
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: 'TXmlElementController',
          __unwrap: TXmlElementController.__unwrap,
          value: preloads.TEST_ELEMENT.obj.category.value,
        },
        assertQty: 3,
        before: undefined,
        after: undefined,
      },
    }, { auto: true }),
  ],
};

module.exports = {
  descr: [
    valueTestDescr,
    //resultTestDescr,
  ],
  tests: [
    valueTestDescr,
    //resultTestDescr,
  ],
};
