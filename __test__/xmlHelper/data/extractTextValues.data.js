// [v0.1.001-20240702]

/* module:   `xmlHelper`
 * function: extractTextValues
 */

const { genTestCase } = require('#test-dir/test-hfunc.js');

const {
  TXmlElementController,
  TXmlElementsListController,
} = require('#lib/xmldoc-lib.js');

const __unwrap_ec = TXmlElementController.__unwrap;

const TEST_FUNC = (value) => { return value; };

const preloads = {
  TEST_ELEMENT_OB1: {
    get obj(){
      return {
        __attr: { id: '1' },
        __text: 'first',
      };
    },
    get options(){
      return undefined;
    },
    status: {
      obj: {
        __attr: { id: '1' },
        __text: 'first',
      },
      options: undefined,
      __unwrap: undefined,
    },
  },
  TEST_ELEMENT_AR1: {
    get obj(){
      return [
        {
          __attr: { id: '1' },
          __text: 'first',
        },
        {
          __attr: { id: '2' },
          __text: 'second',
        }, {
          __attr: { id: '3', lang: 'en' },
          __text: 'third',
        },
      ];
    },
    get options(){
      return undefined;
    },
    status: {
      obj: [
        {
          __attr: { id: '1' },
          __text: 'first',
        },
        {
          __attr: { id: '2' },
          __text: 'second',
        }, {
          __attr: { id: '3', lang: 'en' },
          __text: 'third',
        },
      ],
      options: undefined,
      __unwrap: undefined,
    },
  },
};

const valueTestDescr = {
  msg: 'run tests aganst "value" param',
  rem: '(defaults is used)',
  param: [
    ...genTestCase({
      inGroup: 'some args are given',
      preloads: undefined,
      cases: [
        {
          msg: 'value is a "TXmlElementController"',
          rem: '',
          values: {
            obj: new TXmlElementController(preloads.TEST_ELEMENT_OB1.obj),
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: [ 'first' ],
          __unwrap: undefined,
        },
        assertQty: 2,
        before: undefined,
        after: undefined,
      },
    }, { auto: true }),
    ...genTestCase({
      inGroup: 'some args are given',
      preloads: undefined,
      cases: [
        {
          msg: 'value is a "TXmlElementsListController"',
          rem: '',
          values: {
            obj: new TXmlElementsListController(preloads.TEST_ELEMENT_AR1.obj),
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: [ 'first', 'second', 'third' ],
          __unwrap: undefined,
        },
        assertQty: 2,
        before: undefined,
        after: undefined,
      },
    }, { auto: true }),
  ],
};

module.exports = {
  descr: [
    valueTestDescr,
    //resultTestDescr,
  ],
  tests: [
    valueTestDescr,
    //resultTestDescr,
  ],
};
