// [v0.1.003-20240702]

/* module:   `xmlHelper`
 * function: ---
 */

module.exports = {
  getChildByPath: require('./getChildByPath.data.js'),
  addChildByPath: require('./addChildByPath.data.js'),
  extractTextValues: require('./extractTextValues.data.js'),
  extractTextValuesLn: require('./extractTextValuesLn.data.js'),
};
