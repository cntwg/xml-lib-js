// [v0.1.003-20240702]

const { runTestFn } = require('#test-dir/test-hfunc.js');

const xmlDoc = require('#lib/xmldoc-lib.js');

const {
  TXmlElementController,
  TXmlElementsListController,
} = xmlDoc;

const t_Mod = require('#lib/xml-helper.js');

const t_Dat = require('./data/index.data.js');

describe.each([
  {
    msg: 'function: getChildByPath()',
    method: 'getChildByPath',
  }, {
    msg: 'function: addChildByPath()',
    method: 'addChildByPath',
  }, {
    msg: 'function: extractTextValues()',
    method: 'extractTextValues',
  }, {
    msg: 'function: extractTextValuesLn()',
    method: 'extractTextValuesLn',
  },
])('$msg', ({ method }) => {
  const testData = t_Dat[method];
  describe.each(testData.tests)('+ $msg $rem', ({ param }) => {
    describe.each(param)('+ $msg', ({ param }) => {
      it.each(param)('+ $msg $rem', ({ preloads, values, status }) => {
        const { ops, assertQty } = status;
        let result = undefined; let isERR = false;
        try {
          result = runTestFn({ testInst: t_Mod, method }, values);
          //console.log('CHECK: => NO_ERR');
        } catch (err) {
          //console.log('CHECK: IS_ERR => '+err);
          isERR = true;
          result = err;
        } finally {
          expect.assertions(assertQty);
          expect(isERR).toBe(ops.isERR);
          if (isERR) {
            const { errType, errCode } = ops;
            expect(result instanceof errType).toBe(true);
            expect(result.code).toStrictEqual(errCode);
          } else {
            const { className, value, __unwrap } = ops;
            switch (typeof className) {
              case 'string' : {
                expect(result instanceof xmlDoc[className]).toBe(true);
                if (className !== 'HTMLElement') {
                  if (typeof __unwrap !== 'function') break;
                  result = __unwrap(result);
                };
              }
              default : {
                expect(result).toStrictEqual(value);
              }
            };
          };
        };
      });
    });
  });
});
