// [v0.1.016-20240801]

// === module init block ===

const xObj = require('@ygracs/xobj-lib-js');
const xmldoc = require('./lib/xmldoc-lib.js');
const xmlHelper = require('./lib/xml-helper.js');

// === module extra block (helper functions) ===

// === module main block ===

// === module exports block ===

module.exports.xObj = xObj;
module.exports.xmlHelper = xmlHelper;
module.exports.xmlDoc = xmldoc;

module.exports.XML_DEF_PARSE_OPTIONS = xmldoc.XML_DEF_PARSE_OPTIONS;
module.exports.XML_DEF_ROOT_ETAG_NAME = xmldoc.XML_DEF_ROOT_ETAG_NAME;
module.exports.XML_LANG_ATTR_TNAME = xmldoc.XML_LANG_ATTR_TNAME;

module.exports.readAsTagName = xmldoc.readAsTagName;
module.exports.readAsAttrName = xmldoc.readAsAttrName;
module.exports.valueToElementID = xmldoc.valueToElementID;

module.exports.TXmlContentParseOptions = xmldoc.TXmlContentParseOptions;

module.exports.TXmlAttributesMapper = xmldoc.TXmlAttributesMapper;
module.exports.TXmlElementController = xmldoc.TXmlElementController;
module.exports.TXmlElementsListController = xmldoc.TXmlElementsListController;
module.exports.TXmlContentDeclaration = xmldoc.TXmlContentDeclaration;
module.exports.TXmlContentRootElement = xmldoc.TXmlContentRootElement;
module.exports.TXmlContentContainer = xmldoc.TXmlContentContainer;
