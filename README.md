|***rev.*:**|0.1.9|
|:---|---:|
|***date***:|2024-08-01|

## Introduction

This module provide a base functionality for handling an XML-documents.

## Description

This module contains the following components:

### 1. xObj-manipulator

xObj-manipulator is a set of functions (see docs for [`@ygracs/xobj-lib-js` module](https://gitlab.com/ygracs/xobj-lib-js)).

### 2. a set of XML-document constants and components

- constants:

	+ `XML_DEF_ROOT_ETAG_NAME`;
	+ `XML_DEF_PARSE_OPTIONS`;

- functions:

	+ `readAsTagName` (*experimental*);
	+ `readAsAttrName` (*experimental*);
	+ `valueToElementID` (*experimental*);

- classes:

	+ `TXmlAttributesMapper`;
	+ `TXmlElementController`;
	+ `TXmlElementsListController`;
	+ `TXmlContentParseOptions`;
	+ `TXmlContentDeclaration`;
	+ `TXmlContentRootElement`;
	+ `TXmlContentContainer`.

For more read the `xmldoc-lib.md` in the project `doc` directory.

### 3. an `xmlHelper` module extension

- functions:

	+ `getChildByPath`;
	+ `addChildByPath`;
	+ `extractTextValues`;
	+ `extractTextValuesLn`.

For more read the `xmldoc-lib.md` in the project `doc` directory.

## Use cases

### Installation

`npm install @cntwg/xml-lib-js`
