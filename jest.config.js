// [v0.1.004-20240630]

const cmdArgs = require('minimist')(process.argv.slice(2));

const testObj = new Map([
  [ 'xml:amap', 'TXmlAttributesMapper' ],
  [ 'xml:ec', 'TXmlElementController' ],
  [ 'xml:lc', 'TXmlElementsListController' ],
  [ 'xml:re', 'TXmlContentRootElement' ],
  [ 'xml:cdec', 'TXmlContentDeclaration' ],
  [ 'xml:cc', 'TXmlContentContainer' ],
  [ 'xml:helper', 'xmlHelper' ],
  [ 'xml:bsf', 'xmlBase' ],
]);

let rootDirExt = '';
let obj = cmdArgs.object;
let tmSym = cmdArgs.target;
if (
  typeof tmSym !== 'string'
  || ((tmSym = tmSym.trim()) === '')
) {
  tmSym = '*';
};
if (
  typeof obj === 'string'
  && ((obj = obj.trim()) !== '')
) {
  if (testObj.has(obj)) rootDirExt = `/${testObj.get(obj)}/`;
};

module.exports = {
  rootDir: `__test__${rootDirExt}`,
  testMatch: [`**/?(${tmSym}.)+(spec|test).[jt]s?(x)`],
  //verbose: false,
};
