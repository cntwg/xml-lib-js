#### *v0.0.31*

Pre-release version.

> - `xmldoc-lib.md` updated;
> - (`TXmlElementController`) fix issue in method: `__setChildRaw`;
> - (`TXmlElementController`) add special static method: `__getChildRaw`;
> - (`TXmlElementController`) add special static method: `__addChildRaw`;
> - (`TXmlElementController`) deprecate methods: `_getChildRaw`, `_addChildRaw`< `_setChildRaw`;
> - some fixes in `xmldoc-lib.js` module.

#### *v0.0.30*

Pre-release version.

> - `xmldoc-lib.md` updated;
> - updated dependency on `@ygracs/xobj-lib-js` module to v0.2.0;
> - (`TXmlAttributesMapper`) add method: `renameAttribute`;
> - (`TXmlElementController`) add method: `renameAttribute`;
> - (`TXmlElementController`) add method: `loadFromXMLString`;
> - (`TXmlElementController`) add method: `create` (static);
> - (`TXmlElementController`) change class constructor behavior (*see docs for details*).

#### *v0.0.29*

Pre-release version.

> - updated dependency on `@ygracs/xml-js6` module to v0.0.4-b.

#### *v0.0.28*

Pre-release version.

> - `xmldoc-lib.md` updated;
> - add functions: readAsTagName, `readAsAttrName`, valueToElementID;
> - some fixes in `xmldoc-lib.js` module.

#### *v0.0.27*

Pre-release version.

> - `xmldoc-lib.md` updated;
> - fix error in a `addChildByPath` function;
> - add functions (`xmlHelper` module): `extractTextValues`, `extractTextValuesLn`.

#### *v0.0.26*

Pre-release version.

> - `xmldoc-lib.md` updated;
> - updated dependency on `@ygracs/bsfoc-lib-js` module to v0.2.1;
> - updated dependency on `@ygracs/xobj-lib-js` module to v0.1.2;
> - improve error handling on file load/save ops for `TXmlContentContainer`;
> - fix behavior for a `setTextValue` method of a `TXmlElementController`;
> - add `xml-helper.js` module.

#### *v0.0.25*

Pre-release version.

> - `xmldoc-lib.md` updated;
> - updated dependency on `@ygracs/bsfoc-lib-js` module to v0.2.0;
> - updated dependency on `@ygracs/xobj-lib-js` module to v0.1.1.

#### *v0.0.24*

Pre-release version.

> - `xmldoc-lib.md` updated;
> - updated dependency on `@ygracs/bsfoc-lib-js` module to v0.1.4;
> - updated dependency on `@ygracs/xobj-lib-js` module to v0.1.0;
> - some fixes in `xmldoc-lib.js` module.

#### *v0.0.23*

Pre-release version.

> - updated dependency on `@ygracs/bsfoc-lib-js` module to v0.1.3;
> - updated dependency on `@ygracs/xobj-lib-js` module to v0.0.14rc4.

#### *v0.0.22*

Pre-release version.

> - some fixes in `xmldoc-lib.js` module.

#### *v0.0.21*

Pre-release version.

> - some fixes in `xmldoc-lib.js` module;
> - added method `hasChild()` from `TXmlElementController` class;
> - removed constants `DEF_XML_PARSE_OPTIONS` and `DEF_XML_ROOT_ETAG_NAME` from `xmldoc-lib.js` module exports;
> - updated dependency on `@ygracs/xobj-lib-js` module to v0.0.14rc3.

#### *v0.0.20*

Pre-release version.

> - `xmldoc-lib.md` updated;
> - changed behavior of `TXmlAttributesMapper` class constructor: if `obj` parameter is not an object, the `TypeError` will be thrown;
> - some fixes in `xmldoc-lib.js` module.

#### *v0.0.19*

Pre-release version.

> - updated dependency on `@ygracs/xobj-lib-js` module to v0.0.14rc1.

#### *v0.0.18*

Pre-release version.

> fixed error in `saveToFile` method of `TXmltvContentProvider` class.

#### *v0.0.17*

Pre-release version.

> fixed error in `TXmlContentRootElement` class constructor that caused failure on `TXmlContentContainer` class initiation with `options => autoBindRoot = "false"`;
> - some fixes in `xmldoc-lib.js` module.

#### *v0.0.16*

Pre-release version.

> - `xmldoc-lib.md` updated;
> - removed method `init()` from `TXmlContentRootElement` class;
> - some fixes in `xmldoc-lib.js` module.

#### *v0.0.15*

Pre-release version.

> - updated dependency on `@ygracs/bsfoc-lib-js` module to v0.1.2;
> - updated dependency on `@ygracs/xobj-lib-js` module to v0.0.13.

#### *v0.0.14*

Pre-release version.

> - updated dependency on `@ygracs/bsfoc-lib-js` module to v0.1.1;
> - updated dependency on `@ygracs/xobj-lib-js` module to v0.0.12.

#### *v0.0.13*

Pre-release version.

> - `xmldoc-lib.md` updated;
> - added method `hasAttribute()` to `TXmlAttributesMapper` class;
> - added method `hasAttribute()` to `TXmlElementController` class;
> - added method `delChild()` to `TXmlElementController` class;
> - some fixes in `xmldoc-lib.js` module.

#### *v0.0.12*

Pre-release version.

> - `xmldoc-lib.md` updated;
> - some fixes in `xmldoc-lib.js` module.

#### *v0.0.11*

Pre-release version.

> - `xmldoc-lib.md` updated;
> - some fixes in `xmldoc-lib.js` module;
> - added method `clear()` to `TXmlAttributesMapper` class;
> - added method `getTextValue()` to `TXmlElementController` class;
> - added method `setTextValue()` to `TXmlElementController` class;
> - added property `maxIndex` to `TXmlElementsListController` class;
> - change to `TXmlContentRootElement` constructor: `name` param was excluded.

#### *v0.0.10*

Pre-release version.

> - switch dependency from `@cntwg/bsfoc-lib-js` module to `@ygracs/bsfoc-lib-js` module;
> - updated dependency on `@ygracs/xml-js6` module to v0.0.3b;
> - added dependency on `@ygracs/xobj-lib-js` module;
> - deprecate `$module/lib/xObj-lib.js` module;
> - `xObj-lib.md` deprecated.

#### *v0.0.9*

Pre-release version.

> - updated dependency on `@cntwg/bsfoc-lib-js` module to v0.0.11;
> - some fixes in `xmldoc-lib.js` module;
> - some fixes in `xObj-lib.js` module.

#### *v0.0.8*

Pre-release version.

> - `xmldoc-lib.md` updated;
> - `xObj-lib.md` updated;
> - updated dependency on `@cntwg/bsfoc-lib-js` module to v0.0.8;
> - some fixes in `xmldoc-lib.js` module;
> - added special static method `__unwrap()` to `TXmlContentRootElement` class.

#### *v0.0.7*

Pre-release version.

> - `xmldoc-lib.md` updated;
> - `xObj-lib.md` updated;
> - added function `getXObjAttributes()` in `xObj-lib.js` module;
> - fixed some errors in `xmldoc-lib.js` module;
> - added method `addChild()` to `TXmlElementController` class;
> - added special method `_addChildRaw()` to `TXmlElementController` class;
> - add tests of 'TXmlElementController' class for `xmldoc-lib.js` module;
> - add tests of 'TXmlElementsListController' class for `xmldoc-lib.js` module;
> - add tests of 'TXmlContentContainer' class for `xmldoc-lib.js` module.

#### *v0.0.6*

Pre-release version.

> - `xObj-lib.md` updated;
> - updated dependency on `@cntwg/bsfoc-lib-js` module to v0.0.7;
> - updated dependency on `@ygracs/xml-js6` module to v0.0.3;
> - fixed some errors in `xObj-lib.js` module;
> - added function `writeXObjParamEx()` in `xObj-lib.js` module;
> - removed function `writeXObjParamAsStr()` from `xObj-lib.js` module;
> - added function `writeXObjAttrEx()` in `xObj-lib.js` module;
> - removed function `writeXObjAttrAsStr()` from `xObj-lib.js` module;
> - added function `addXObjElement()` in `xObj-lib.js` module;
> - added function `deleteXObjElement()` in `xObj-lib.js` module;
> - added function `deleteXObjElementEx()` in `xObj-lib.js` module;
> - added function `renameXObjElement()` in `xObj-lib.js` module;
> - add tests for `xObj-lib.js` module.

#### *v0.0.5*

Pre-release version.

> - `README.md` updated;
> - `xmldoc-lib.md` updated;
> - removed method `_unwrap()` from class `TXmlElementController`;
> - added method `saveToFileSync()` to class `TXmlContentContainer`;
> - added method `loadFromFileSync()` to class `TXmlContentContainer`.

#### *v0.0.4*

Pre-release version.

> - `xmldoc-lib.md` updated.

#### *v0.0.3*

Pre-release version.

> - `README.md` updated;
> - updated dependency on `@cntwg/bsfoc-lib-js` module to v0.0.6;
> - added dependency on `@ygracs/xml-js6` module;
> - include `$module/lib/xmldoc-lib.js` module;
> - license included.

#### *v0.0.1-v0.0.2*

Pre-release version.
