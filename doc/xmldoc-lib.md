>|***rev.*:**|0.1.31|
>|:---|---:|
>|date:|2024-09-29|

## Introduction

This paper describes a constants, functions and an object classes provided by `xmldoc-lib.js` module.

## Module constants

### **XML\_DEF\_PARSE_OPTIONS**

This constant object provided by the module contains a default settings of options for XML-parser module used within.

The settings listed in the table below:

|name|type|value|
|:---|---|:---|
|`compact`|`boolean`|`true`|
|`declarationKey`|`string`|`__decl`|
|`attributesKey`|`string`|`__attr`|
|`textKey`|`string`|`__text`|
|`commentKey`|`string`|`__note`|
|`cdataKey`|`string`|`__cdata`|
|`nameKey`|`string`|`__name`|
|`typeKey`|`string`|`__type`|
|`parentKey`|`string`|`parent`|
|`elementsKey`|`string`|`items`|
|`ignoreDeclaration`|`boolean`|`false`|
|`ignoreDocType`|`boolean`|`false`|
|`ignoreInstractions`|`boolean`|`false`|
|`ignoreText`|`boolean`|`false`|
|`ignoreComments`|`boolean`|`false`|
|`ignoreCData`|`boolean`|`false`|
|`fullTagEmptyElement`|`boolean`|`true`|
|`addParent`|`boolean`|`false`|
|`trim`|`boolean`|`true`|
|`spaces`|`number`|`2`|

### **XML\_DEF\_ROOT\_ETAG_NAME**

This constant defines a default value for naming the root element for XML-document. Its value is `root`.

### **XML\_LANG\_ATTR\_TNAME**

This constant defines a default value for naming the language attribute for XML-element. Its value is `lang`.

## Module functions

### Experimental functions

> Note: Purpose of those functions will be discussed and some may be deprecate and make obsolete or functionality may be altered. So use it with cautions in mind.

#### **readAsTagName(value)** => `string`

> since: \[`v0.0.28`]

This function tries to convert a given `value` to the value of type which is suitable for an XML-element's tag name. If failed an empty string is returned.

#### **readAsAttrName(value)** => `string`

> since: \[`v0.0.28`]

This function tries to convert a given `value` to the value of type which is suitable for an XML-element's attribute name. If failed an empty string is returned.

#### **valueToElementID(value)**

> since: \[`v0.0.28`]

This function tries to convert a given `value` to the value of a valid identifier which is suitable for an XML-element's ID-attribute. If failed an empty string is returned.

## Module classes

### **TXmlContentParseOptions**

This class implements an interface for handling XML-parse options.

#### class constructor

The class constructor creates a new instance of the class. It receives arguments listed below:

|name|type|default value|description|
|:---|---|---:|:---|
|`object`|---|---|a host object.|
|`options`|`object`|---|an options settings.|

#### class properties

The table below describes a properties of a `TXmlContentParseOptions` class:

|name|read only|description|
|:---|---|:---|
|settings|yes|presents a current options|
|xml2js|yes|returns an options for an XML-to-JS converter|
|js2xml|yes|returns an options for an JS-to-XML converter|

#### class methods (*static*)

##### **createNewOptionsSet(obj)**

This method transforms a given object to a set of accepted parser options.

### **TXmlAttributesMapper**

This class implements a proxy interface for handling attributes of an element.

#### class constructor

The class constructor creates a new instance of the class.

##### constructor parameters

The class constructor receives an arguments listed below:

|parameter name|value type|default value|description|
|:---|---|---:|:---|
|`object`|---|---|a host object that holds an attributes|
|`options`|`object`|---|an options settings|

###### `options` parameter

The `options` structure is listed below:

|option name|value type|default value|description|
|:---|---|---:|:---|
|`parseOptions`|`object`|`EMPTY_OBJECT`|contains a XML-parse options (*see description for `XML_DEF_PARSE_OPTIONS`*)|

##### exceptions

If `object` parameter is not a type of `object` or not given a `TypeError` thrown.

#### class properties

|property name|value type|read only|description|
|:---|---|---|:---|
|`entries`|`array`|yes|returns an array of entries for all existing attributes|


#### class methods

##### **hasAttribute(name)** => `boolean`

This method checks whether or not an attribute addressed by a `name` exists. If attribute exists, 'true' is returned.

##### **getAttribute(name)** => `string`

This method returns an attribute value addressed by a `name`. If attribute is not exists the empty string is returned.

##### **setAttribute(name, value)** => `boolean`

This method sets an attribute value addressed by a `name` and return `true` if succeed.

##### **delAttribute(name)** => `boolean`

This method clears an attribute value addressed by a `name`.

##### **renameAttribute(name, value)** => `boolean`

> since: \[`v0.0.30`]

This method renames an attribute addressed by a `name` to its new name given by a `value` parameter.

##### **clear()** => `void`

This method clears all attributes.

### **TXmlElementController**

This class implements a proxy interface for an element.

#### class constructor

The class constructor creates a new instance of the class.

##### constructor parameters

The class constructor receives an arguments listed below:

|parameter name|value type|default value|description|
|:---|---|---:|:---|
|`object`|`object`|---|a host object|
|`options`|`object`|---|an options settings|

###### `options` parameter

The `options` structure is listed below:

|option name|value type|default value|description|
|:---|---|---:|:---|
|`proxyModeEnable`|`boolean`|`false`|<*reserved*>|
|`parseOptions`|`object`|`EMPTY_OBJECT`|contains a XML-parse options (see description for `XML_DEF_PARSE_OPTIONS`)|

##### exceptions

If `object` parameter is not a type of `object` or not given a `TypeError` thrown.

#### class properties

|property name|value type|read only|description|
|:---|---|---|:---|
|`attributes`|`TXmlAttributesMapper`|yes|returns an attributes of the element|
|`name`||yes|\<*reserved*>|
|`textValue`|`string`|no|represents a value of the element|

#### class methods

##### **hasAttribute(name)** => `boolean`

This method checks whether or not an attribute addressed by a `name` exists. If attribute exists, 'true' is returned.

##### **getAttribute(name)** => `string`

This method returns an attribute value addressed by a `name`. If attribute is not exists the empty string is returned.

##### **setAttribute(name, value)** => `boolean`

This method sets an attribute value addressed by a `name` and return `true` if succeed.

##### **delAttribute(name)** => `boolean`

This method clears an attribute value addressed by a `name` parameter.

##### **renameAttribute(name, value)** => `boolean`

> since: \[`v0.0.30`]

This method renames an attribute addressed by a `name` to its new name given by a `value` parameter.

##### **getTextValue()** => `array`

This method returns an array witch contain a value of a `textValue` property and a value of a `lang` attribute in format: `[ <lang>, <textValue> ]`. If element has no `lang` attribute, it replaced by an empty string in the result.

##### **setTextValue(value)** => `boolean`

This method sets the value of a `textValue` property and its `lang` attribute and if succeed returns `true`.

The method received value that can be a string or an array. If value is an array it must be in the following format:

`[ <textValue> ]`
or
`[ <lang>, <textValue> ]`.

##### **hasChild(name)** => `boolean`

This method checks whether or not a child element addressed by `name` is exists.

##### **getChild(name)** => `?(TXmlElementController|TXmlElementsListController)`

This method returns a child element addressed by `name`. If failed `null` is returned.

##### **addChild(name)** => `?TXmlElementController`

This method adds a new element as a child element addressed by a `name` parameter.

##### **delChild(name)** => `boolean`

This method deletes a child element addressed by a `name`.

> Warning: A method not fully implemented yet. Not use it for list like elements. If list contains a more than one element, a method will fail.

##### **clear()** => `void`

This method clears an element content.

#### class methods (*experimental*)

> Note: Purpose of those methods will be discussed and some may be deprecate and make obsolete or functionality may be altered. So use it with cautions in mind.

##### **loadFromXMLString(str)** => `boolean`

This method loads an element content from a string given by a `str` parameter.

###### *exceptions*

This methods trows `Error` if something goes wrong through a parsing process of a string.

#### class methods (*special*)

##### **_getChildRaw(name)** => `?any`

> WARNING: **\[since `v0.0.31`]** this method deprecated (*use static `__getChildRaw` method of the class*).

##### **_addChildRaw(name)** => `?object`

> WARNING: **\[since `v0.0.31`]** this method deprecated (*use static `__addChildRaw` method of the class*).

##### **_setChildRaw(name, obj)** => `boolean`

> WARNING: **\[since `v0.0.31`]** this method deprecated (*use static `__setChildRaw` method of the class*).

#### class methods (*static*)

##### **clear(item)** => `void`

This method clears an `item` content.

##### **create(obj, opt)** => `?(TXmlElementController|TXmlElementsListController)`

> since: \[`v0.0.30`]

This method creates an instance from a given object.

#### class methods (*static, special*)

##### **__unwrap(node)** => `?object`

This method returns a host for an element as an object. If failed `null` is returned.

##### **__getChildRaw(node, name)** => `?any`

> since: \[`v0.0.31`]

This method returns a child element addressed by `name` as object. If failed `null` is returned.

##### **__addChildRaw(node, name)** => `?object`

> since: \[`v0.0.31`]

This method adds an element addressed by `name` as a child element.

##### **__setChildRaw(node, name, obj)** => `boolean`

This method inserts a given object as a child element addressed by `name`.

### **TXmlElementsListController**

This class implements a proxy interface for a list of elements.

#### class constructor

The class constructor creates a new instance of the class.

##### constructor parameters

The class constructor receives an arguments listed below:

|parameter name|value type|default value|description|
|:---|---|---:|:---|
|`object`|`array`|---|a host object of an array type.|
|`options`|`object`|---|an options settings.|

###### `options` parameter

The `options` structure is listed below:

|option name|value type|default value|description|
|:---|---|---:|:---|
|`proxyModeEnable`|`boolean`|`false`|<*reserved*>|
|`isNullItemsAllowed`|`boolean`|`false`|<*reserved*>|
|`parseOptions`|`object`|`EMPTY_OBJECT`|contains a XML-parse options (see description for `XML_DEF_PARSE_OPTIONS`)|

##### exceptions

If `object` parameter is not a type of `array` or not given a `TypeError` may be thrown in case if a `proxyModeEnable` options is set.

#### class properties

|property name|property type|read only|description|
|:---|---|---|:---|
|`count`|`number`|yes|returns a quantity of the elements|
|`size`|`number`|yes|returns a quantity of the elements|
|`maxIndex`|`index`|yes|returns a last possible index of the elements|
|`isNullItemsAllowed`|`boolean`|yes|indicates whether a list may be filled  with the null-elements or not|

#### class methods

##### **clear()** => `void`

This method clears a list.

##### **isEmpty()** => `boolean`

This method returns `true` if a list has no elements and `false` otherwise.

##### **isNotEmpty()** => `boolean`

This method returns `true` if a list contains at least one element and `false` otherwise.

##### **chkIndex(index)** => `boolean`

This method checks if a given index fits a range of elements within the list. If so `true` is returned.

##### **isValidItem(obj)** => `boolean`

This method checks whether or not a given element is valid.

##### **getItem(index)** => `?TXmlElementController`

This method returns an element addressed by `index`. If failed `null` is returned.

##### **addItem(obj)** => `index`

This method adds an element to a list members. If succeed an index of the element is returned.

##### **setItem(index, obj)** => `boolean`

This method replaces an element addressed by `index` with a new element. If succeed `true` is returned.

##### **insItem(index, obj)** => `boolean`

This method inserts an element in the position addressed by `index`. If succeed `true` is returned.

##### **delItem(index)** => `boolean`

This method deletes an element addressed by `index`. If succeed `true` is returned.

##### **loadItems(items, options)** => `number`

This method loaded a list of elements listed by `items` and returns a quantity of a successfully added elements.

The `options` structure is listed below:

|option name|value type|default value|description|
|:---|---|---:|:---|
|`useClear`|`boolean`|`true`|if `true` the method deletes all the previous content before load the list|

#### class methods (*special*)

##### **_getItemRaw(name)**

This method returns an element addressed by `index` as object. If failed `null` is returned.

##### **_addItemRaw(obj)**

This method adds an element to a list members as object. If succeed an index of the element is returned.

##### **_setItemRaw(index, obj)**

This method replaces an element addressed by `index` with a new element which is saved as raw object. If succeed `true` is returned.

##### **_insItemRaw(index, obj)**

This method inserts an element as a raw object in the position addressed by `index`. If succeed `true` is returned.

### **TXmlContentDeclaration**

This class implements a proxy interface for a document declaration element.

#### class constructor

The class constructor creates a new instance of the class.

##### constructor parameters

The class constructor receives an arguments listed below:

|parameter name|value type|default value|description|
|:---|---|---:|:---|
|`object`|`object`|---|a host object for the document content.|
|`options`|`object`|---|an options settings.|

###### `options` parameter

The `options` structure is listed below:

|option name|value type|default value|description|
|:---|---|---:|:---|
|`parseOptions`|`object`|`EMPTY_OBJECT`|contains a XML-parse options (see description for `XML_DEF_PARSE_OPTIONS`)|

##### exceptions

If `object` parameter is not a type of `object` or not given a `TypeError` thrown.

#### class properties

|property name|property type|read only|description|
|:---|---|---|:---|
|`version`|`string`|no|returns a version of a XML|
|`encoding`|`string`|no|returns an encodings used by a document|

#### class methods (*special*)

##### **init()** => `boolean`

This method initializes an element with a default values.

### **TXmlContentRootElement**

This class extends a functionality of a `TXmlElementController` class.

#### class constructor

The class constructor creates a new instance of the class.

##### constructor parameters

The class constructor receives an arguments listed below:

|parameter name|value type|default value|description|
|:---|---|---:|:---|
|`object`|`object`|---|a host object for the document content|
|`options`|`object`|---|an options settings|

###### `options` parameter

The `options` structure is listed below:

|option name|value type|default value|description|
|:---|---|---:|:---|
|`rootETagName`|`string`|`root`|contains a the name of a tag that represents a root element|
|`autoBindRoot`|`boolean`|`true`|<*reserved*>|
|`proxyModeEnable`|`boolean`|`false`|<*reserved*>|
|`isNullItemsAllowed`|`boolean`|`false`|<*reserved*>|
|`parseOptions`|`object`|`EMPTY_OBJECT`|contains a XML-parse options (see description for `XML_DEF_PARSE_OPTIONS`)|

#### class properties

|property name|value type|read only|description|
|:---|---|---|:---|
|tagName|`string`|no||

#### class methods (*static*)

##### **__unwrap(item\[, options])**

This method returns a host for an element as an object. If failed `null` is returned.

The `options` parameter, if given, change a behavior for the method. If set to 'true' the given instance checks against inheritance from a `TXmlContentRootElement`, if else, from a `TXmlElementController` class.

#### class methods (*special*)

### **TXmlContentContainer**

This class implements a proxy interface for manipulate a content of a document.

#### class constructor

The class constructor creates a new instance of the class.

##### constructor parameters

The class constructor receives an `options` parameter as its argument.

###### `options` parameter

The `options` structure is listed below:

|option name|value type|default value|description|
|:---|---|---:|:---|
|`rootETagName`|`string`|`root`|contains a the name of a tag that represents a root element|
|`autoBindRoot`|`boolean`|`true`|<*reserved*>|
|`proxyModeEnable`|`boolean`|`false`|<*reserved*>|
|`isNullItemsAllowed`|`boolean`|`false`|<*reserved*>|
|`parseOptions`|`object`|`EMPTY_OBJECT`|contains a XML-parse options (see description for `XML_DEF_PARSE_OPTIONS`)|

#### class properties

|property name|value type|read only|description|
|:---|---|---|:---|
|rootElement||yes|returns a document root element|

#### class methods

##### **clear()** => `void`

This method clears a document content.

##### **saveToXMLString()** => `string`

This method returns a document content as a string.

##### **saveToFileSync(src)**

This method saves a document content into a file addressed by `src`.

##### **loadFromXMLString(str)** => `boolean`

This method loads a document content from a string given by `str`.

##### **loadFromFileSync(src)**

This method loads a document content from a file addressed by `src`.

#### class methods (*async*)

##### **saveToFile(src)**

This method saves a document content into a file addressed by `src`.

##### **loadFromFile(src)**

This method loads a document content from a file addressed by `src`.

#### class methods (*special*)

##### **asJSON()**

This method returns a document content as a string in the JSON-format.

##### **_bindContent(obj\[, options])**

This method binds a given object as a document content.

For `options` parameter details see the class constructor description.

## Module `xmlHelper`

This section contains functions provided by `xmlHelper` module.

### Experimental functions

> Note: Purpose of those functions will be discussed and some may be deprecate and make obsolete or functionality may be altered. So use it with cautions in mind.

#### **getChildByPath(object, chain)**

> since: \[v0.0.26]

This function tries to get a child element from a given object following path provided by `chain` parameter. If failed `null` is returned.

The `chain` parameter contains an array of an element names.

#### **addChildByPath(object, chain)**

> since: \[v0.0.26]

This function tries to add a new child element into a given object following path provided by `chain` parameter. If succeed a new element returned. If failed `null` is returned.

The `chain` parameter contains an array of an element names. The last name in `chain` is a name of the element which will be added.

#### **extractTextValues(object)**

> since: \[v0.0.27]

This function tries to get a text value from a given element.

#### **extractTextValuesLn(object)**

> since: \[v0.0.27]

This function tries to get a text value with its language attribute from a given element.
