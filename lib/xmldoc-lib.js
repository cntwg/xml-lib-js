// [v0.2.096-20240929]

// === module init block ===

const fs = require('fs');
const fse = fs.promises;
const xmlParser = require('@ygracs/xml-js6');

const {
  valueToIndex, readAsBool, readAsString, isNotEmptyString,
  isArray, isObject, isPlainObject,
  valueToArray, valueToEntry,
  valueToIDString,
} = require('@ygracs/bsfoc-lib-js');

const xObj = require('@ygracs/xobj-lib-js');
const {
  TXmlContentParseOptions,
  DEF_XML_PARSE_OPTIONS: XML_DEF_PARSE_OPTIONS,
} = xObj;

// === module extra block (helper functions) ===

/**
 * @function checkFsError
 * @param {object} descr
 * @param {Error} err
 * @returns {object}
 * @inner
 * @description Checs an error code of a fs ops and sets descr info if succeed
 */
function checkFsError(descr, err) {
  let isSucceed = false;
  if (isPlainObject(err) && isPlainObject(descr)) {
    switch (err.code) {
      case 'ENOENT':
      case 'EISDIR':
      case 'EPERM': {
        descr.errCode = err.errno;
        descr.errEvent = err.code;
        descr.errMsg = `ERR_FILE_${err.code}`;
        isSucceed = true;
        break;
      }
      default: {}
    };
    if (isSucceed) descr.isERR = true;
  };
  return { isSucceed, descr };
};

// === module main block ===

/***
 * (* constant definitions *)
 */

const XML_DEF_ROOT_ETAG_NAME = 'root';
const XML_LANG_ATTR_TNAME = 'lang';

const XML_TE_INVARG_EMSG = 'invalid argument';
const XML_TE_NOBJ_EMSG = `${XML_TE_INVARG_EMSG} (an object is expected)`;
const XML_TE_NARR_EMSG = `${XML_TE_INVARG_EMSG} (an array is expected)`;
const XML_TE_NROOT_EMSG = 'root element not found';

/***
 * (* function definitions *)
 */

/**
 * @function readAsTagName
 * @param {any} value
 * @returns {string}
 * @since v0.0.28
 * @description Tries to convert a given value to a valid tag name
 *              of an XML-element.
 */
function readAsTagName(value) {
  let tagName = valueToIDString(value, { ignoreNumbers: true });
  if (tagName === null) return '';
  if (tagName !== '') {
    // // TODO: do extra checks
    const template = /[\s\/\\\"\'<>=]/;
    const trigger = tagName.match(template);
    if (trigger) {
      //console.log(trigger);
      tagName = '';
    };
  };
  return tagName;
};

/**
 * @function readAsAttrName
 * @param {any} value
 * @returns {string}
 * @since v0.0.28
 * @description Tries to convert a given value to a valid name
 *              of an XML-attribute.
 */
function readAsAttrName(value) {
  let attrName = valueToIDString(value, { ignoreNumbers: true });
  if (attrName === null) return '';
  if (attrName !== '') {
    // // TODO: do extra checks
    const template = /[\s\/\\\"\'<>=]/;
    const trigger = attrName.match(template);
    if (trigger) {
      //console.log(trigger);
      attrName = '';
    };
  };
  return attrName;
};

/**
 * @function valueToElementID
 * @param {any} value
 * @returns {string}
 * @since v0.0.28
 * @description Tries to convert a given value to a valid identifier
 *              suitable as a value for an "ID-attribute" of an XML-element.
 */
function valueToElementID(value) {
  let id = valueToIDString(value);
  if (id === null) return '';
  if (id !== '') {
    // // TODO: do extra checks
    const template = /[\s\/\\\"\'<>=]/;
    const trigger = id.match(template);
    if (trigger) {
      //console.log(trigger);
      id = '';
    };
  };
  return id;
};

/***
 * (* class definitions *)
 */

/**
 * @description This class implements an attributes mapper instance
 */
class TXmlAttributesMapper {
  /** @property {?object} */
  #_host;// = null;
  /** @property {?object} */
  #_options;// = null;

  /**
   * @param {object} obj
   * @param {object} [opt]
   * @param {object} [opt.parseOptions]
   * @throws {TypeError}
   */
  constructor(obj, opt) {
    // init an elements content
    if (!isPlainObject(obj)) throw new TypeError(XML_TE_NOBJ_EMSG);
    this.#_host = obj;
    // load options
    const _options = isPlainObject(opt) ? opt : {};
    let { parseOptions } = _options;
    if (!isPlainObject(parseOptions)) {
      _options.parseOptions = parseOptions = {};
    };
    let { settings } = parseOptions;
    if (!isPlainObject(settings)) parseOptions.settings = settings = {};
    let { attributesKey } = settings;
    if (
      typeof attributesKey !== 'string'
      || ((attributesKey = attributesKey.trim()) === '')
    ) {
      attributesKey = XML_DEF_PARSE_OPTIONS.attributesKey;
    };
    // save options
    settings.attributesKey = attributesKey;
    this.#_options = parseOptions;
  }

  /**
   * @property {Array}
   * @readonly
   */
  get entries() {
    let items = xObj.getXObjAttributes(
      this.#_host,
      this.#_options.settings.attributesKey,
    );
    return isPlainObject(items) ? Object.entries(items) : [];
  }

  /**
   * @param {string} name
   * @returns {boolean}
   * @description Checks whether an attribute exists.
   */
  hasAttribute(name) {
    let result = false;
    try {
      result = xObj.checkXObjAttribute(
        this.#_host,
        name,
        this.#_options.settings.attributesKey,
      );
    } catch (err) {
      // // TODO: verify what error is thrown
      result = false;
    };
    return result;
  }

  /**
   * @param {string} name
   * @returns {string}
   * @description Returns an attribute value.
   */
  getAttribute(name) {
    let result = '';
    try {
      result = xObj.readXObjAttr(
        this.#_host,
        name,
        this.#_options.settings.attributesKey,
      );
    } catch (err) {
      // // TODO: verify what error is thrown
      result = '';
    };
    return result;
  }

  /**
   * @param {string} name
   * @param {any} value
   * @returns {boolean}
   * @description Sets an attribute value.
   */
  setAttribute(name, value) {
    const attrName = readAsAttrName(name);
    let result = false;
    if (attrName !== '') {
      try {
        result = xObj.writeXObjAttr(
          this.#_host,
          name,
          value,
          this.#_options.settings.attributesKey,
        );
      } catch (err) {
        // // TODO: verify what error is thrown
        result = false;
      };
    };
    return result;
  }

  /**
   * @param {string} name
   * @returns {boolean}
   * @description Deletes an attribute.
   */
  delAttribute(name) {
    let result = false;
    try {
      result = xObj.deleteXObjAttribute(
        this.#_host,
        name,
        this.#_options.settings.attributesKey,
      );
    } catch (err) {
      // // TODO: verify what error is thrown
      result = false;
    };
    return result;
  }

  /**
   * @param {string} name
   * @param {string} value
   * @returns {boolean}
   * @since v0.0.30
   * @description Renames an attribute.
   */
  renameAttribute(name, value) {
    const newName = readAsAttrName(value);
    let result = false;
    if (newName !== '') {
      try {
        result = xObj.renameXObjAttribute(
          this.#_host,
          name,
          newName,
          this.#_options.settings.attributesKey,
        );
      } catch (err) {
        // // TODO: verify what error is thrown
        result = false;
      };
    };
    return result;
  }

  /**
   * @returns {void}
   * @description Deletes all attributes.
   */
  clear() {
    xObj.insertXObjElement(
      this.#_host,
      this.#_options.settings.attributesKey,
      {
        force: true,
        ripOldies: true,
      },
    );
  }

}

/**
 * @description This class implements an instance of an element controller
 */
class TXmlElementController {
  /** @property {?object} */
  #_host;// = null;
  /** @property {?object} */
  #_options;// = null;
  /** @property {?object} */
  #_parseOptions;// = null;
  /** @property {?TXmlAttributesMapper} */
  #_attributes;// = null;
  #_name = null;

  /**
   * @param {object} obj
   * @param {object} [opt]
   * @param {object} [opt.parseOptions]
   * @param {boolean} [opt.proxyModeEnable=false]
   * @throws {TypeError}
   */
  constructor(obj, opt) {
    // load options
    let _options = isPlainObject(opt) ? opt : {};
    let {
      proxyModeEnable,
      parseOptions,
    } = _options;
    if (typeof proxyModeEnable !== 'boolean') proxyModeEnable = false;
    // init an element content
    if (isPlainObject(obj)) {
      // // TODO: split a plain object and class instances
      this.#_host = obj;
    } else {
      if (proxyModeEnable) {
        throw new TypeError(
          `TXmlElementController in "proxyMode": ${XML_TE_NOBJ_EMSG}`
        );
      };
      this.#_host = {};
    };
    // set parser options
    if (!(parseOptions instanceof TXmlContentParseOptions)) {
      parseOptions = new TXmlContentParseOptions(parseOptions);
      _options.parseOptions = parseOptions;
    };
    //if (!isPlainObject(parseOptions)) {
    //  _options.parseOptions = parseOptions = {};
    //};
    //let { settings } = parseOptions;
    //if (!isPlainObject(settings)) parseOptions.settings = settings = {};
    //let { textKey } = settings;
    //if (
    //  typeof textKey !== 'string'
    //  || ((textKey = textKey.trim()) === '')
    //) {
    //  textKey = XML_DEF_PARSE_OPTIONS.textKey;
    //};
    // save options
    //settings.textKey = textKey;
    this.#_parseOptions = parseOptions;
    _options.proxyModeEnable = proxyModeEnable;
    this.#_options = _options;
    // bind atributes mapper
    this.#_attributes = new TXmlAttributesMapper(obj, _options);
  }

  /**
   * @property {TXmlAttributesMapper}
   * @readonly
   */
  get attributes() {
    return this.#_attributes;
  }

  /**
   * @property {string}
   * @readonly
   * @experimental
   */
  get name() {
    let result = '';
    if (this.#_parseOptions.settings.compact) {
      result = xObj.readXObjParam(
        this.#_host,
        this.#_parseOptions.settings.nameKey
      );
    } else {
      // TODO: read name of element in non-compact mode
    };
    return result;
  }

  /**
   * @property {string}
   */
  get textValue() {
    return xObj.readXObjParam(this.#_host, this.#_parseOptions.settings.textKey);
  }

  set textValue(value) {
    xObj.writeXObjParam(this.#_host, value, this.#_parseOptions.settings.textKey);
  }

  /**
   * @param {string} name
   * @returns {boolean}
   * @description Checks whether an attribute exists.
   */
  hasAttribute(name) {
    return this.#_attributes.hasAttribute(name);
  }

  /**
   * @param {string} name
   * @returns {string}
   * @description Returns an attribute value.
   */
  getAttribute(name) {
    return this.#_attributes.getAttribute(name);
  }

  /**
   * @param {string} name
   * @param {any} value
   * @returns {boolean}
   * @description Sets an attribute value.
   * @see TXmlAttributesMapper.setAttribute
   */
  setAttribute(...args) {
    return this.#_attributes.setAttribute(...args);
  }

  /**
   * @param {string} name
   * @returns {boolean}
   * @description Deletes an attribute.
   */
  delAttribute(name) {
    return this.#_attributes.delAttribute(name);
  }

  /**
   * @param {string} name
   * @param {string} value
   * @returns {boolean}
   * @since v0.0.30
   * @description Renames an attribute.
   * @see TXmlAttributesMapper.renameAttribute
   */
  renameAttribute(...args) {
    return this.#_attributes.renameAttribute(...args);
  }

  /**
   * @returns {string[]}
   * @description Returns a text value in format of a `[ <lang>, <text> ]` pair.
   */
  getTextValue() {
    return [
      this.#_attributes.getAttribute(XML_LANG_ATTR_TNAME),
      this.textValue,
    ];
  }

  /**
   * @param {(string|string[])} value
   * @returns {boolean}
   * @description Sets a text value.
   */
  setTextValue(value) {
    const _value = valueToEntry(value);
    if (_value !== null) {
      const [ lang, param ] = _value;
      if (xObj.writeXObjParam(
        this.#_host,
        param,
        this.#_parseOptions.settings.textKey,
      )) {
        const attributes = this.#_attributes;
        if (lang === '') {
          if (attributes.hasAttribute(XML_LANG_ATTR_TNAME)) {
            return attributes.delAttribute(XML_LANG_ATTR_TNAME);
          } else {
            return true;
          };
        } else {
          return attributes.setAttribute(XML_LANG_ATTR_TNAME, lang);
        };
      };
    };
    return false;
  }

  /**
   * @param {string} name
   * @returns {boolean}
   * @description Checks whether an element has a child element.
   */
  hasChild(name) {
    const _name = xObj.evalXObjEName(name);
    if (_name === null) return false;
    let result = false;
    try {
      result = isObject(xObj.getXObjElement(this.#_host, _name));
    } catch (err) {
      // // TODO: [?] verify what error is thrown
      //throw err;
      result = false;
    };
    return result;
  }

  /**
   * @param {string} name
   * @returns {(null|TXmlElementController|TXmlElementsListController)}
   * @description Returns a child element.
   */
  getChild(name) {
    const _name = xObj.evalXObjEName(name);
    let result = null;
    if (_name !== null) {
      try {
        const item = xObj.getXObjElement(this.#_host, _name);
        result = TXmlElementController.create(item, this.#_options);
      } catch (err) {
        // // TODO: [?] verify what error is thrown
        result = null;
      };
    };
    return result;
  }

  /**
   * @param {string} name
   * @returns {?any}
   * @protected
   * @deprecated
   * @description Returns a child element.
   * @see TXmlElementController.__getChildRaw
   */
  _getChildRaw(name) {
    //console.log('CHECK: TXmlElementController._getChildRaw() => was called...');
    return TXmlElementController.__getChildRaw(this, name);
  }

  /**
   * @param {string} name
   * @param {object} obj
   * @returns {boolean}
   * @protected
   * @deprecated
   * @description Sets a child element.
   * @see TXmlElementController.__setChildRaw
   */
  _setChildRaw(...args) {
    //console.log('CHECK: TXmlElementController._setChildRaw() => was called...');
    return TXmlElementController.__setChildRaw(this, ...args);
  }

  /**
   * @param {string} name
   * @returns {?Object}
   * @protected
   * @deprecated
   * @description Adds a new child element.
   * @see TXmlElementController.__addChildRaw
   */
  _addChildRaw(name) {
    //console.log('CHECK: TXmlElementController._addChild() => was called...');
    return TXmlElementController.__addChildRaw(this, name);
  }

  /**
   * @param {string} name
   * @returns {?TXmlElementController}
   * @description Adds a new child element.
   */
  addChild(name) {
    const _name = xObj.evalXObjEName(name);
    if (_name === null) return null;
    let result = null;
    try {
      const { item } = xObj.addXObjElement(this.#_host, _name);
      if (item) result = new TXmlElementController(item, this.#_options)
    } catch (err) {
      // // TODO: [?] verify what error is thrown
      //throw err;
      result = null;
    };
    return result;
  }

  /**
   * @param {string} name
   * @returns {boolean}
   * @description Deletes a child element.
   */
  delChild(name) {
    const _name = xObj.evalXObjEName(name);
    if (_name === null) return false;
    let isSucceed = false;
    try {
      const item = xObj.getXObjElement(this.#_host, _name);
      let isACCEPTED = false;
      if (isArray(item)) {
        //console.log('CHECK: TXmlElementController.delChild() => item.typeof() => [object Array]');
        if (item.length < 2) {
          isACCEPTED = true;
        } else {
          //console.log('CHECK: TXmlElementController.delChild() => item.length() => ['+item.length+']');
        };
      } else {
        isACCEPTED = item !== null;
        //console.log('CHECK: TXmlElementController.delChild() => item.typeof() => ['+typeof item+']');
        //console.log('CHECK: TXmlElementController.delChild() => item.value() => ['+item+']');
      };
      if (isACCEPTED) isSucceed = xObj.deleteXObjElement(this.#_host, _name);
    } catch (err) {
      // // TODO: [?] verify what error is thrown
      //throw err;
      isSucceed = false;
    };
    return isSucceed;
  }

  /**
   * @returns {void}
   * @description Deletes all child elements and attributes.
   */
  clear() {
    TXmlElementController.clear(this);
  }

  /**
   * @param {string} loadFromXMLString
   * @returns {boolean}
   * @throws {Error}
   * @experimental
   * @since v0.0.30
   * @description Loads a element content from a string.
   */
  loadFromXMLString(xmlString) {
    let result = false;
    if (typeof xmlString === 'string' && xmlString !== '') {
      const _options = this.#_options;
      let {
        proxyModeEnable,
        parseOptions,
      } = _options;
      parseOptions = parseOptions.xml2js;
      parseOptions.ignoreDocType = true;
      parseOptions.ignoreDeclaration = true;
      try {
        let obj = xmlParser.xml2js(xmlString, parseOptions);
        if (isPlainObject(obj)) {
          let list = Object.entries(obj);
          let item = list[0];
          obj = item[1];
          if (isPlainObject(obj)) {
            if (proxyModeEnable) {
              let list = Object.entries(obj);
              const target = this.#_host;
              this.clear();
              list.forEach((entry) => {
                const [key, value] = entry;
                target[key] = value;
              });
            } else {
              this.#_host = obj;
            };
            this.#_attributes = new TXmlAttributesMapper(obj, _options);
            result = true;
          };
        };
      } catch (err) {
        //console.log('CHECK: TXmlContentContainer.loadFromXMLString() => Error => '+err);
        //console.log('CHECK: TXmlContentContainer.loadFromXMLString() => Error => '+err.code);
        throw err;
      };
    };
    return result;
  }

  /**
   * @param {object} item
   * @returns {?object}
   * @protected
   * @static
   * @description Tries to unwraps a given object.
   */
  static __unwrap(item) {
    return (item instanceof TXmlElementController) ? item.#_host : null;
  }

  /**
   * @param {TXmlElementController} node
   * @param {string} name
   * @returns {?any}
   * @protected
   * @static
   * @since v0.0.31
   * @description Returns a child element.
   */
  static __getChildRaw(node, name) {
    let item = null;
    if (node instanceof TXmlElementController) {
      try {
        const obj = node.#_host;
        item = xObj.getXObjElement(obj, name);
        if (item === undefined) item = null;
      } catch (err) {
        // // TODO: [?] check what kind of error thrown
        item = null;
      };
    };
    return item;
  }

  /**
   * @param {TXmlElementController} node
   * @param {string} name
   * @returns {?Object}
   * @protected
   * @static
   * @since v0.0.31
   * @description Adds a new child element.
   */
  static __addChildRaw(node, name) {
    const childName = readAsTagName(name);
    let item = null;
    if (
      node instanceof TXmlElementController
      && childName !== ''
    ) {
      try {
        const obj = node.#_host;
        ({ item } = xObj.addXObjElement(obj, childName));
      } catch (err) {
        // // TODO: [?] check what kind of error thrown
        item = null;
      };
    };
    return item;
  }

  /**
   * @param {TXmlElementController} node
   * @param {string} name
   * @param {object} obj
   * @returns {boolean}
   * @protected
   * @static
   * @description Sets a child element.
   */
  static __setChildRaw(node, name, obj) {
    const childName = readAsTagName(name);
    let isSucceed = false;
    if (
      node instanceof TXmlElementController
      && childName !== ''
      && isObject(obj)
    ) {
      node.#_host[childName] = obj;
      isSucceed = true;
    };
    return isSucceed;
  }

  /**
   * @param {object} obj
   * @returns {void}
   * @static
   * @description Tries to clean a given object.
   */
  static clear(obj) {
    let item = TXmlElementController.__unwrap(obj);
    if (item) {
      for (let prop in item) {
        // // TODO: catch errors in strict mode
        delete item[prop];
      };
    };
  }

  /**
   * @param {object} obj
   * @param {object} [opt]
   * @returns {(null|TXmlElementController|TXmlElementsListController)}
   * @static
   * @since v0.0.30
   * @description Creates new controller element.
   */
  static create(obj, opt) {
    let result = null;
    try {
      if (isObject(obj)) {
        if (isArray(obj)) {
          result = new TXmlElementsListController(obj, opt);
        } else {
          // // TODO: [?] check what kind of object passed in
          result = new TXmlElementController(obj, opt);
        };
      };
    } catch (err) {
      result = null;
    };
    return result;
  }

};

/**
 * @description This class implements an instance of a list controller
 */
class TXmlElementsListController {
  /** @property {?Array} */
  #_host;// = null;
  /** @property {?object} */
  #_options;// = null;
  #_count = null;

  /**
   * @param {Array} obj
   * @param {object} [opt]
   * @param {object} [opt.parseOptions]
   * @param {boolean} [opt.proxyModeEnable=false]
   * @param {boolean} [opt.isNullItemsAllowed=false] - <reserved>
   * @throws {TypeError}
   */
  constructor(obj, opt) {
    // load options
    let _options = isPlainObject(opt) ? opt : {};
    let {
      proxyModeEnable,
      isNullItemsAllowed,
      parseOptions,
    } = _options;
    if (typeof proxyModeEnable !== 'boolean') proxyModeEnable = false;
    if (typeof isNullItemsAllowed !== 'boolean') isNullItemsAllowed = false;
    // set parser options
    if (!isPlainObject(parseOptions)) {
      _options.parseOptions = parseOptions = {};
    };
    let { settings } = parseOptions;
    if (!isPlainObject(settings)) parseOptions.settings = settings = {};
    // init an elements list content
    if (isArray(obj)) {
      this.#_host = obj;
    } else {
      if (proxyModeEnable) {
        throw new TypeError(
          `TXmlElementsListController in "proxyMode": ${XML_TE_NARR_EMSG}`
        );
      } else {
        // // TODO: split a plain object and class instances
        this.#_host = [];
      };
    };
    // save options
    _options.proxyModeEnable = proxyModeEnable;
    _options.isNullItemsAllowed = isNullItemsAllowed;
    this.#_options = _options;
  }

  /**
   * @property {number}
   * @readonly
   */
  get count() {
    return this.#_host.length;
  }

  /**
   * @property {number}
   * @readonly
   */
  get size() {
    return this.#_host.length;
  }

  /**
   * @property {number}
   * @readonly
   */
  get maxIndex() {
    return this.#_host.length - 1;
  }

  /**
   * @property {boolean}
   * @readonly
   */
  get isNullItemsAllowed() {
    return readAsBool(this.#_options.isNullItemsAllowed);
  }

  /**
   * @returns {void}
   * @description Deletes all child elements and attributes.
   */
  clear() {
    this.#_host.length = 0;
  }

  /**
   * @returns {boolean}
   * @description Checks whether an instance holds none element.
   */
  isEmpty() {
    return this.count === 0;
  }

  /**
   * @returns {boolean}
   * @description Checks whether an instance holds at least one element.
   */
  isNotEmpty() {
    return this.count > 0;
  }

  /**
   * @param {any} value
   * @returns {boolean}
   * @description Checks whether a given value is a valid index value
   *              and it is not exceeds an index range within the instance.
   */
  chkIndex(value) {
    const index = valueToIndex(value);
    return index !== -1 && index < this.size;
  }

  /**
   * @param {any} obj
   * @returns {boolean}
   * @description Checks whether or not a given element is valid.
   */
  isValidItem(obj) {
    return (
      (this.#_options.isNullItemsAllowed && obj === null)
      || isPlainObject(obj)
    );
  }

  /**
   * @param {number} index
   * @returns {?object}
   * @protected
   * @description Returns an element.
   */
  _getItemRaw(index) {
    let item = this.#_host[valueToIndex(index)];
    return item !== undefined ? item : null;
  }

  /**
   * @param {number} index
   * @returns {?TXmlElementController}
   * @description Returns an element.
   */
  getItem(index) {
    let item = this.#_host[valueToIndex(index)];
    // wrap item in container
    return (
      isPlainObject(item)
      ? new TXmlElementController(item, this.#_options)
      : null
    );
  }

  /**
   * @param {any} item
   * @returns {number}
   * @protected
   * @description Adds a new element.
   */
  _addItemRaw(item) {
    let index = -1;
    if (this.isValidItem(item)) {
      index = this.size;
      this.#_host.push(item); // // TODO: correct count
    };
    return index;
  }

  /**
   * @param {any} item
   * @returns {number}
   * @description Adds a new element.
   */
  addItem(item) {
    // unwrap item from container
    if (item instanceof TXmlElementController) {
      item = TXmlElementController.__unwrap(item);
    };
    return this._addItemRaw(item);
  }

  /**
   * @param {number} index
   * @param {any} item
   * @returns {boolean}
   * @protected
   * @description Sets a new element.
   */
  _setItemRaw(index, item) {
    let isSUCCEED = this.chkIndex(index) && this.isValidItem(item);
    if (isSUCCEED) this.#_host[Number(index)] = item; // // TODO: correct count
    return isSUCCEED;
  }

  /**
   * @param {string} index
   * @param {any} item
   * @returns {boolean}
   * @description Sets a new element.
   */
  setItem(index, item) {
    // unwrap item from container
    if (item instanceof TXmlElementController) {
      item = TXmlElementController.__unwrap(item);
    };
    return this._setItemRaw(index, item);
  }

  /**
   * @param {number} index
   * @param {any} item
   * @returns {boolean}
   * @protected
   * @description Insertes a new element.
   */
  _insItemRaw(index, item) {
    index = valueToIndex(index);
    let size = this.size;
    let isSUCCEED = (
      index !== -1 && size >= index && this.isValidItem(item)
    );
    if (isSUCCEED) {
      if (size > index) {
        if (this.#_host[index] === null) {
          this.#_host[index] = item; // // TODO: correct count
        } else {
          this.#_host.splice(index, 0, item);  // // TODO: correct count
        };
      } else {
        this.#_host.push(item); // // TODO: correct count
      };
    };
    return isSUCCEED;
  }

  /**
   * @param {number} index
   * @param {any} item
   * @returns {boolean}
   * @description Insertes a new element.
   */
  insItem(index, item) {
    // unwrap item from container
    if (item instanceof TXmlElementController) {
      item = TXmlElementController.__unwrap(item);
    };
    return this._insItemRaw(index, item);
  }

  /**
   * @param {number} index
   * @returns {boolean}
   * @description Deletes an element.
   */
  delItem(value) {
    let isSUCCEED = this.chkIndex(value);
    if (isSUCCEED) {
      let index = Number(value);
      if (this.#_options.isNullItemsAllowed) {
        this.#_host[index] = null; // // TODO: correct count
      } else {
        if (index === 0) {
          this.#_host.shift(); // // TODO: correct count
        } else if (index === this.maxIndex) {
          this.#_host.pop(); // // TODO: correct count
        } else {
          this.#_host.splice(index, 1); // // TODO: correct count
        };
      };
    };
    return isSUCCEED;
  }

  /**
   * @param {any} items
   * @param {object} [opt]
   * @param {boolean} [opt.useClear=true]
   * @returns {number}
   * @description Loads a list of an elements.
   */
  loadItems(items, opt) {
    const _options = isPlainObject(opt) ? opt : {};
    let {
      useClear,
    } = _options;
    if (typeof useClear !== 'boolean') useClear = true;
    if (useClear) this.clear();
    let count = this.count;
    valueToArray(items).forEach((item) => { this.addItem(item); });
    return this.count - count;
  }

};

/**
 * @description This class implements an instance of an element
 *              that managea content of a document declaration
 */
class TXmlContentDeclaration {
  /** @property {?object} */
  #_host;// = null;
  /** @property {?object} */
  #_options;// = null;
  /** @property {?TXmlElementController} */
  #_ctrls;// = null;

  /**
   * @param {object} obj
   * @param {object} [opt]
   * @param {object} [opt.parseOptions]
   * @throws {TypeError}
   */
  constructor(obj, opt) {
    // check <obj>-param
    if (!isPlainObject(obj)) {
      throw new TypeError(`TXmlContentDeclaration : ${XML_TE_NOBJ_EMSG}`);
    };
    // load options
    let _options = isPlainObject(opt) ? opt : {};
    // set parser options
    let { parseOptions } = _options;
    if (!isPlainObject(parseOptions)) {
      _options.parseOptions = parseOptions = {};
    };
    let { settings } = parseOptions;
    if (!isPlainObject(settings)) parseOptions.settings = settings = {};
    let { declarationKey } = settings;
    if (
      typeof declarationKey !== 'string'
      || ((declarationKey = declarationKey.trim()) === '')
    ) {
      declarationKey = XML_DEF_PARSE_OPTIONS.declarationKey;
      settings.declarationKey = declarationKey;
    };
    // save options
    this.#_options = _options;
    // bind a documents content
    this.#_host = obj;
    // init a content declaration element
    this.init();
  }

  /**
   * @property {string}
   */
  get version() {
    return this.#_ctrls.getAttribute('version');
  }

  set version(value) {
    this.#_ctrls.setAttribute('version', value);
  }

  /**
   * @property {string}
   */
  get encoding() {
    return this.#_ctrls.getAttribute('encoding');
  }

  set encoding(value) {
    this.#_ctrls.setAttribute('encoding', value);
  }

  /**
   * @returns {boolean}
   * @description Initializes an instance content.
   */
  init() {
    const _options = this.#_options;
    this.#_ctrls = new TXmlElementController(
      xObj.insertXObjElement(
        this.#_host,
        _options.parseOptions.settings.declarationKey,
      ),
      _options,
    );
    // // TODO: set version & encoding
    if (!isNotEmptyString(this.version)) this.version = '1.0';
    if (!isNotEmptyString(this.encoding)) this.encoding = 'UTF-8';
  }

};

/**
 * @augments TXmlElementController
 * @description This class implements an instance of a root element
 */
class TXmlContentRootElement extends TXmlElementController {
  /** @property {?object} */
  #_content;// = null;
  /** @property {?object} */
  #_options;// = null;

  /**
   * @param {object} obj
   * @param {object} [opt]
   * @param {object} [opt.parseOptions]
   * @param {string} [opt.rootETagName='']
   * @param {boolean} [opt.autoBindRoot=true]
   * @throws {TypeError}
   */
  constructor(obj, opt) {
    // check <obj>-param
    if (!isPlainObject(obj)) {
      throw new TypeError(`TXmlContentRootElement : ${XML_TE_NOBJ_EMSG}`);
    };
    // load options
    const _options = isPlainObject(opt) ? opt : {};
    let { rootETagName, autoBindRoot, parseOptions } = _options;
    if (
      typeof rootETagName !== 'string'
      || ((rootETagName = rootETagName.trim()) === '')
    ) {
      rootETagName = XML_DEF_ROOT_ETAG_NAME;
    };
    if (typeof autoBindRoot !== 'boolean') autoBindRoot = true;
    if (!(parseOptions instanceof TXmlContentParseOptions)) {
      parseOptions = new TXmlContentParseOptions(parseOptions);
    };
    // get root element
    let rootItem = obj[rootETagName];
    if (obj[rootETagName] === undefined) {
      const entries = Object.keys(obj);
      const len = entries.length;
      if (len === 0) {
        rootItem = xObj.insertXObjElement(obj, rootETagName);
      } else {
        const {
          declarationKey,
          attributesKey,
          textKey,
          commentKey,
          cdataKey,
          nameKey,
          typeKey,
          parentKey,
          elementsKey,
        } = parseOptions.settings;
        const reservedKeys = new Set([
          declarationKey,
          attributesKey,
          textKey,
          commentKey,
          cdataKey,
          nameKey,
          typeKey,
          parentKey,
          elementsKey,
        ]);
        let targetKey = '';
        for (let name of entries) {
          if (!reservedKeys.has(name)) {
            targetKey = name;
            break;
          };
        };
        if (targetKey === '') {
          rootItem = xObj.insertXObjElement(obj, rootETagName);
        } else {
          if (autoBindRoot) {
            rootItem = obj[targetKey];
            rootETagName = targetKey;
            if (!isPlainObject(rootItem)) {
              rootItem = xObj.insertXObjElement(obj, rootETagName);
            };
          } else {
            throw new TypeError(`TXmlContentRootElement : ${XML_TE_NROOT_EMSG}`);
          };
        };
      };
    } else if (!isPlainObject(rootItem)) {
      rootItem = xObj.insertXObjElement(obj, rootETagName);
    };
    // init parent class instance
    super(rootItem, _options);
    // save options
    _options.rootETagName = rootETagName;
    _options.autoBindRoot = autoBindRoot;
    _options.parseOptions = parseOptions;
    this.#_options = _options;
    // bind a documents content
    this.#_content = obj;
    // init a content root element
    //this.init(rootETagName);
  }

  /**
   * @property {string}
   */
  get tagName() {
    return this.#_options.rootETagName;
  }

  set tagName(value) {
    value = readAsString(value, true);
    this.#_options.rootETagName = isNotEmptyString(
      value
    ) ? value : XML_DEF_ROOT_ETAG_NAME;
  }

  /**
   * @param {object} item
   * @param {boolean} [opt=false]
   * @returns {?object}
   * @protected
   * @static
   * @description Tries to unwraps a given object.
   */
  static __unwrap(item, opt) {
    return (
      item instanceof TXmlContentRootElement
      && (typeof opt === 'boolean' ? opt : false)
      ? item.#_content :
      TXmlElementController.__unwrap(item)
    );
  }

}

/**
 * @description This class implements an instance of a container
 *              that managea content of a document
 */
class TXmlContentContainer {
  /** @property {?object} */
  #_content;// = null;
  /** @property {?object} */
  #_options;// = null;
  /** @property {?object} */
  #_parseOptions;// = null;
  /** @property {?TXmlContentDeclaration} */
  #_docDecl;// = null;
  /** @property {?TXmlContentRootElement} */
  #_docRoot;// = null;

  /**
   * @param {object} [opt]
   * @param {object} [opt.parseOptions]
   */
  constructor(opt) {
    // load options
    const _options = isPlainObject(opt) ? opt : {};
    let { parseOptions } = _options;
    if (!(parseOptions instanceof TXmlContentParseOptions)) {
      parseOptions = new TXmlContentParseOptions(parseOptions);
    };
    // save options
    _options.parseOptions = parseOptions;
    this.#_options = _options;
    // save parser options
    this.#_parseOptions = parseOptions;
    // init a documents content
    let _content = {};
    this.#_content = _content;
    // init a content declaration element
    this.#_docDecl = new TXmlContentDeclaration(_content, _options);
    // init a content root element
    this.#_docRoot = new TXmlContentRootElement(_content, _options);
  }

  /**
   * @property {TXmlContentRootElement}
   * @readonly
   */
  get rootElement() {
    return this.#_docRoot;
  }

  /**
   * @returns {void}
   * @description Cleans a document content.
   */
  clear() {
    this.#_docDecl.init();
    this.rootElement.clear();
  }

  /**
   * @param {object} obj
   * @param {object} [opt]
   * @returns {boolean}
   * @protected
   * @description Initializes an instance with a given content.
   */
  _bindContent(obj, opt) {
    const _options = isPlainObject(opt) ? opt : this.#_options;
    let isSUCCEED = false;
    if (isPlainObject(obj)) {
      let { parseOptions } = _options;
      if (!(parseOptions instanceof TXmlContentParseOptions)) {
        parseOptions = new TXmlContentParseOptions(parseOptions);
        _options.parseOptions = parseOptions;
      };
      const _docDecl = new TXmlContentDeclaration(obj, _options);
      const _docRoot = new TXmlContentRootElement(obj, _options);
      // // TODO: check elements
      isSUCCEED = true;
      if (isSUCCEED) {
        // save options
        this.#_options = _options;
        // save parser options
        this.#_parseOptions = parseOptions;
        // save content
        this.#_content = obj;
        // bind refs to controllers
        this.#_docDecl = _docDecl;
        this.#_docRoot = _docRoot;
      };
    };
    return isSUCCEED;
  }

  /**
   * @returns {string}
   * @description Returns a document content as a string in an XML-format.
   */
  saveToXMLString() {
    let result = '';
    try {
      result = xmlParser.js2xml(this.#_content, this.#_parseOptions.js2xml);
    } catch (err) {
      //console.log('CHECK: TXmlContentContainer.saveToXMLString() => Error => '+err);
      throw err;
    };
    return result;
  }

  /**
   * @param {string} source
   * @returns {object}
   * @throws {Error}
   * @async
   * @description Saves a document content to a file.
   */
  saveToFile(source) {
    /**/// main part that return promise as a result
    return new Promise((resolve, reject) => {
      let data = {
        isSucceed: false,
        source: typeof source === 'string' ? source.trim() : '',
        content: '',
        isERR: false,
        errEvent: '',
      };
      if (data.source === '') {
        data.isERR = true;
        data.errEvent = (
          typeof source === 'string' ? 'EMPTY_SRCLINK' : 'NO_SRCLINK'
        );
        resolve(data);
      } else {
        data.content = this.saveToXMLString();
        fse.writeFile(data.source, data.content, 'utf8').then(result => {
          data.isSucceed = true;
          data.errEvent = 'OPS_IS_SUCCEED';
          resolve(data);
        }).catch(err => {
          let { isSucceed } = checkFsError(data, err);
          if (isSucceed) {
            data.content = '';
            resolve(data);
          } else {
            //console.log('CHECK: TXmlContentContainer.saveToFile() => Error => '+err);
            //console.log('CHECK: TXmlContentContainer.saveToFile() => Error => '+err.code);
            reject(err);
          };
        });
      };
    });
  }

  /**
   * @param {string} source
   * @returns {object}
   * @throws {Error}
   * @description Saves a document content to a file.
   */
  saveToFileSync(source) {
    let data = {
      isSucceed: false,
      source: typeof source === 'string' ? source.trim() : '',
      content: '',
      isERR: false,
      errEvent: '',
    };
    if (data.source === '') {
      data.isERR = true;
      data.errEvent = (
        typeof source === 'string' ? 'EMPTY_SRCLINK' : 'NO_SRCLINK'
      );
    } else {
      try {
        data.content = this.saveToXMLString();
        fs.writeFileSync(data.source, data.content, 'utf8');
      } catch (err) {
        let { isSucceed } = checkFsError(data, err);
        if (isSucceed) {
          data.content = '';
        } else {
          //console.log('CHECK: TXmlContentContainer.saveToFileSync() => Error => '+err);
          //console.log('CHECK: TXmlContentContainer.saveToFileSync() => Error => '+err.code);
          throw err;
        };
      };
    };
    return data;
  }

  /**
   * @param {string} loadFromXMLString
   * @returns {boolean}
   * @throws {Error}
   * @description Loads a document content from a string.
   */
  loadFromXMLString(xmlString) {
    let isSUCCEED = false;
    if (typeof xmlString === 'string') {
      let obj = null;
      try {
        obj = xmlParser.xml2js(xmlString, this.#_parseOptions.xml2js);
        if (isPlainObject(obj)) {
          isSUCCEED = this._bindContent(obj, this.#_options);
        };
      } catch (err) {
        //console.log('CHECK: TXmlContentContainer.loadFromXMLString() => Error => '+err);
        //console.log('CHECK: TXmlContentContainer.loadFromXMLString() => Error => '+err.code);
        throw err;
      };
    };
    return isSUCCEED;
  }

  /**
   * @param {string} source
   * @returns {object}
   * @throws {Error}
   * @async
   * @description Loads a document content from a file.
   */
  loadFromFile(source) {
    /**/// main part that return promise as a result
    return new Promise((resolve, reject) => {
      let data = {
        isLoaded: false,
        source: typeof source === 'string' ? source.trim() : '',
        content: '',
        isERR: false,
        errEvent: '',
      };
      if (data.source === '') {
        data.isERR = true;
        data.errEvent = (
          typeof source === 'string' ? 'EMPTY_SRCLINK' : 'NO_SRCLINK'
        );
        resolve(data);
      } else {
        fse.readFile(data.source, 'utf8').then(result => {
          data.content = result;
          if (result !== '') {
            if (this.loadFromXMLString(result)) {
              data.isLoaded = true;
              data.errEvent = 'OPS_IS_SUCCEED';
            } else {
              data.isERR = true;
              data.errEvent = 'OPS_IS_FAILED';
            };
          } else {
            data.isERR = true;
            data.errEvent = 'NO_CONTENT';
          };
          resolve(data);
        }).catch(err => {
          let { isSucceed } = checkFsError(data, err);
          if (isSucceed) {
            data.content = '';
            resolve(data);
          } else {
            //console.log('CHECK: TXmlContentContainer.loadFromFile() => Error => '+err);
            //console.log('CHECK: TXmlContentContainer.loadFromFile() => Error => '+err.code);
            reject(err);
          };
        });
      };
    });
  }

  /**
   * @param {string} source
   * @returns {object}
   * @throws {Error}
   * @description Loads a document content from a file.
   */
  loadFromFileSync(source) {
    let data = {
      isLoaded: false,
      source: typeof source === 'string' ? source.trim() : '',
      content: '',
      isERR: false,
      errEvent: '',
    };
    if (data.source === '') {
      data.isERR = true;
      data.errEvent = (
        typeof source === 'string' ? 'EMPTY_SRCLINK' : 'NO_SRCLINK'
      );
    } else {
      try {
        data.content = fs.readFileSync(data.source, 'utf8');
        if (data.content !== '') {
          if (this.loadFromXMLString(data.content)) {
            data.isLoaded = true;
            data.errEvent = 'OPS_IS_SUCCEED';
          } else {
            data.isERR = true;
            data.errEvent = 'OPS_IS_FAILED';
          };
        } else {
          data.isERR = true;
          data.errEvent = 'NO_CONTENT';
        };
      } catch (err) {
        let { isSucceed } = checkFsError(data, err);
        if (isSucceed) {
          data.content = '';
        } else {
          //console.log('CHECK: TXmlContentContainer.loadFromFileSync() => Error => '+err);
          //console.log('CHECK: TXmlContentContainer.loadFromFileSync() => Error => '+err.code);
          throw err;
        };
      };
    };
    return data;
  }

  /**
   * @returns {string}
   * @protected
   * @description Returns a document content a string in a JSON-format.
   */
  asJSON() {
    return JSON.stringify(this.#_content, null, 2);
  }

};

// === module exports block ===

module.exports.XML_DEF_PARSE_OPTIONS = XML_DEF_PARSE_OPTIONS;
module.exports.XML_DEF_ROOT_ETAG_NAME = XML_DEF_ROOT_ETAG_NAME;
module.exports.XML_LANG_ATTR_TNAME = XML_LANG_ATTR_TNAME;

module.exports.readAsTagName = readAsTagName;
module.exports.readAsAttrName = readAsAttrName;
module.exports.valueToElementID = valueToElementID;

module.exports.TXmlAttributesMapper = TXmlAttributesMapper;
module.exports.TXmlElementController = TXmlElementController;
module.exports.TXmlElementsListController = TXmlElementsListController;
module.exports.TXmlContentParseOptions = TXmlContentParseOptions;
module.exports.TXmlContentDeclaration = TXmlContentDeclaration;
module.exports.TXmlContentRootElement = TXmlContentRootElement;
module.exports.TXmlContentContainer = TXmlContentContainer;
