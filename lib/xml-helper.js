// [v0.1.004-20240702]

// === module init block ===

const {
  valueToArray,
} = require('@ygracs/bsfoc-lib-js');

const {
  TXmlElementController,
  TXmlElementsListController,
} = require('./xmldoc-lib');

// === module extra block (helper functions) ===

// === module main block ===

/***
 * (* constant definitions *)
 */

/***
 * (* function definitions *)
 */

/**
 * @function getChildByPath
 * @param {TXmlElementController}
 * @param {array}
 * @returns {?TXmlElementController}
 * @description Tries to get a child element.
 */
function getChildByPath(obj, chain) {
  let result = null;
  if (obj instanceof TXmlElementController) {
    const list = valueToArray(chain);
    let child = obj;
    let isFound = false;
    for (let name of list) {
      child = child.getChild(name);
      if (
        child !== null
        && (child instanceof TXmlElementController)
      ) {
        isFound = true;
      } else {
        isFound = false;
        break;
      };
    };
    if (isFound) result = child;
  };
  return result;
};

/**
 * @function addChildByPath
 * @param {TXmlElementController}
 * @param {array}
 * @returns {?TXmlElementController}
 * @description Tries to get a child element.
 */
function addChildByPath(obj, chain){
  let item = obj;
  let child = null;
  if (obj instanceof TXmlElementController) {
    const list = valueToArray(chain);
    for (let name of list) {
      child = item.getChild(name);
      if (child === null) {
        child = item.addChild(name);
      };
      if ((child instanceof TXmlElementController)) {
        item = child;
      } else {
        child = null;
        break;
      };
    };
  };
  return child;
};

/**
 * @function extractTextValues
 * @param {(TXmlElementController|TXmlElementsListController)}
 * @returns {array}
 * @description Reads a text values from a given XML-element.
 */
function extractTextValues(obj){
  const pushValue = (result, item) => {
    if (item instanceof TXmlElementController) {
      let value = item.textValue;
      if (value !== '') result.push(value);
    };
  };
  let result = [];
  if (obj instanceof TXmlElementsListController) {
    for (let i = 0; i < obj.size; i++) {
      pushValue(result, obj.getItem(i));
    };
  } else {
    pushValue(result, obj);
  };
  return result;
};

/**
 * @function extractTextValuesLn
 * @param {(TXmlElementController|TXmlElementsListController)}
 * @returns {array}
 * @description Reads a text values with a `lang` attribute
 *              from a given XML-element.
 */
function extractTextValuesLn(obj){
  const pushValue = (result, item) => {
    if (item instanceof TXmlElementController) {
      let [ lang, value ] = item.getTextValue();
      if (value !== '') result.push([ lang, value ]);
    };
  };
  let result = [];
  if (obj instanceof TXmlElementsListController) {
    for (let i = 0; i < obj.size; i++) {
      pushValue(result, obj.getItem(i));
    };
  } else {
    pushValue(result, obj);
  };
  return result;
};

/***
 * (* class definitions *)
 */

// === module exports block ===

module.exports.getChildByPath = getChildByPath;
module.exports.addChildByPath = addChildByPath;
module.exports.extractTextValues = extractTextValues;
module.exports.extractTextValuesLn = extractTextValuesLn;
